#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Merge

    Fusion de certaines sorties pour deux zones simulees separement

        copyright            : (C) 2018 by Vincent DELBAR

"""
import os
import re
import sys
from pathlib import Path
from shutil import rmtree

data_dir = Path(sys.argv[1])
input_dir = Path(sys.argv[2])
output_dir = Path(sys.argv[3])

mkdirList = ['fusion/inf',
             'fusion/maitrise',
             'fusion/central',
             'fusion/developpement',
             'fusion_bv/tif',
             'fusion_bv/asc']

if (output_dir/'fusion').exists():
    rmtree(str(output_dir/'fusion'))
if (output_dir/'fusion_bv').exists():
    rmtree(str(output_dir/'fusion_bv'))

for d in mkdirList:
    os.makedirs(str(output_dir/d))

zones = ['mtp', 'gpsl']
scenarios = {'inf': {}, 'maitrise': {}, 'central': {}, 'developpement': {}}
directories = os.listdir(str(input_dir))

res = None
exp = r'SimUrba_([0-9]{12})_([a-z]*)_brgm_([a-z]*)_(.*)'
for d in directories:
    res = re.match(exp, d)
    if res:
        zone = res.group(2)
        scen = res.group(3)
        scenarios[scen][zone] = input_dir/d

print(str(scenarios))

rasters2014 = ['artificialisation.tif',
               'ratio_plancher_sol.tif',
               'demographie.tif',
               'surface_bati_res_pla.tif']

rasters2040 = ['ocsol_2040.tif',
               'artificialisation_2040.tif',
               'artificialisation_nouvelle.tif',
               'ratio_plancher_sol_2040.tif',
               'demographie_2040.tif',
               'surface_plancher_2040.tif',
               'surface_densif_plancher.tif',
               'ocsol_orig_reclassee.tif']

gdalwarp = 'gdalwarp -srcnodata 0 -dstnodata 0 {} {} {}'
for r in rasters2014:
    cmd = gdalwarp.format(data_dir/('gpsl/inputs/25m/' + r),
                          data_dir/('mtp/inputs/25m/' + r),
                          output_dir/('fusion/' + r))
    os.system(cmd)

cmd = gdalwarp.format(scenarios['inf']['gpsl']/'ocsol_orig_reclassee.tif',
                      scenarios['inf']['mtp']/'ocsol_orig_reclassee.tif',
                      output_dir/('fusion/' + 'ocsol_orig_reclassee.tif'))
os.system(cmd)

for r in rasters2040:
    if r == 'ocsol_2040.tif':
        gdalwarp = 'gdalwarp -srcnodata 0 -dstnodata 0 {} {} {}'
    else:
        gdalwarp = 'gdalwarp {} {} {}'
    for s, z in scenarios.items():
        cmd = gdalwarp.format(z['gpsl']/r, z['mtp']/r,
                              output_dir/('fusion/' + s + '/' + r))
        os.system(cmd)

filler = Path.home()/'SimUrba/brgm/all/clc12_bv_reclassee.tif'
gdalwarp = 'gdalwarp -srcnodata 0 -dstnodata 0 {} {} {}'
for s in scenarios.keys():
    outfile = 'ocsol_2040_BV_' + s + '.tif'
    cmd = gdalwarp.format(filler, output_dir/('fusion/' + s + '/ocsol_2040.tif'),
                          output_dir/('fusion_bv/tif/' + outfile))
    os.system(cmd)

    ascfile = outfile.replace('.tif', '.asc')
    gdal_translate = 'gdal_translate {} {}'
    cmd = gdal_translate.format(output_dir/('fusion_bv/tif/' + outfile),
                                output_dir/('fusion_bv/asc/' + ascfile))
    os.system(cmd)
