#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Reclass

    Reclassement des sorties de simulation a partir de classes "hard-coded"

        copyright            : (C) 2018 by Vincent DELBAR

"""
import os
import csv
import sys
import gdal
import numpy as np
from pathlib import Path
from simurba.tools import to_nparray, to_tiff


def zero_mask(arr, mask):
    return np.where(mask == 1, 0, arr)


inputs = Path(sys.argv[1])
HOME = Path.home()

for d in os.listdir(str(inputs)):
    directory = Path(inputs/d)
    zone = d.split('_')[2]
    scenario = 'brgm_' + d.split('_')[4]
    dataDir = HOME/('SimUrba/brgm/data/' + zone + '/inputs/25m/')

    ds = gdal.Open(str(dataDir/'classes_ocsol.tif'))
    proj = ds.GetProjection()
    geot = ds.GetGeoTransform()
    ocsol_orig = ds.GetRasterBand(1).ReadAsArray().astype(np.uint16)
    ocsol = ocsol_orig.copy()
    rows, cols = ocsol.shape[0], ocsol.shape[1]

    mask = to_nparray(dataDir/'masque.tif')
    artif_orig = zero_mask(to_nparray(dataDir/'artificialisation.tif'), mask)
    artif_orig = (artif_orig / artif_orig.max()).astype(np.float32)
    nb_niv_orig = zero_mask(to_nparray(dataDir/'ratio_plancher_sol.tif', np.float32), mask)
    artif_nouv = zero_mask(to_nparray(directory/'artificialisation_nouvelle.tif', np.uint16), mask)
    dsf = zero_mask(to_nparray(directory/'densification_plancher.tif', np.byte), mask)
    nb_niv = zero_mask(to_nparray(directory/'ratio_plancher_sol_2040.tif', np.float32), mask)
    artif = zero_mask(to_nparray(directory/'artificialisation_2040.tif', np.uint16), mask)
    artif = (artif / artif.max()).astype(np.float32)
    ocsol_orig = ds.GetRasterBand(1).ReadAsArray().astype(np.uint16)
    ocsol = ocsol_orig.copy()

    zac_mmm = to_nparray(HOME/'SimUrba/brgm/all/zac_mtp.tif', np.byte)
    zae_mmm = to_nparray(HOME/'SimUrba/brgm/all/zae_mtp.tif', np.byte)
    zac_gpsl = to_nparray(HOME/'SimUrba/brgm/all/zac_gpsl.tif', np.byte)
    zae_gpsl = to_nparray(HOME/'SimUrba/brgm/all/zae_gpsl.tif', np.byte)

    # Les classes
    if zone == 'mtp':
        # mixte
        ocsol = np.where(((artif_nouv > 0) | (dsf == 1)) & (nb_niv < 3)
                         & ((ocsol != 211) & (ocsol != 212) & (ocsol != 213) & (ocsol != 215))
                         & (ocsol != 111), 115, ocsol)
        # collectif
        ocsol = np.where(((artif_nouv > 0) | (dsf == 1)) & (nb_niv >= 3)
                         & ((ocsol != 211) & (ocsol != 212) & (ocsol != 213) & (ocsol != 215))
                         & (ocsol != 111), 114, ocsol)
        # pav peu dense
        ocsol = np.where(((artif_nouv > 0) | (dsf == 1)) & (artif <= 0.2) & (nb_niv < 2)
                         & ((ocsol != 211) & (ocsol != 212) & (ocsol != 213) & (ocsol != 215))
                         & (ocsol != 111), 113, ocsol)
        # pav dense
        ocsol = np.where(((artif_nouv > 0) | (dsf == 1)) & (artif > 0.2) & (nb_niv < 2)
                         & ((ocsol != 211) & (ocsol != 212) & (ocsol != 213) & (ocsol != 215))
                         & (ocsol != 111), 112, ocsol)
        # za
        ocsol = np.where((artif_nouv > 0) & (zac_mmm == 1), 121, ocsol)
        ocsol = np.where((artif_nouv > 0) & (zae_mmm == 1), 142, ocsol)

    elif zone == 'gpsl':
        # mixte
        ocsol = np.where(((artif_nouv > 0) | (dsf == 1)) & (nb_niv < 3) & (
                        ((ocsol < 400) & (ocsol != 121) & (ocsol != 131) & (ocsol != 132))
                        & (ocsol != 111)), 115, ocsol)
        # collectif
        ocsol = np.where(((artif_nouv > 0) | (dsf == 1)) & (nb_niv >= 3) & (
                         ((ocsol < 400) & (ocsol != 121) & (ocsol != 131) & (ocsol != 132))
                         & (ocsol != 111)), 114, ocsol)
        # pav peu dense
        ocsol = np.where(((artif_nouv > 0) | (dsf == 1)) & (artif <= 0.2) & (nb_niv < 2) & (
                         ((ocsol < 400) & (ocsol != 121) & (ocsol != 131) & (ocsol != 132))
                         & (ocsol != 111)), 113, ocsol)
        # pav dense
        ocsol = np.where(((artif_nouv > 0) | (dsf == 1)) & (artif > 0.2) & (nb_niv < 2) & (
                         ((ocsol < 400) & (ocsol != 121) & (ocsol != 131) & (ocsol != 132))
                         & (ocsol != 111)), 112, ocsol)
        # za
        ocsol = np.where((artif_nouv > 0) & (zac_gpsl == 1), 121, ocsol)
        ocsol = np.where((artif_nouv > 0) & (zae_gpsl == 1), 142, ocsol)

    with open(HOME/('SimUrba/brgm/all/reclass_' + zone + '.csv'), 'r') as r:
        next(r)
        c = csv.reader(r)
        for line in c:
            a, b = int(line[1]), int(line[2])
            ocsol = np.where(ocsol == a, b, ocsol)
            ocsol_orig = np.where(ocsol_orig == a, b, ocsol_orig)

        to_tiff(ocsol, 'uint16', proj, geot, directory/'ocsol_2040.tif')
        to_tiff(ocsol_orig, 'uint16', proj, geot, directory/'ocsol_orig_reclassee.tif')

    srf = {}
    for a in np.unique(ocsol_orig):
        if a != 0:
            srf[a] = [np.where(ocsol_orig == a, 25 * 25, 0).sum()]
    if zone == 'gpsl':
        srf[113] = srf[114] = srf[115] = [0]

    for a in np.unique(ocsol):
        if a != 0:
            srf[a].append(np.where(ocsol == a, 25 * 25, 0).sum())

    with (directory/'evo_classes_ocs.csv').open('w') as w:
        w.write('nouveau_code,aire_orig,aire_nouv,diff,evo\n')
        for k, v in srf.items():
            a = str(v[0])
            b = str(v[1])
            c = str(int(b) - int(a))
            if int(a) > 0:
                d = str(int(c) / int(a))
            else:
                d = 'nan'
            line = '{},{},{},{},{}\n'.format(k, a, b, c, d)
            w.write(line)
