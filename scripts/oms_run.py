#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/*****************************************************************************
OpenMOLE Script Run

    Pour régler le soucis de guillemets dans une UDockerTask sur OpenMOLE



"""
import sys
from simurba.simu import Simulation, SETTINGS

params = sys.argv[1:]
param_str = ''
n = len(SETTINGS)

if len(params) == n:
    for i in range(n):
        p = params[i]
        if i < n - 1:
            p += ' '
        param_str += p

else:
    i = 0
    for p in params:
        if '=' not in p:
            sys.exit("Can't split('=') : " + p)

        k = p.split('=')[0]
        if k in SETTINGS.keys():
            if i < len(params) - 1:
                p += ' '
            param_str += p
            i += 1
        else:
            sys.exit('Unknown parameter name : ' + k)

if __name__ == "__main__":
    s = Simulation('openmole', param_str)
    s.run()
