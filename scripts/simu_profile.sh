#!/bin/bash
# Script pour faire un profil de l'éxécution d'une simulation avec gprof2dot

WORKSPACE=$1
cd $WORKSPACE
NAME=$2
PARAMS=$3
if [ -n "$PARAMS" ]; then
    PARAMS="dataDir="$HOME"/SimUrba/zooms/mtp/inputs/50m/ scenario=reduction growth=0.5"
fi

# pip3 install gprof2dot
echo "from simurba import Simulation" > dummy_simu.py
echo "s=Simulation('void', '"$PARAMS"')" >> dummy_simu.py
echo "s.run(False, False)" >> dummy_simu.py
python3 -m cProfile -o simurba_"$NAME"_profile dummy_simu.py
gprof2dot -f pstats simurba_"$NAME"_profile | dot -Tpdf -o simurba_"$NAME"_pstats.pdf
gprof2dot -f pstats simurba_"$NAME"_profile | dot -Tpng -o simurba_"$NAME"_pstats.png
rm -f dummy_simu.py
