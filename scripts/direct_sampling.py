#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Simulation sampling

    Pour proceder à un echantillonage uniforme dans l'espace des parametres et indefiniment,
    avec enregistrements dans psql (pour un service systemd sur Linux)

        copyright            : (C) 2019 by Vincent DELBAR

"""
import sys
import multiprocessing as mp
from pathlib import Path
from time import sleep
from simurba.simu import Simulation
import gc

CREATE_SAMPLING_SQL = """
    DROP TABLE IF EXISTS sampling; CREATE TABLE sampling (
    id SERIAL PRIMARY KEY, timestamp varchar(14) UNIQUE NOT NULL, pix_res INTEGER NOT NULL,
    growth FLOAT NOT NULL, final_year FLOAT NOT NULL, scenario varchar(20) NOT NULL,
    plu_prio boolean NOT NULL, build_non_res boolean NOT NULL, exclusion_ratio FLOAT NOT NULL,
    max_built_ratio INTEGER NOT NULL, max_used_srf_pla INTEGER NOT NULL,
    densify_old boolean NOT NULL, win_size INTEGER NOT NULL,
    min_contig FLOAT NOT NULL, max_contig FLOAT NOT NULL, road_ratio FLOAT NOT NULL,
    fit_to varchar(5) NOT NULL, eco_weight INTEGER NOT NULL, road_weight INTEGER NOT NULL,
    sir_weight INTEGER NOT NULL, tra_weight INTEGER NOT NULL, m2sol_hab09 FLOAT NOT NULL,
    m2sol_hab15 FLOAT NOT NULL, pop_nouv INTEGER NOT NULL, skip_zau_year INTEGER,
    choices_sum INTEGER NOT NULL, exec_time FLOAT NOT NULL, reste_pop INTEGER NOT NULL,
    reste_srf FLOAT NOT NULL, peupl_moy FLOAT NOT NULL, artif_nouv FLOAT NOT NULL,
    bati_sol FLOAT NOT NULL, bati_pla FLOAT NOT NULL, cell_ouv INTEGER NOT NULL,
    impact_env FLOAT NOT NULL, tx_artif_moy FLOAT NOT NULL, cell_dsf INTEGER,
    m2sol_hab_fin FLOAT NOT NULL);
"""
# os.system('psql -d simurba -c ' + CREATE_SAMPLING_SQL)

nb_cores = 1
if len(sys.argv) > 1:
    nb_cores = int(sys.argv[1])

input_dir = Path().home()/'SimUrba/zooms/mtp/full_dataset'
if len(sys.argv) > 2:
    input_dir = Path(sys.argv[2])


if __name__ == '__main__':

    gc.enable()
    # On continue tant que le processus n'est pas interompu
    pool = mp.Pool(nb_cores)
    sen, rec = mp.Pipe()

    base_param = 'scenario=random dataDir='
    for c in range(nb_cores):
        pool.apply_async(Simulation('psql', base_param + str(input_dir), False, sen).run,
                         (False, True))
        sleep(1)

    while True:
        # Des qu'une simulation est terminée, on en ajoute une nouvelle
        rec.recv()
        pool.apply_async(Simulation('psql', base_param + str(input_dir), False, sen).run,
                         (False, True))
        sleep(1)

    pool.terminate()
