#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Batch

    Lancer plusieurs simulations a l'aide du module multiprocessing

        copyright            : (C) 2018 by Vincent DELBAR

"""
import sys
import multiprocessing as mp
from pathlib import Path
from time import sleep


def printer(string):
    if sys.platform == 'win32':
        sys.stdout.write("\r" + string)
    else:
        sys.stdout.write("\r\x1b[K" + string)
    sys.stdout.flush()


PREP_ALIAS = ['-p', 'p', 'prep', 'preparation', 'prepare']
SIMU_ALIAS = ['-s', 's', 'simu', 'simulation', 'simulate']

task = sys.argv[1]
# Le fichier texte doit contenir simplement une chaine de paramètres par ligne
param_file = Path(sys.argv[2])
nb_cores = 1
if len(sys.argv) > 3:
    nb_cores = int(sys.argv[3])

if task in SIMU_ALIAS:
    from simurba.simu import Simulation
else:
    from simurba.prep import Preparation, QgsProcessingException


if __name__ == '__main__':
    params = []
    s = 0
    e = 0
    with open(str(param_file), 'r') as r:
        for l in r.readlines():
            if len(l) > 1 and l[0] != '#':
                params.append(l[:-1])
    params.reverse()
    t = len(params)
    print('\n\n')
    progres = "Completed tasks :   %i/%i   (%i succeeded)" % (0, t, s)
    printer(progres)

    if task in SIMU_ALIAS:
        pool = mp.Pool(nb_cores)
        snd, rec = mp.Pipe()
        simus = []
        jobs = []
        for i in range(nb_cores):
            p = params.pop()
            pool.apply_async(Simulation('batch', p).run,
                             (True, False, snd))
            sleep(1)

        c = 0
        while c < t:
            res = rec.recv()
            if res:
                s += 1
            else:
                e += 1
            if params:
                p = params.pop()
                pool.apply_async(Simulation('batch', p).run,
                                 (True, False, snd))
            c += 1
            progres = "Completed tasks :   %i/%i   (%i succeeded)" % (c, t, s)
            printer(progres)
            sleep(1)

        pool.close()
        pool.join()
        pool.terminate()

    elif task in PREP_ALIAS:
        c = 0
        while params:
            res = None
            p = None
            try:
                p = Preparation('cli', params.pop(), debug=False)
                res = p.run()
            except (FileNotFoundError, QgsProcessingException):
                pass
            except KeyboardInterrupt:
                raise
            finally:
                c += 1
                if res:
                    s += 1
                else:
                    e += 1
                if p is not None:
                    del p

                progres = "Completed tasks : %i/%i - %i succeeded" % (c, t, s)
                printer(progres)

    print('\nEnd.')
