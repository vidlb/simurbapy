-- Primary key and indexes
ALTER TABLE bati00 DROP COLUMN IF EXISTS id_local ;
ALTER TABLE bati00 ADD COLUMN id_local varchar(15);
UPDATE bati00 SET id_local=replace(concat(ccodep, ccocom, invar), ' ', '0');
CREATE INDEX bati00_id_local ON bati00 (id_local);
ALTER TABLE bati00 ADD CONSTRAINT bati00_pkey PRIMARY KEY (id_local);

ALTER TABLE bati00 DROP COLUMN IF EXISTS id_parc ;
ALTER TABLE bati00 ADD COLUMN id_parc varchar(14);
UPDATE bati00 SET id_parc=replace(concat(ccodep, ccocom, ccopre, ccosec, dnupla), ' ', '0');
CREATE INDEX bati00_id_parc ON bati00 (id_parc);

ALTER TABLE bati10 DROP COLUMN IF EXISTS id_local ;
ALTER TABLE bati10 ADD COLUMN id_local varchar(15);
UPDATE bati10 SET id_local=replace(concat(ccodep, ccocom, invar), ' ', '0');
CREATE INDEX bati10_id_local ON bati10 (id_local);
ALTER TABLE bati10 ADD CONSTRAINT bati10_pkey PRIMARY KEY (id_local);

ALTER TABLE nbat10 DROP COLUMN IF EXISTS id_parc ;
ALTER TABLE nbat10 ADD COLUMN id_parc varchar(14);
UPDATE nbat10 SET id_parc=replace(concat(ccodep, ccocom, ccopre, ccosec, dnupla), ' ', '0');
CREATE INDEX nbat10_id_parc ON nbat10 (id_parc);
ALTER TABLE nbat10 ADD CONSTRAINT nabt10_pkey PRIMARY KEY (id_parc);

ALTER TABLE nbat21 DROP COLUMN IF EXISTS id_parc ;
ALTER TABLE nbat21 ADD COLUMN id_parc varchar(14);
UPDATE nbat21 SET id_parc=replace(concat(ccodep, ccocom, ccopre, ccosec, dnupla), ' ', '0');
CREATE INDEX nbat21_id_parc ON nbat21 (id_parc);

ALTER TABLE pdll30 DROP COLUMN IF EXISTS id_lot ;
ALTER TABLE pdll30 ADD COLUMN id_lot varchar(24);
UPDATE pdll30 SET id_lot=replace(concat(ccodep, ccocom, ccopre, ccosec, dnupla, dnupdl, dnulot), ' ', '0');
CREATE INDEX pdll30_id_lot ON pdll30 (id_lot);

ALTER TABLE nbat21 DROP COLUMN IF EXISTS lot_pdl ;
ALTER TABLE nbat21 ADD COLUMN lot_pdl varchar(24);
UPDATE nbat21 SET lot_pdl=replace(concat(ccodep, ccocom, ccoprel, ccosecl, dnuplal, dnupdl, dnulot), ' ', '0');
CREATE INDEX nbat21_lot_pdl ON nbat21 (lot_pdl);

ALTER TABLE lloc DROP COLUMN IF EXISTS id_parc ;
ALTER TABLE lloc ADD COLUMN id_parc varchar(14);
UPDATE lloc SET id_parc=replace(concat(ccodepl, ccocoml, ccoprel, ccosecl, dnuplal), ' ', '0');
CREATE INDEX lloc_id_parc ON lloc (id_parc);

ALTER TABLE lloc DROP COLUMN IF EXISTS id_lot ;
ALTER TABLE lloc ADD COLUMN id_lot varchar(24);
UPDATE lloc SET id_lot=replace(concat(ccodepl, ccocoml, ccoprel, ccosecl, dnuplal, dnupdl, dnulot), ' ', '0');
CREATE INDEX lloc_id_lot ON lloc (id_lot);

ALTER TABLE prop ADD COLUMN id_prop text;
UPDATE prop SET id_prop=concat(ccodep,ccocom,dnupro);
CREATE INDEX prop_id_prop ON prop (id_prop);

ALTER TABLE prop ADD COLUMN id_pers varchar(17);
UPDATE prop SET id_pers = concat(id_prop,dnuper);
ALTER TABLE prop ADD CONSTRAINT prop_pkey PRIMARY KEY (id_pers);

-- Pour séléctionner les parcelles non residentielles par la suite (where min_dteloc > 2)
DROP TABLE IF EXISTS parcelles_agg_bati;
CREATE TABLE parcelles_agg_bati AS (
    SELECT parc.ogc_fid, parc.geom, parc.id,
    agg.* FROM (select id_parc, count(*) AS nb_bat,
    avg(nullif(nullif(jannat,'9999'),'0000')::int)::int AS avg_jannat,
    avg(nullif(dnbniv,'00')::int)::float AS avg_dnbniv,
    min(nullif(dnbniv,'00')::int) AS min_dnbniv,
    max(nullif(dnbniv,'00')::int) AS max_dnbniv,
    min(dteloc::int) AS min_dteloc,
    max(dteloc::int) AS max_dteloc FROM (
        SELECT b.*, a.id_parc FROM bati00 AS a
        INNER JOIN bati10 AS b ON a.id_local = b.id_local)
    AS loc GROUP BY id_parc)
AS agg inner join parcelles AS parc ON parc.id = agg.id_parc);
