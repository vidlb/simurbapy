#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Magic

    Extraction des données MAJIC III millesime 2017

        copyright            : (C) 2018 by Vincent DELBAR

"""
import os
import re
import sys
import csv
import operator
from pathlib import Path
from multiprocessing import Pool, Pipe
from time import strftime, time

nb_cores = int(sys.argv[1])
input_dir = Path(sys.argv[2])
models_dir = Path(sys.argv[3])
output_dir = Path(sys.argv[4])
if len(sys.argv) > 5:
    dep_list = sys.argv[5].split(',')
if len(sys.argv) > 6:
    param = sys.argv[6]
    if param != 'all':
        tables = sys.argv[6].upper().split(',')

if not output_dir.exists():
    os.makedirs(str(output_dir))


def printer(string):
    if sys.platform == 'win32':
        sys.stdout.write("\r" + string)
    else:
        sys.stdout.write("\r\x1b[K" + string)
    sys.stdout.flush()


model = {}
cenr_dic = {
    'BATI': ['00', '10', '21', '30', '36', '40', '50', '60'],
    'NBAT': ['10', '21', '30', '36'],
    'PDLL': ['10', '20', '30']
}
cenr_pos_dic = {
    'BATI': [30, 32],
    'NBAT': [19, 21],
    'PDLL': [25, 27]
}
min_len_dic = {'BATI': 82, 'LLOC': 61, 'NBAT': 89, 'PDLL': 98, 'PROP': 121}

if 'tables' not in globals():
    tables = ['BATI', 'LLOC', 'NBAT', 'PDLL', 'PROP']
if 'dep_list' not in globals():
    dep_list = []
    fileList = os.listdir(str(input_dir))
    fileList.sort()
    for f in fileList:
        res = None
        res = re.search(r'ART\.DC21\.W17([0-9]{2})0\.BATI\.A([0-9]{4})\.N000671', f)
        if res:
            dep = res.group(1)
            if dep not in dep_list:
                dep_list.append(dep)


mod_list = os.listdir(str(models_dir))
mod_list.sort()
# Création de la structure de recherche des valeurs de découpe {'':{'':[]}}
for tab in mod_list:
    with (models_dir/tab).open('r') as file:
        reader = csv.reader(file)
        next(reader, None)
        tab = tab.replace('.csv', '')
        model[tab] = {str(row[4]): [int(row[0]) - 1, int(row[1]), int(row[2])] for row in reader}

# Tri du dictionnaire en fonction de la première valeur début, utilisée pour créer la ligne CSV
tmp_model = {}
for tab in model.keys():
    tmp_model[tab] = sorted(model[tab].items(), key=operator.itemgetter(1))
model_sorted = {tab: [field[0] for field in tmp_model[tab]]
                for tab in tmp_model.keys()}
del tmp_model


def write_headers(prefix, dep, tab):
    with (prefix/(tab + '.csv')).open('w') as w:
        i = 0
        h = ''
        for field in model_sorted[tab]:
            i += 1
            h += field
            if i < len(model_sorted[tab]):
                h += ','
            else:
                h += '\n'
        w.write(h)
    with (prefix/'copy_csv.sql').open('a') as w:
        i = 0
        h = 'CREATE TABLE IF NOT EXISTS ' + tab.lower() + '('
        for field in model_sorted[tab]:
            i += 1
            lgr = model[tab][field][2]
            h += '"' + field.lower() + '" varchar(' + str(lgr) + ')'
            if i < len(model_sorted[tab]):
                h += ','
            else:
                h += ');\n'
        w.write(h)
        w.write('\\COPY ' + tab.lower()
                + ' FROM ' + tab + """.csv CSV HEADER QUOTE '"' DELIMITER ','; \n""")


def get_tuple(l, tab):
    i = 0
    tuple = ''
    len_line = len(l)
    for field in model_sorted[tab]:
        deb = int(model[tab][field][0])
        fin = int(model[tab][field][1])
        if deb < len_line:
            if fin <= len_line:
                v = l[deb:fin]
            else:
                v = l[deb:len_line]
            if '"' in v:
                v = v.replace('"', '')
            v = '"' + v + '"'
        else:
            v = ''
        if i < len(model_sorted[tab]) - 1:
            v += ','
        tuple += v
        i += 1
    return tuple + '\n'


def write_line(prefix, dep, tab, line, min_len, cenr_pos_idx):
    e = line[cenr_pos_idx[0]:cenr_pos_idx[1]]
    if e.isnumeric() and e in cenr_dic[tab]:
        with (prefix/(tab + e + '.csv')).open('a') as w:
            w.write(get_tuple(line, tab + e))


def parse_table(prefix, dep, tab, sender):
    min_len = min_len_dic[tab]
    with (input_dir/('ART.DC21.W17' + dep + '0.' + tab + '.A2017.N000671')).open('r') as r:
        if tab in cenr_pos_dic.keys():
            cenr_pos_idx = cenr_pos_dic[tab]
            for line in r:
                if len(line) >= min_len:
                    write_line(prefix, dep, tab, line[:-1], min_len, cenr_pos_idx)
        else:
            with (prefix/(tab + '.csv')).open('a') as w:
                for line in r:
                    if len(line) >= min_len:
                        w.write(get_tuple(line[:-1], tab))
        sender.send(True)
        return
    sender.send(False)


if __name__ == '__main__':
    start_time = time()
    print("Started at " + strftime('%H:%M:%S'))

    # Variables pour le multiprocessing
    pool = Pool(nb_cores)
    snd, rec = Pipe()
    jobs = []
    # Itération dans les départements
    for dep in dep_list:
        prefix = output_dir/dep
        if not prefix.exists():
            os.makedirs(str(prefix))
        for tab in tables:
            if tab not in cenr_dic.keys():
                write_headers(prefix, dep, tab)
            else:
                for e in cenr_dic[tab]:
                    write_headers(prefix, dep, tab + e)
            jobs.append(pool.apply_async(parse_table, (prefix, dep, tab, snd)))

    pool.close()
    s = 0
    e = 0
    t = len(jobs)
    progres = "Parsed files :   %i/%i   (%i succeeded)" % (0, t, s)
    printer(progres)
    for c in range(t):
        res = rec.recv()
        if res:
            s += 1
        else:
            e += 1
        progres = "Parsed files :   %i/%i   (%i succeeded)" % (c + 1, t, s)
        printer(progres)
    pool.terminate()

    end_time = time()
    exec_time = end_time - start_time
    exec_min = round(exec_time // 60)
    exec_sec = round(exec_time % 60)
    print("\nTotal elapsed time : %im %is" % (exec_min, exec_sec))
