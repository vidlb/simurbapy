#!/bin/bash

#all_dpt=(09 11 12 30 31 32 34 46 48 65 66 81 82)
all_dpt=(12 30 31 34 46 66)
# Import des fichiers vecteurs cadastraux DGFIP
dgfip_dir=$HOME"/Geom/data/vector/occitanie/dgfip/"
cd $dgfip_dir
for shape in parcelles subdivisions_fiscales; do
    for dpt in $all_dpt; do
        unzip $dpt/"cadastre-"$dpt"-"$shape"-shp.zip" -d $dpt
        shp2pgsql -s 2154 -p -g geom -D -I -t 2D $dpt/$shape".shp" | psql -d cadastre
        ogr2ogr --config PG_USE_COPY YES -gt 65536 -nlt MULTIPOLYGON -f PGDump /vsistdout/ $dpt"/"$shape".shp" \
                -lco GEOMETRY_NAME=geom -lco CREATE_TABLE=OFF -lco DROP_TABLE=OFF -lco SPATIAL_INDEX=GIST | iconv -f iso-8859-14 -t utf8 | psql -d cadastre
        rm -f $dpt/$shape".dbf" $dpt/$shape".shp" $dpt/$shape".shx" $dpt/$shape".prj"
    done
    if [ $shape = "parcelles" ]; then
        psql -d cadastre -c "create unique index "$shape"_id ON "$shape" (id)"
    fi
done

# Import des sorties du script dans la base
majic_dir=$HOME"/Geom/data/stat/dgfip/majic/csv"
cd $majic_dir
for dpt in $all_dpt; do
    cd $dpt
    psql -d cadastre -f copy_csv.sql
    cd ..
done
