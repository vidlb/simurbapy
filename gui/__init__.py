# -*- coding: utf-8 -*-
"""
/***************************************************************************
QGISimUrba

    Plugin pour la simulation de l'urbanisation

        copyright            : (C) 2018 by Vincent DELBAR

"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    from .qgisimurba import QGISimUrba
    return QGISimUrba(iface)
