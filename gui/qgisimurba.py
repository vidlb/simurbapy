# -*- coding: utf-8 -*-
"""
/***************************************************************************
QGISimUrba

    Intégration de la classe Simulation dans un plugin QGIS via une QgsTask

        copyright            : (C) 2018 by Vincent DELBAR

"""
import os
from numpy import inf
from time import time
from pathlib import Path
from collections import OrderedDict

from qgis.core import Qgis, QgsApplication, QgsTask, QgsMessageLog
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QDockWidget

from .widget import Ui_QSimUrbaWidget
from .simu import Simulation, random_scenario, SETTINGS as SIMU_SETTINGS
from .tools import to_tiff
from .resources import *

SIMU_QOBJECTS = OrderedDict([
    ("dataDir", "dataDirWidget"),
    ("outputDir", "outputDirWidget"),
    ("growth", "growthSpinBox"),
    ("finalYear", "finalYearSpinBox"),
    ("scenario", "scenarioCombo"),
    ("pluPriority", "pluPriorityCheckBox"),
    ("buildNonRes", "buildNonResCheckBox"),
    ("exclusionRatio", "exclusionRatioSpinBox"),
    ("maxBuiltRatio", "maxBuiltRatioSpinBox"),
    ("maxUsedSrfPla", "maxUsedSrfPlaSpinBox"),
    ("densifyOld", "densifyOldCheckBox"),
    ("winSize", "winSizeSpinBox"),
    ("minContig", "minContigSpinBox"),
    ("maxContig", "maxContigSpinBox"),
    ("roadRatio", "roadRatioSpinBox"),
    ("fitTo", "fitToCombo"),
    ("seed", "seedSpinBox"),
    ("ecoWeight", "ecoSpinBox"),
    ("roadWeight", "rouSpinBox"),
    ("sirWeight", "sirSpinBox"),
    ("traWeight", "traSpinBox"),
    ("tiffs", "tiffsCheckBox"),
    ("snaps", "snapsCheckBox"),
    ("verbose", "verboseCheckBox")])


class QGISimUrba:
    def __init__(self, iface):
        self.iface = iface
        self.plugin_dir = os.path.dirname(__file__)
        self.actions = []
        self.menu = u'&QGISimUrba'
        self.dockwidget = QSimUrbaWidget(self)
        self.dockwidget.setObjectName('QGISimUrba')
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dockwidget)
        self.task_manager = QgsApplication.taskManager()
        self.logger = QgsMessageLog()
        self._prep_jobs = []
        self._simu_jobs = []

    def toggleDock(self):
        if self.dockwidget.isVisible():
            self.dockwidget.setVisible(False)
        else:
            self.dockwidget.setVisible(True)

    def initGui(self):
        text = 'Afficher/Masquer le panneau'
        icon = QIcon(':/plugins/QGISimUrba/icon.png')
        self.action = QAction(icon, text, self.iface.mainWindow())
        self.action.setEnabled(True)
        self.action.setStatusTip('')
        self.action.setWhatsThis(text)
        self.action.setObjectName('QGISimUrba')
        self.action.triggered.connect(self.toggleDock)
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(self.menu, self.action)

    def unload(self):
        self.iface.removePluginMenu('QGISimUrba', self.action)
        self.iface.removeToolBarIcon(self.action)
        del self.dockwidget


class QSimUrbaWidget(QDockWidget, Ui_QSimUrbaWidget):
    def __init__(self, parent):
        super().__init__()
        super(Ui_QSimUrbaWidget, self).__init__()
        self.parent = parent
        self.setupUi(self)
        self.weights = {}
        self.coeffs = {}
        self.computeCoefButton.clicked.connect(self._compute_coeffs)
        self.saveCoefButton.clicked.connect(self._save_weights)
        self.Simu_StartButton.clicked.connect(self._run_simulation)
        self.Simu_CancelButton.clicked.connect(self._cancel_all_simulations)
        self.Simu_RandomizeButton.clicked.connect(self._set_random_params)
        self._set_default_values()
        self._set_tool_tips()

    def _set_tool_tips(self):
        for param, obj_str in SIMU_QOBJECTS.items():
            tooltip = SIMU_SETTINGS[param][2]
            obj = getattr(self, obj_str)
            obj.setToolTip(tooltip)

    @staticmethod
    def _get_qtgui_function(goal, obj):

        func = ''
        if goal == 'set':
            if 'Widget' in obj:
                func = 'setText'
            elif 'SpinBox' in obj:
                func = 'setValue'
            elif 'Combo' in obj:
                func = 'setCurrentIndex'
            elif 'CheckBox' in obj:
                func = 'setChecked'
        elif goal == 'get':
            if 'Widget' in obj:
                func = 'text'
            elif 'SpinBox' in obj:
                func = 'value'
            elif 'Combo' in obj:
                func = 'currentText'
            elif 'CheckBox' in obj:
                func = 'isChecked'

        return func

    def _set_default_values(self):
        for param, values in SIMU_SETTINGS.items():
            t = values[0]
            v = values[1]
            if t == Path:
                v = str(v)
            if param == 'scenario':
                v = self._scenario_index(v)
            if param == 'fitTo':
                v = self._fit_level_index(v)
            if param == 'seed':
                v = -1
            obj_str = SIMU_QOBJECTS[param]
            obj = getattr(self, obj_str)
            func = getattr(obj, self._get_qtgui_function('set', obj_str))
            func(v)

    def _get_ui_params(self):
        d = {}
        for param, obj_str in SIMU_QOBJECTS.items():
            obj = getattr(self, obj_str)
            func = getattr(obj, self._get_qtgui_function('get', obj_str))
            value = func()
            if param == 'growth':
                value = round(value, 3)
            d[param] = value
        return d

    def _set_random_params(self):
        new_settings = random_scenario(Path(self.dataDirWidget.text()))
        for param, v in new_settings.items():
            t = SIMU_SETTINGS[param][0]
            if t == Path:
                v = str(v)
            if param == 'scenario':
                v = self._scenario_index(v)
            if param == 'fitTo':
                v = self._fit_level_index(v)
            obj_str = SIMU_QOBJECTS[param]
            obj = getattr(self, obj_str)
            func = getattr(obj, self._get_qtgui_function('set', obj_str))
            func(v)
        self._compute_coeffs()

        return True

    def _compute_coeffs(self):
        sum_weights = 0
        self.weights = {'ecologie': self.ecoSpinBox.value(),
                        'routes': self.rouSpinBox.value(),
                        'sirene': self.sirSpinBox.value(),
                        'transports': self.traSpinBox.value()}
        sum_weights = sum(self.weights.values())
        for k, v in self.weights.items():
            self.coeffs[k] = round(v / sum_weights, 5)
        self.sirLabelCoef.setText(str(round(self.coeffs['sirene'] * 100)) + ' %')
        self.traLabelCoef.setText(str(round(self.coeffs['transports'] * 100)) + ' %')
        self.rouLabelCoef.setText(str(round(self.coeffs['routes'] * 100)) + ' %')
        self.ecoLabelCoef.setText(str(round(self.coeffs['ecologie'] * 100)) + ' %')
        return self.coeffs

    def _save_weights(self):
        self.weights = {'ecologie': self.ecoSpinBox.value(),
                        'routes': self.rouSpinBox.value(),
                        'sirene': self.sirSpinBox.value(),
                        'transports': self.traSpinBox.value()}
        dataDir = Path(self.dataDirWidget.text())
        with (dataDir/'interet/poids.csv').open('w') as w:
            w.write('raster, poids\n')
            for k, v in self.weights.items():
                w.write(k + ', ' + str(v) + '\n')
        self.parent.logger.logMessage('Fichier des poids enregistré', 'SimUrba', Qgis.Info)
        return True

    def _run_simulation(self):
        already_exists = False
        params_dict = self._get_ui_params()
        self._save_weights()
        param_str = ''
        for v in params_dict.values():
            param_str += str(v) + ' '
        for task in self.parent._simu_jobs:
            if param_str == task._param_str:
                already_exists = True
                self.parent.iface.messageBar().pushMessage('Scénario est déjà en cours de calcul',
                                                           level=Qgis.Info)
        if not already_exists:
            task = QGISimUrbaTask('gui', param_str, self.parent)
            self.parent._simu_jobs.append(task)
            self.parent.task_manager.addTask(task)

    def _cancel_all_simulations(self):
        for task in self.parent._simu_jobs:
            task.cancel()

    @staticmethod
    def _scenario_index(scenario):
        if scenario == 'tendanciel':
            return 0
        elif scenario == 'stable':
            return 1
        elif scenario == 'reduction':
            return 2
        elif scenario == 'inf':
            return 3

    @staticmethod
    def _fit_level_index(level):
        if level == 'zone':
            return 0
        elif level == 'com':
            return 1
        elif level == 'iris':
            return 2


class QGISimUrbaTask(Simulation, QgsTask):
    def __init__(self, interface='qgs', param_str=None, parent=None):
        super().__init__(interface, param_str)
        super(QgsTask, self).__init__('Simu - ' + self._timestamp)
        self.parent = parent
        self.logger = self.parent.logger
        self.messageBar = self.parent.iface.messageBar()
        self.broken = False

    def run(self):
        """Redefinition de la fonction run de Simulation pour l'utiliser dans une QgsTask"""

        if not self._project_initialized:
            self.init_project()
        self.timings = {}
        self.beginTime = time()
        self._results_computed = False
        log_params = ''
        for k, v in self._settings.items():
            log_params += k + ' = ' + str(v) + '\n'
        self.logger.logMessage('Paramètres:\n' + str(log_params),
                               'SimUrba', Qgis.Info)

        preLog = 0
        preBuilt = 0
        c = 1
        self.setProgress(0)
        yearProgress = 100 / (self.finalYear - self.startYear)
        # Boucle principale pour itération annuelle
        for year in range(self.startYear, self.finalYear + 1):
            begin = time()
            self.currentYear = year
            progres = "Annee %i/%i" % (year, self.finalYear)
            self.logger.logMessage(progres, 'SimUrba', Qgis.Info)

            srfMax = self.dicSrf[year]
            popALoger = self.popDic[year]
            if self.verbose:
                self.logger.logMessage('Objectif demographique: ' + str(popALoger),
                                       'SimUrba', Qgis.Info)
                self.logger.logMessage('Seuil surface artif: ' + str(srfMax),
                                       'SimUrba', Qgis.Info)

            restePop, resteSrf = self._urbanisation(popALoger - preLog,
                                                    srfMax - preBuilt, use_zau=not self.skipZau)
            preBuilt = -resteSrf
            preLog = -restePop
            if self.verbose:
                self.logger.logMessage('Population restante : ' + str(restePop),
                                       'SimUrba', Qgis.Info)
                self.logger.logMessage('Surface a construire restante : ' + str(resteSrf),
                                       'SimUrba', Qgis.Info)
            self.timings[year] = time() - begin
            # Snapshots
            if self.tiffs and self.snaps:
                to_tiff(self.srfBatiPla, 'uint16', self.proj, self.geot,
                        self.project/('snapshots/surface_plancher/srf_pla_' + str(year) + '.tif'),
                        self.mask, self._debug)
                to_tiff(self.txArtif, 'uint16', self.proj, self.geot,
                        self.project/('snapshots/artificialisation/artif_' + str(year) + '.tif'),
                        self.mask, self._debug)

            # Pour arrêter la tâche en cas de threading
            if self.isCanceled():
                self.broken = True
                break
            self.setProgress(c * yearProgress)
            c += 1

        if not self.broken:
            if resteSrf == inf:
                self.resteSrf = resteSrf
            elif resteSrf > 0:
                self.resteSrf = int(round(resteSrf))
            else:
                self.resteSrf = 0
            self.restePop = int(restePop) if restePop > 0 else 0
            self.endTime = time()
            self.execTime = round(self.endTime - self.beginTime, 2)
            self.logger.logMessage('Temps écoulé: ' + str(self.execTime) + ' seconds',
                                   'SimUrba', Qgis.Info)
            self._completed = True
            return self.compute_results(self.project)
        else:
            return False

    def cancel(self):
        super().cancel()
        self.logger.logMessage('Annulation de la simulation', 'SimUrba', level=Qgis.Info)

    def finished(self, result):
        if result:
            self.messageBar.pushMessage('Simulation terminée. Temps écoulé : '
                                        + str(self.execTime) + ' secondes',
                                        level=Qgis.Success)
            self.logger.logMessage('Simulation terminée. Temps écoulé : '
                                   + str(self.execTime) + ' secondes',
                                   'SimUrba', Qgis.Success)
            self.compute_results()
        else:
            if not self.broken:
                self.messageBar.pushMessage('Échec de la simulation',
                                            level=Qgis.Critical)
                self.logger.logMessage('Échec de la simulation', 'SimUrba', Qgis.Critical)
            else:
                self.logger.logMessage('Simulation annulée', 'SimUrba', Qgis.Info)
