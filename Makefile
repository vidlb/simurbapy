# -*- coding: utf-8 -*-
# """
# /***************************************************************************
#  QGISimUrba
#
#  Simulation de l'urbanisation pour la planification de la séquence ERC
#
#         copyright            : (C) 2018 by Vincent DELBAR
#
# """
LOCALES = fr

PLUGINNAME = QGISimUrba

PY_FILES = gui/__init__.py \
		   gui/qgisimurba.py \
		   simurba/simu.py \
		   simurba/prep.py \
		   simurba/expr.py \
		   simurba/fit.py \
		   simurba/tools.py \

UI_FILE = gui/qsimurbawidget.ui

PYUI_FILE = gui/widget.py

EXTRAS = metadata.txt icon.png

COMPILED_RESOURCE_FILES = gui/resources.py

PEP8EXCLUDE = pydev,qgis/resources.py,conf.py,third_party,ui

HELP = help/build/html

PLUGIN_UPLOAD = $(c)/scripts/plugin_upload.py

RESOURCE_SRC = $(shell grep '^ *<file'  | sed 's@</file>@@g;s/.*>//g' | tr '\n' ' ')

QGISDIR = .local/share/QGIS/QGIS3/profiles/default

default: deploy

compile: $(COMPILED_RESOURCE_FILES)

%.py : %.qrc $(RESOURCES_SRC)
	pyrcc5 -o $*.py  $<

%.qm : %.ts
	$(LRELEASE) $<

deploy: derase compile uic
	@echo
	@echo "------------------------------------------"
	@echo "Deploying plugin to your QGIS 3 directory."
	@echo "------------------------------------------"
	mkdir -p $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vf $(PY_FILES) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vf $(PYUI_FILE) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vf $(COMPILED_RESOURCE_FILES) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	cp -vf $(EXTRAS) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)
	# cp -vfr $(HELP) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)/help

dclean:
	@echo
	@echo "-----------------------------------"
	@echo "Removing any compiled python files."
	@echo "-----------------------------------"
	find $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME) -iname "*.pyc" -delete
	find $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME) -iname ".git" -prune -exec rm -Rf {} \;

derase:
	@echo
	@echo "-------------------------"
	@echo "Removing deployed plugin."
	@echo "-------------------------"
	rm -Rf $(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)

zip: deploy dclean
	@echo
	@echo "---------------------------"
	@echo "Creating plugin zip bundle."
	@echo "---------------------------"
	rm -f $(PLUGINNAME).zip
	cd $(HOME)/$(QGISDIR)/python/plugins; zip -9r $(CURDIR)/$(PLUGINNAME).zip $(PLUGINNAME)

upload: zip
	@echo
	@echo "-------------------------------------"
	@echo "Uploading plugin to QGIS Plugin repo."
	@echo "-------------------------------------"
	$(PLUGIN_UPLOAD) $(PLUGINNAME).zip

clean:
	@echo
	@echo "------------------------------------"
	@echo "Removing uic and rcc generated files"
	@echo "------------------------------------"
	rm $(COMPILED_UI_FILE) $(COMPILED_RESOURCE_FILES)

doc:
	@echo
	@echo "------------------------------------"
	@echo "Building documentation using sphinx."
	@echo "------------------------------------"
	cd help; make html

pylint:
	@echo
	@echo "-----------------"
	@echo "Pylint violations"
	@echo "-----------------"
	@pylint3 --reports=n --rcfile=pylintrc qgis simurba || true


pep8:
	@echo
	@echo "-----------"
	@echo "PEP8 issues"
	@echo "-----------"
	@pep8 --repeat --ignore=E203,E121,E122,E123,E124,E125,E126,E127,E128 --exclude $(PEP8EXCLUDE) . || true
	@echo "-----------"
	@echo "Ignored in PEP8 check:"
	@echo $(PEP8EXCLUDE)

uic:
	@echo
	pyuic5 $(UI_FILE) > $(PYUI_FILE)
