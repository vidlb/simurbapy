import _file_.base_var._

// val pythonTask = CARETask(workDirectory / "simurba.tgz.bin",
val pythonTask=ContainerTask(
    DockerImage(image = "vidlb/simurbapy", tag = "develop", registry = "https://registry.gitlab.com"),
    """oms_run.py inputs/${pixRes} output ${growth} ${finalYear} ${scenario} ${pluPriority} ${buildNonRes} ${exclusionRatio} ${maxBuiltRatio} ${maxUsedSrfPla} ${densifyOld} ${winSize} ${minContig} ${maxContig} ${roadRatio} ${fitTo} ${seed} ${ecoWeight} ${roadWeight} ${sirWeight} ${traWeight} ${ocsWeight} ${tiffs} ${snaps} ${verbose}"""
    ) set (
    pixRes := "60m",
    //dataDir := workDirectory / "inputs",
    dataDir := workDirectory / "inputs" / "60m",
    //growth := 0.1,
    finalYear := 2040.0,
    scenario := 0.5,
    pluPriority := 1.0,
    buildNonRes := 1.0,
    //exclusionRatio := 0.2,
    //maxBuiltRatio := 0.8,
    //maxUsedSrfPla := 200.0,
    densifyOld := 1.0,
    //winSize := 5.0,
    //minContig := 0.1,
    //maxContig := 0.9,
    roadRatio := 0.0,
    fitTo := 2.0,
    seed := -1,
    //ecoWeight := 1.0,
    //roadWeight := 2.0,
    //sirWeight := 4.0,
    //traWeight := 3.0,
    tiffs := "False",
    snaps := "False",
    verbose := "False",
    //inputs += (growth, finalYear, scenario, pluPriority, buildNonRes, exclusionRatio, maxBuiltRatio, maxUsedSrfPla, densifyOld, winSize, minContig, maxContig, roadRatio, fitTo, seed, ecoWeight, roadWeight, sirWeight, traWeight),
    inputs += (growth, scenario, pluPriority, buildNonRes, exclusionRatio, maxBuiltRatio, maxUsedSrfPla, densifyOld, winSize, minContig, maxContig, ecoWeight, roadWeight, sirWeight, traWeight, ocsWeight),
    outputs += (growth, scenario, pluPriority, buildNonRes, exclusionRatio, maxBuiltRatio, maxUsedSrfPla, densifyOld, winSize, minContig, maxContig, ecoWeight, roadWeight, sirWeight, traWeight, ocsWeight),
    //outputs += (growth, finalYear, scenario, pluPriority, buildNonRes, exclusionRatio, maxBuiltRatio, maxUsedSrfPla, densifyOld, winSize, minContig, maxContig, roadRatio, fitTo, seed, ecoWeight, roadWeight, sirWeight, traWeight),
    //inputFiles += (dataDir, "inputs"),
    inputFiles += (dataDir, "inputs/60m"),
    outputFiles += ("output/measures.csv", measures)
)

val csvHook = AppendToCSVFileHook(workDirectory / "results" / "saltelli_100" / "sensitivity.csv")
val anotherHook = CSVHook(workDirectory / "results" / "saltelli_100" / "all.csv", values = Seq(
        growth, scenario, pluPriority, buildNonRes, exclusionRatio, maxBuiltRatio, maxUsedSrfPla, densifyOld, winSize, minContig, maxContig, ecoWeight, roadWeight, sirWeight, traWeight,
        ocsWeight, restePop, resteSrf, artifNouvSum, srfBatiSolNouvSum, srfBatiPlaNouvSum, expansionSum, impactEnvCum, txArtifMoyen, dsfBatiPlaSum, m2SolHabFinal, skipZauYear))

val env = LocalEnvironment(4)
//val env = SLURMEnvironment("debian", "134.158.74.226", workDirectory = "/tmp", memory = 2 gigabytes)//avec NFS
// Pour regarder l'exécution : http://134.158.74.226/ganglia/

val moTask = MoleTask(pythonTask -- (extract set (
    inputs += measures,
    inputs += (growth, scenario, pluPriority, buildNonRes, exclusionRatio, maxBuiltRatio, maxUsedSrfPla, densifyOld, winSize, minContig, maxContig, ecoWeight, roadWeight, sirWeight, traWeight, ocsWeight),
    outputs += (restePop, resteSrf, artifNouvSum, srfBatiSolNouvSum, srfBatiPlaNouvSum, expansionSum, impactEnvCum, txArtifMoyen, dsfBatiPlaSum, m2SolHabFinal, skipZauYear),
    outputs += (growth, scenario, pluPriority, buildNonRes, exclusionRatio, maxBuiltRatio, maxUsedSrfPla, densifyOld, winSize, minContig, maxContig, ecoWeight, roadWeight, sirWeight, traWeight, ocsWeight)
))) hook anotherHook

val salt = SensitivitySaltelli(
  evaluation = moTask on env,
  inputs = Seq(

        growth in (0.01, 1.0),
        exclusionRatio in (0.0, 0.2),
        maxBuiltRatio in (50.0, 100.0),
        densifyOld in (0.0, 1.0),
        winSize in (2.5, 7.5),
        minContig in (0.0, 0.3),
        maxContig in (0.7, 1.0),
        
        ecoWeight in (0.0, 10.0),
        roadWeight in (0.0, 10.0),
        sirWeight in (0.0, 10.0),
        traWeight in (0.0, 10.0)
  ),
  outputs = Seq(srfBatiPlaNouvSum, impactEnvCum),
  samples = 100
) hook csvHook

salt
