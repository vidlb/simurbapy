# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Fitter

    Classe pour ajuster des distributions en multiprocessing

        copyright            : (C) 2018 by Vincent DELBAR

"""
import os
import warnings
import numpy as np
import pandas as pd
import scipy.stats as st
import multiprocessing as mp
from time import sleep, strftime, time
from datetime import datetime

from .tools import get_exec_time

warnings.filterwarnings('ignore')
np.seterr(divide='ignore', invalid='ignore')

DISTRIB_BLACKLIST = ['rv_continuous', 'rv_histogram', 'foldcauchy']
# DISTRIBS = [d for d in dir(st) if 'fit' in dir(getattr(st, d))]
DISTRIBS = ['alpha', 'anglit', 'arcsine', 'betaprime', 'burr', 'burr12',
            'cauchy', 'cosine', 'dgamma', 'expon', 'exponnorm', 'exponweib',
            'f', 'fatiguelife', 'fisk', 'genexpon', 'genextreme',
            'gengamma', 'genhalflogistic', 'genlogistic', 'gilbrat', 'gumbel_l',
            'gumbel_r', 'halfcauchy', 'halfgennorm', 'halfnorm', 'hypsecant',
            'invgamma', 'invgauss', 'invweibull', 'johnsonsb', 'johnsonsu',
            'kappa3', 'kstwobign', 'laplace', 'levy', 'levy_l', 'logistic',
            'maxwell', 'moyal', 'norm', 'norminvgauss', 'pearson3', 'rayleigh',
            'recipinvgauss', 'semicircular', 'skewnorm', 't', 'uniform', 'wald']
DISTRIBUTIONS = [d for d in DISTRIBS if d not in DISTRIB_BLACKLIST]


class Fitter:
    def __init__(self,
                 data: pd.Series,
                 binw: int,
                 nature: str,
                 distributions: list = DISTRIBUTIONS,
                 verbose: bool = True,
                 nb_cores: int = 1,
                 use_ks: bool = False):
        """Objet pour ajuster des distributions."""

        self.data = data
        self.binw = binw
        self.nature = nature
        self.nb_cores = nb_cores
        if nb_cores > 1:
            self._multiproc = True
        else:
            self._multiproc = False
        self.verbose = verbose
        self.use_ks = use_ks
        self.plot = None
        self.results = None
        self._dump = []
        self._distributions = {}
        for d in distributions:
            dist_func = dir(getattr(st, d))
            if "fit" in dist_func:
                self._distributions[d] = 'continuous_fit'
            elif "pdf" in dist_func:
                # self._distributions[d] = 'continuous_pdf'
                pass
            elif "pmf" in dist_func:
                # self._distributions[d] = 'discrete_pmf'
                pass
            else:
                self._dump.append(d)
        self._max_v = int(data.max()) + 1
        bins = int(self._max_v / binw)
        self._bins = bins
        y, x = np.histogram(data, bins=bins, density=True)
        x = (x + np.roll(x, -1))[:-1] / 2.0
        self._x, self._y = x, y
        self._n = len(data)

        if verbose:
            print('\nFitter :\nname=' + self.nature + '\nn=' + str(self._n) + '\n')
            # str_dist = str(list(self._distributions.keys()))
            print(str(len(self._distributions)) + ' distributions to fit.\n')

    def run(self, distributions=None):

        self.beginT = time()
        if distributions is None:
            distributions = self._distributions
        jobs = []
        pool = mp.Pool(self.nb_cores)
        sender, receiver = mp.Pipe()
        self.results = []
        if self.use_ks:
            self.ks_results = []
        if self.verbose:
            s = '\nBegin fitting with {} core(s) at {}'
            print(s.format(self.nb_cores, strftime("%Hh%Mm%Ss")))
            sleep(0.01)

        for name, dtype in distributions.items():
            if dtype == 'continuous_fit':
                jobs.append(pool.apply_async(self.cont_fit, (name, sender)))
            else:
                pass  # TODO : discrete distrib

        pool.close()
        pool.join()

        for j in range(len(jobs)):
            res, ks_res = receiver.recv()
            if res:
                self.results.append(res)
            if ks_res:
                self.ks_results.append(ks_res)

        pool.terminate()
        self.endT = time()

        if self.verbose:
            print('\nEnd with ' + str(len(self.results)) + ' fitted distributions')
            # print('Dump :\n' + str(self._dump))
            print('Total elapsed time : ' + get_exec_time(self.beginT) + '\n')

        if self.use_ks:
            sort = sorted(self.ks_results, key=lambda tup: tup[1][0], reverse=True)
            self.ks_results = sort
        return self.results

    def cont_fit(self, name, pipe):

        if self.verbose and not self._multiproc:
            print('\nFitting ' + name + ' :')
        distrib = getattr(st, name)
        params = None
        results = None
        ks_results = None

        try:
            started_at = datetime.now()
            with warnings.catch_warnings():
                params = distrib.fit(self.data)
            ended_at = datetime.now()
            diff = ended_at - started_at
        except (TypeError, NotImplementedError):
            if self.verbose and not self._multiproc:
                print('*** Error ***')
            # self._dump.append(name)
        except KeyboardInterrupt:
            raise

        if not params:
            pipe.send((None, None))
            return None

        k = len(params)
        lines = []
        if self.use_ks:
            D, p = st.kstest(self._y, name, args=params)
            ks_results = (name, [p, D])
            lines.append('KS: p = ' + str(p) + '  >  D = ' + str(D) + ' : ' + str(p > D))

        # Computing Probability Density Function and LikeLiHood
        args, loc, scale = self._split_params(params)
        PDF = distrib.pdf(self._x, loc=loc, scale=scale, *args)
        SSE = np.sum(np.power(self._y - PDF, 2.0))
        LLH = distrib.logpdf(self._y, loc=loc, scale=scale, *args).sum()
        AIC = 2 * k - 2 * LLH
        BIC = k * np.log(self._n) - 2 * LLH
        scores = [SSE, LLH, AIC, BIC]

        # Printing results
        s = 'Elapsed time: {}\n  SSE={} LLH={}\n  AIC={} BIC={}'
        lines.append(s.format(diff, SSE, LLH, AIC, BIC))

        if self.verbose and not self._multiproc:
            for li in lines:
                print(li)

        results = (name, (params, scores))
        pipe.send((results, ks_results))

    def plot_n_best(self, n=10, crit='sse', ax=None, bins=0, max_x=0, max_y=0):
        if not self._max_v:
            return None
        computed = True
        if not self.results:
            computed = self.run(self._distributions)
        if not computed:
            return False
        self._sort_results(crit)
        results = dict(self.results)

        if bins <= 0:
            bins = self._bins
        if max_x <= 0:
            max_x = int(self._max_v / 2)
        if max_y <= 0:
            max_dens = round(self._y.max(), 2)
            max_y = max_dens + 0.001 if max_dens > 0 else 0.01
        if ax is None:
            ax = self.data.plot(kind='hist', bins=bins, density=True)

        ax.set_xlim(left=0, right=max_x)
        ax.set_ylim(0, max_y)
        ax.set_xlabel('Buildings ' + self.nature)
        ax.figure.set_size_inches((14, 8))
        abr_indexes = {
            'sse': 'Sum of Sqrt Errors',
            'llh': 'Log-Likelihood',
            'aic': 'Akaike Information Criterion',
            'bic': 'Bayes Information Criterion'}
        crit_desc = abr_indexes[crit]
        ax.set_title(str(n) + ' best fittings with ' + crit_desc + ' criterion')

        i = 0
        for distrib_name, tup in results.items():
            distrib = getattr(st, distrib_name)
            params = tup[0]
            pdf_serie = self._make_pdf(distrib, params, bins=bins)
            pdf_serie.plot(lw=2, label=str(i + 1) + ': ' + distrib_name, legend=True, ax=ax)
            i += 1
            if i == n:
                return ax

    def _sort_results(self, crit='sse'):
        sort_tricks = {'sse': [0, False], 'llh': [1, True], 'aic': [2, False], 'bic': [3, False]}
        if not self.results:
            return False
        key, is_reverse = sort_tricks[crit][0], sort_tricks[crit][1]
        sort = sorted(self.results, key=lambda tup: tup[1][1][key], reverse=is_reverse)
        self.results = sort
        return True

    def write_results(self, outdir, plot=None):
        if not outdir.exists():
            os.makedirs(str(outdir))
        if plot:
            plot.figure.savefig(str(outdir/(self.nature + '_plot.png')))
        with open(str(outdir) + '/' + self.nature + '_fittings.csv', 'w') as w:
            for l in self.results:
                w.write(str(l) + '\n')

    @staticmethod
    def _split_params(params):
        args = params[:-2]
        loc = params[-2]
        scale = params[-1]
        return args, loc, scale

    def _make_pdf(self, distrib, params, bins):
        args, loc, scale = self._split_params(params)
        if args:
            start = distrib.ppf(0.01, *args, loc=loc, scale=scale)
            end = distrib.ppf(0.99, *args, loc=loc, scale=scale)
        else:
            start = distrib.ppf(0.01, loc=loc, scale=scale)
            end = distrib.ppf(0.99, loc=loc, scale=scale)

        x = np.linspace(start, end, bins)
        y = distrib.pdf(x, loc=loc, scale=scale, *args)
        return pd.Series(y, x)
