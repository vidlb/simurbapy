# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Expressions et constantes



        copyright            : (C) 2018 by Vincent DELBAR

"""
# Preparation
MP_DEPTS = ['09', '12', '31', '32', '46', '65', '81', '82']

LR_DEPTS = ['11', '30', '34', '48', '66']

STATS_BLACKLIST = ['count', 'unique', 'min', 'max', 'range', 'sum', 'mean',
                   'median', 'stddev', 'minority', 'majority', 'q1', 'q3', 'iqr']

AGGR_IRIS = {
    'AGGREGATES': [
        {'aggregate': 'first_value', 'delimiter': ',', 'input': '"INSEE_COM"', 'length': 5,
         'name': 'INSEE_COM', 'precision': 0, 'type': 10},
        {'aggregate': 'first_value', 'delimiter': ',', 'input': '"NOM_COM"', 'length': 50,
         'name': 'NOM_COM', 'precision': 0, 'type': 10},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP06"', 'length': 10,
         'name': 'POP06', 'precision': 0, 'type': 4},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP07"', 'length': 10,
         'name': 'POP09', 'precision': 0, 'type': 4},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP07"', 'length': 10,
         'name': 'POP09', 'precision': 0, 'type': 4},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP08"', 'length': 10,
         'name': 'POP08', 'precision': 0, 'type': 4},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP09"', 'length': 10,
         'name': 'POP09', 'precision': 0, 'type': 4},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP10"', 'length': 10,
         'name': 'POP10', 'precision': 0, 'type': 4},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP12"', 'length': 10,
         'name': 'POP12', 'precision': 0, 'type': 4},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP14"', 'length': 10,
         'name': 'POP14', 'precision': 0, 'type': 4},
        {'aggregate': 'sum', 'delimiter': ',', 'input': '"POP15"', 'length': 10,
         'name': 'POP15', 'precision': 0, 'type': 4}],
    'GROUP_BY': '"INSEE_COM"'}

OSO_CLASSES = ['Hiver', 'Ete', 'Feuillus', 'Coniferes', 'Pelouse', 'Landes',
               'UrbainDens', 'UrbainDiff', 'ZoneIndCom', 'Route', 'PlageDune',
               'SurfMin', 'Eau', 'GlaceNeige', 'Prairie', 'Vergers', 'Vignes', 'Aire']

BDT_BUILDS = [
    'E_BATI/BATI_INDIFFERENCIE.SHP',
    'E_BATI/BATI_INDUSTRIEL.SHP',
    'E_BATI/BATI_REMARQUABLE.SHP',
    'E_BATI/CONSTRUCTION_SURFACIQUE.SHP',
    'E_BATI/PISTE_AERODROME.SHP',
    'E_BATI/RESERVOIR.SHP',
    'E_BATI/TERRAIN_SPORT.SHP']

BDT_ROADS = [
    'A_RESEAU_ROUTIER/ROUTE.SHP',
    'A_RESEAU_ROUTIER/SURFACE_ROUTE.SHP']

BDT_SERV = [
    'I_ZONE_ACTIVITE/PAI_ADMINISTRATIF_MILITAIRE.SHP',
    'I_ZONE_ACTIVITE/PAI_CULTURE_LOISIRS.SHP',
    # 'I_ZONE_ACTIVITE/PAI_ESPACE_NATUREL.SHP',
    'I_ZONE_ACTIVITE/PAI_INDUSTRIEL_COMMERCIAL.SHP',
    'I_ZONE_ACTIVITE/PAI_RELIGIEUX.SHP',
    'I_ZONE_ACTIVITE/PAI_SANTE.SHP',
    'I_ZONE_ACTIVITE/PAI_SCIENCE_ENSEIGNEMENT.SHP',
    'I_ZONE_ACTIVITE/PAI_SPORT.SHP',
    'I_ZONE_ACTIVITE/PAI_TRANSPORT.SHP']

OLD_BDT_BUILDS = [
    'E_BATI/BATIMENT.SHP',
    'E_BATI/CIMETIERE.SHP',
    'E_BATI/CONSTRUCTION_SURFACIQUE.SHP',
    'E_BATI/PISTE_AERODROME.SHP',
    'E_BATI/RESERVOIR.SHP',
    'E_BATI/TERRAIN_SPORT.SHP']

OLD_BDT_ROADS = [
    'A_VOIES_COMM_ROUTE/TRONCON_ROUTE.SHP',
    'A_VOIES_COMM_ROUTE/SURFACE_ROUTE.SHP']

ROADS = """ "NATURE" NOT IN ('Chemin' , 'Escalier' , 'Sentier', 'Piste cyclable')
        AND "NATURE" NOT LIKE 'Bac pi%ton' AND "ETAT" != 'En construction' """

ROADS_WIDTH = """IF ("LARGEUR" = 0, 2, 1 + ("LARGEUR" / 2))"""
SIMPLE_OSO = """
    CASE WHEN to_string("Classe") LIKE '1%' or to_string("Classe") LIKE '2%'
        THEN 'ESPACE AGRICOLE'
    WHEN to_string("Classe") LIKE '3%' or "Classe"=45 or "Classe"=46 or "Classe"=53
        THEN 'ESPACE NATUREL'
    WHEN "Classe"=41 or "Classe"=42 or "Classe"=43 or "Classe"=44
        THEN 'ESPACE URBANISE'
    WHEN "Classe"=53
        THEN 'SURFACE EN EAU'
    ELSE NULL END
"""
SIMPLE_OCS_3M = """
    CASE
        WHEN "lib15_niv1" = 'EAU'
            THEN 'SURFACE EN EAU'
        WHEN "lib15_niv1" = 'ESPACES AGRICOLES'
            THEN 'ESPACE AGRICOLE'
        WHEN "lib15_niv1" = 'ESPACES BOISES'
            THEN 'ESPACE NATUREL'
        WHEN "lib15_niv1" = 'ESPACES NATURELS NON BOISES'
            THEN 'ESPACE NATUREL'
        WHEN "lib15_niv1" = 'ESPACES RECREATIFS'
            THEN 'AUTRE'
        WHEN "lib15_niv1" = 'ESPACES URBANISES'
            THEN 'ESPACE URBANISE'
        WHEN "lib15_niv1" LIKE 'EXTRACTION DE MATERIAUX%'
            THEN 'AUTRE'
        WHEN "lib15_niv1" LIKE 'SURFACES INDUSTRIELLES OU COM%'
            THEN 'ESPACE URBANISE'
        ELSE 0 END
"""
SIMPLE_OCS_NIM = """
    CASE
        WHEN "NIV1_12" = 1 THEN 'ESPACE URBANISE'
        WHEN "NIV1_12" = 2 THEN 'ESPACE AGRICOLE'
        WHEN "NIV1_12" = 3 THEN 'ESPACE NATUREL'
        WHEN "NIV1_12" = 4 THEN 'ESPACE NATUREL'
        WHEN "NIV1_12" = 5 THEN 'SURFACE EN EAU'
        ELSE 0 END
"""
SIMPLE_CLC = """
    CASE
        WHEN "CODE_12" LIKE '1__' THEN 'ESPACE URBANISE'
        WHEN "CODE_12" LIKE '2__' THEN 'ESPACE AGRICOLE'
        WHEN "CODE_12" LIKE '3__' THEN 'ESPACE NATUREL'
        WHEN "CODE_12" LIKE '4__' THEN 'ESPACE NATUREL'
        WHEN "CODE_12" LIKE '5__' THEN 'SURFACE EN EAU'
        ELSE 0
    END
"""
OCS_INTEREST = """
    CASE
        WHEN "classe_2" = 'ESPACE URBANISE' THEN 1
        WHEN "classe_2" = 'ESPACE AGRICOLE' THEN 0.6
        WHEN "classe_2" = 'ESPACE NATUREL' THEN 0.3
        WHEN "classe_2" = 'SURFACE EN EAU' THEN 0
        WHEN "classe_2" = 'AUTRE' THEN 0
        ELSE 0 END
"""
SIMPLE_PLU = """
    CASE
        WHEN "type" LIKE '%AU%' THEN 'AU'
        WHEN "type" LIKE '%N%' THEN 'N'
        WHEN "type" LIKE '%U%' AND "type" NOT LIKE '%AU%' THEN 'U'
        WHEN "type" LIKE 'A%' AND "type" NOT LIKE 'AU%' THEN 'A'
        WHEN "type" = 'ZAC' THEN 'ZAC'
        ELSE '0' END
"""
RESTRICT_PLU = """
    CASE
        WHEN "type" = 'Ns' THEN 1
        WHEN "type" = 'N' THEN 1
        WHEN "classe" = 'A' THEN 1
        WHEN "classe" = 'ZAC' THEN 1
        ELSE 0 END
"""
PLU_PRIO = """
    CASE
        WHEN "classe" = 'AU' THEN 1
        ELSE 0 END
"""
RESTRICT_PLU_3M = """
    IF ("coment" LIKE '% _ prot_ger%'
    OR "coment" LIKE 'Coupures%'
    OR "coment" LIKE 'p_rim_tre protection %'
    OR "coment" LIKE 'protection forte %'
    OR "coment" LIKE 'sauvegarde de sites naturels, paysages ou _cosyst_mes'
    OR "coment" LIKE '% terrains r_serv_s %'
    OR "coment" LIKE '% prot_g_e'
    OR "coment" LIKE '% construction nouvelle est interdite %'
    OR "coment" LIKE '% protection contre risques naturels', 1, 0)
"""
PLU_PRIO_3M = """
    IF ("type" LIKE '%AU%'
    OR "coment" LIKE '%urbanisation future%'
    OR "coment" LIKE '%ouvert_ _ l_urbanisation%'
    OR "coment" LIKE '% destin_e _ l_urbanisation%', 1, 0)
"""
SPLIT_SIRENE = """ CASE
    WHEN "CODE_NAF1" = 'O' THEN 'administratif'
    WHEN
        "CODE_NAF1" = 'G'
        OR "CODE_NAF1" = 'I'
        OR "CODE_NAF1" = 'J'
        OR "CODE_NAF1" = 'K'
        OR "CODE_NAF1" = 'L'
        OR "CODE_NAF1" = 'S'
    THEN 'commercial'
    WHEN "CODE_NAF1" = 'P' THEN 'enseignement'
    WHEN "CODE_NAF1" = 'R' THEN 'recreatif'
    WHEN "CODE_NAF1" = 'Q' THEN 'medical'
    ELSE 'autre' END
"""
