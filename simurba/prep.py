# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Preparation

    Traitement des donnees pour la simulation de l'urbanisation

        copyright            : (C) 2018 by Vincent DELBAR

"""
import os
import re
import sys
import csv
import gdal
import decimal
import warnings
import numpy as np
import pandas as pd
from pathlib import Path
from time import strftime, time
from multiprocessing import Pool
from collections import OrderedDict
from skimage.measure import block_reduce
from shutil import rmtree, copyfile, copytree, move, ignore_patterns

from . import expr
from .fit import Fitter
from .tools import (
    get_bdt_metadata,
    get_env,
    get_exec_time,
    get_shapefiles,
    printer,
    rasterize,
    set_parameters,
    to_tiff,
    to_nparray)

QGS_ROOT, PYQGIS_DIR, BIG_RAM, TMPFS, TRUE_TMP = get_env()
if not QGS_ROOT:
    sys.exit("Can't find QGIS.")

sys.path.append(str(Path(PYQGIS_DIR)/'plugins'))
sys.path.append(PYQGIS_DIR)

from qgis.core import (
    QgsApplication,
    QgsCoordinateTransform,
    QgsCoordinateTransformContext,
    QgsCoordinateReferenceSystem,
    QgsField,
    QgsProcessingFeedback,
    QgsProcessingException,
    QgsRectangle,
    QgsTask,
    QgsVectorFileWriter,
    QgsVectorLayerJoinInfo,
    QgsVectorLayer
)
from qgis.analysis import QgsNativeAlgorithms
from PyQt5.QtCore import QVariant
import processing
from processing.core.Processing import Processing

qgs = QgsApplication([b''], False)
qgs.initQgis()
qgs.setPrefixPath(str(QGS_ROOT))
Processing.initialize()
qgs.processingRegistry().addProvider(QgsNativeAlgorithms())
feedback = QgsProcessingFeedback()

HOME = Path.home()
NB_CORES = int(os.cpu_count())
SETTINGS = OrderedDict([
    ("globalData", [Path, HOME/'SimUrba/global_data', 'Dossier contenant la donnee regionale']),
    ("localData", [Path, HOME/'SimUrba/local_data/mtp', 'Dossier contenant la donnee en entree']),
    ("outputDir", [Path, HOME/'SimUrba/zooms', 'Dossier où enregistrer les donnees preparees']),
    ("dpt", [str, '34', 'Departements de la zone etudiee separes par une virgule']),
    ("pixRes", [int, 30, "Resolution de la grille"]),
    ("bufferDist", [int, 1000, "Tampon utilise pour extraire des points autour de la zone"]),
    ("minSurf", [int, 40, "Surface au sol minimale pour considerer un batiment comme habitable"]),
    ("maxSurf", [int, 8000, "Surface au sol maximale pour considerer un batiment comme habite"]),
    ("useTxrp", [bool, True, "Utiliser le taux de residence principale pour estimer le plancher"]),
    ("levelHeight", [int, 3, "Hauteur theorique d'un etage pour estimer le nombre de niveaux"]),
    ("roadDist", [int, 300, "Pour la creation des rasters de distance aux routes"]),
    ("transDist", [int, 500, "Pour la creation des rasters de distance aux transports en commun"]),
    ("maxSlope", [int, 30, "Seuil de pente en % pour interdiction a la construction"]),
    ("fitLevels", [str, 'zone,com,iris', "Niveaux d'aggregation des donnees du fitting"]),
    ("force", [bool, False, "Pour ecraser le dossier de sortie au debut du processus"]),
    ("speed", [bool, True, "Utiliser le multiprocessing (pour le fitting)"]),
    ("wisdom", [bool, True, "Conserver les vecteurs pour construire un projet complet par zone"])
])


class Preparation:

    def __init__(self, interface='cli', param_str=None, crs='EPSG:3035', debug=True):

        qgs.initQgis()
        self._timestamp = strftime("%Y%m%d%H%M%S")
        if interface == 'cli':
            print('\n\n********** SimUrba - Preparation **********\n\n__init__():\n')
        self._debug = debug
        self._interface = interface
        if interface != 'cli':
            self._debug = False
        self._param_str = param_str
        self.full_tmpfs = False

        # Attribution des valeurs par defaut et verification des paramètres utilisateur
        self._settings = set_parameters(SETTINGS, interface, param_str)
        self._check_settings()

        self.globalData = self._settings['globalData']
        self.localData = self._settings['localData']
        self.outputDir = self._settings['outputDir']
        self.dpt = self._settings['dpt'].split(',')
        self.pixRes = self._settings['pixRes']
        self.bufferDist = self._settings['bufferDist']
        self.minSurf = self._settings['minSurf']
        self.maxSurf = self._settings['maxSurf']
        self.useTxrp = self._settings['useTxrp']
        self.levelHeight = self._settings['levelHeight']
        self.roadDist = self._settings['roadDist']
        self.transDist = self._settings['transDist']
        self.maxSlope = self._settings['maxSlope']
        self.fitLevels = self._settings['fitLevels'].split(',')
        self.force = self._settings['force']
        self.speed = self._settings['speed']
        self.wisdom = self._settings['wisdom']

        if interface == 'cli':
            for p in self._settings.keys():
                print('    ' + p + ' = ' + str(self.__getattribute__(p)))

        # Autres attributs
        self._data_to_fit = None
        self._use_biotope_data = False
        # if self.studyAreaName in ['fig', 'nim', 'prp', 'pyr']:
        if self.studyAreaName in ['fig', 'mtp', 'nim', 'prp', 'pyr', 'tls']:
            self._use_biotope_data = True

        self._log_file = None
        self._qgs_log_file = None
        # QgsObjects
        # self._qgs_project = QgsProject().instance()
        self._task_manager = qgs.taskManager()
        # Redirection des logs de QGIS
        qgs.messageLog().messageReceived.connect(self._write_qgs_log)

        # Attributs geographiques
        self.extent = None
        self.proj = None
        self.geot = None
        self.rows = None
        self.cols = None
        self.crs = crs
        self.pluPrio = None
        self.maxSlope = np.tanh(self.maxSlope/100) * 180 / np.pi  # % -> °

        # Chemins
        self._find_bdtopo()
        self.project = None
        self.pixResDir = None
        if self._mono_dpt:
            if self.dpt[0] in expr.LR_DEPTS:
                self.reg = ['R91']
            elif self.dpt[0] in expr.MP_DEPTS:
                self.reg = ['R73']
        else:
            self.reg = []
            for dpt in self.dpt:
                if dpt in expr.LR_DEPTS:
                    self.reg.append('R91') if 'R91' not in self.reg else None
                elif dpt in expr.MP_DEPTS:
                    self.reg.append('R73') if 'R73' not in self.reg else None

        # truth == not wisdom : on ne conserve pas la donnee intermedaire
        if self.wisdom:
            self.workspace = self.outputDir/self.studyAreaName
            self.project = self.workspace/'inputs'/(str(self.pixRes) + 'm')
        else:
            self.project = self.outputDir
            if ((TMPFS and BIG_RAM) or not TMPFS) and TRUE_TMP:
                self.workspace = Path('/tmp/simurba/' + self._timestamp + '_data')
            else:
                self.workspace = self.outputDir/'tmp_data'
        self._log_file = self.workspace/(self._timestamp + '_log.txt')
        self._qgs_log_file = self.workspace/(self._timestamp + '_qgs_log.txt')

        # Pour le multiprocessing
        if self.speed:
            self.nb_cores = NB_CORES
        else:
            self.nb_cores = 1

        self.pixResDir = self.workspace/'data'/(str(self.pixRes) + 'm')
        self._worskpace_initialized = False
        if interface == 'cli':
            print('\n    area = ' + str(self.studyAreaName))
            print('    workspace = ' + str(self.workspace))
            print('    project = ' + str(self.project))
            print('    nb_cores = ' + str(self.nb_cores))

        self._clean_last_step = False
        self._last_step = 1
        # { n° etape : [description, dossier, fonction] }
        self._steps = OrderedDict([
            (1, ["extracting and reprojecting data ",
                 self.workspace/'data', self.process_vector_data]),
            (2, ["cleaning buildings to estimate the population ",
                 self.workspace/'data/residential', self.identify_non_residential]),
            (3, ["trying to fit floors and surfaces distributions ",
                 self.workspace/'data/fitting', self.fit_distributions]),
            (4, ["creating a grid with a " + str(self.pixRes) + 'm resolution ',
                 self.pixResDir, self.make_grid]),
            (5, ["computing restriction rasters ",
                 self.pixResDir/'tif/restrictions', self.process_restrictions]),
            (6, ["computing interest rasters ",
                 self.pixResDir/'tif/interets', self.process_interests]),
            (7, ["estimating the population in the grid ",
                 self.pixResDir/'demography', self.estimate_demography]),
            (8, ["analysing the evolution of artificialized areas ",
                 self.pixResDir/'tif/artif', self.artif_measurement]),
            (9, ["finalizing ",
                 self.project, self.finalize])
        ])

    def __del__(self):
        try:
            del self._task_manager
            qgs.messageLog().disconnect()
        except (AttributeError, ValueError):
            pass
        return

    def init_workspace(self):
        """Pour nettoyer et creer les repertoires de sorties (en dehors de __init__)"""

        if self.wisdom:
            if self.force and self.workspace.exists():
                rmtree(str(self.workspace))
        else:
            if self.outputDir.exists():
                rmtree(str(self.outputDir))

        if self.project.exists():
            rmtree(str(self.project))
        if not self.workspace.exists():
            os.makedirs(str(self.workspace))

        if TRUE_TMP:
            self.tmpdir = Path('/tmp/simurba/' + self._timestamp + '_tmpfiles')
        else:
            self.tmpdir = self.workspace/'tmpfiles'
        if self.tmpdir.exists():
            rmtree(str(self.tmpdir))
        os.makedirs(str(self.tmpdir))

        self._worskpace_initialized = True
        return True

    def run(self):
        """Fonction run a ecraser dans un objet type QThread pour gerer la progression"""

        success, excep = True, None
        if self._interface == 'cli':
            print('\nStarted at ' + strftime('%Hh%Mm%Ss') + '\n')
        if not self._worskpace_initialized:
            b = self.init_workspace()
            if not b:
                return False

        self._current_step = self._last_step
        # Iteration centrale
        while success and self._current_step <= len(self._steps):
            self._last_step = self._current_step
            success, excep = self.get_next_step()
            self._current_step += 1
        str_success = 'Success\n' if success else 'There were errors\n'
        if self._interface == 'cli':
            print('\nEnded at ' + strftime('%Hh%Mm%Ss:') + '\n' + str_success)

        if excep is not None:
            self._write_logs("\nError:\n" + str(excep) + ': ' + str(excep.args), False)
            raise excep

        if self.project.exists():
            try:
                move(str(self._log_file), self.project)
                move(str(self._qgs_log_file), self.project)
            except FileNotFoundError:
                pass

        if self.tmpdir.exists():
            rmtree(str(self.tmpdir))
        if not self.wisdom:
            rmtree(str(self.workspace))

        return success

    def get_next_step(self):
        """Verifie si le repertoire existe deja on passe a l'etape suivante,
        lancement de la fonction specifique a l'etape, gestion des logs et erreurs"""

        if self._clean_last_step:
            key = self._last_step
            self._current_step = key
        else:
            key = self._current_step
        desc, directory, function = self._get_step_vars(key)
        if self._clean_last_step:
            rmtree(str(directory))
            self._clean_last_step = False
        if directory.exists():
            return True, None

        success = False
        excep = None
        start_time = time()
        os.makedirs(str(directory))
        self._write_logs(desc + ': ', newline=False)
        progress = "%i/9 : %s" % (key, desc)
        if self._interface == 'cli':
            printer(progress)
            if self._debug:
                print('\n')

        with warnings.catch_warnings():
            warnings.filterwarnings('ignore')
            np.seterr(divide='ignore', invalid='ignore')
            try:
                success = function()
                self._write_logs(get_exec_time(start_time), newline=True)
            except (NameError, AttributeError, KeyboardInterrupt,
                    FileNotFoundError, QgsProcessingException) as e:
                success = False
                excep = e
            finally:
                if not success:
                    self._clean_last_step = True

        if self._debug:
            print('\n\n')
        return success, excep

    # 9 fonctions pour 9 etapes
    def process_vector_data(self):
        """Extraction, decoupe et reprojection des donnees vectorielles"""

        mkdirList = [
            'data/artif/2005',
            'data/artif/2009',
            'data/artif/2012',
            'data/artif/2016',
            'data/artif/2018',
            'data/pai',
            'data/transp',
            'data/sirene',
            'data/restrict'
        ]
        for path in mkdirList:
            os.makedirs(str(self.workspace/path))

        # Tampon de 1000m autour de la zone pour extractions des quartiers et des PAI
        zone = QgsVectorLayer(str(self.localData/'zone.shp'), 'zone')
        params = {
            'INPUT': zone,
            'DISTANCE': self.bufferDist,
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'JOIN_STYLE': 0,
            'MITER_LIMIT': 2,
            'DISSOLVE': True,
            'OUTPUT': 'memory:zone_buffer'
        }
        res = processing.run('native:buffer', params, None, feedback)
        zone_buffer = res['OUTPUT']
        # Extraction des quartiers IRIS avec jointures
        all_iris = QgsVectorLayer(str(self.globalData/'iris/IRIS_GE.SHP'), 'iris')
        all_iris.dataProvider().createSpatialIndex()
        iris = self._iris_extractor(all_iris, zone_buffer, self.globalData/'insee/csv')

        if self._use_biotope_data:
            path = self.globalData/'pop/projection_2040_biotope.shp'
            if path.exists:
                pop2040 = QgsVectorLayer(str(path))
                params = {
                    'INPUT': pop2040,
                    'PREDICATE': 6,
                    'INTERSECT': zone_buffer,
                    'OUTPUT': 'memory:pop2040'
                }
                res = processing.run('native:extractbylocation', params, None, feedback)
                pop2040 = res['OUTPUT']
                params = {'AGGREGATES': [
                            {'aggregate': 'sum', 'delimiter': ',', 'input': '"p2040_haut"',
                             'length': 10, 'name': 'p2040_haut', 'precision': 3, 'type': 6},
                            {'aggregate': 'sum', 'delimiter': ',', 'input': '"p2040_bas"',
                             'length': 10, 'name': 'p2040_bas', 'precision': 3, 'type': 6},
                            {'aggregate': 'sum', 'delimiter': ',', 'input': '"Pop2015"',
                             'length': 10, 'name': 'p2015', 'precision': 0, 'type': 4}]}
                params['INPUT'] = pop2040
                params['OUTPUT'] = 'memory:pop2040_biotope'
                res = processing.run('qgis:aggregate', params, None, feedback)
                pop2040 = res['OUTPUT']
                pop2040.addExpressionField('to_real(("p2040_haut"-"p2015") / "p2015" / 25 * 100)',
                                           QgsField('evo_haut', QVariant.Double))
                pop2040.addExpressionField('to_real(("p2040_bas"-"p2015") / "p2015" / 25 * 100)',
                                           QgsField('evo_bas', QVariant.Double))

                reproj(res['OUTPUT'], self.crs, self.workspace/'data')

        # On utilise les IRIS dissous comme zone car precision RGE
        params = {
            'INPUT': iris,
            'FIELD': [],
            'OUTPUT': 'memory:zone'
        }
        res = processing.run('native:dissolve', params, None, feedback)
        zone = fix_geom(res['OUTPUT'])
        # Traitement du PLU
        if (self.localData/'plu.shp').exists():
            plu = QgsVectorLayer(str(self.localData/'plu.shp'), 'plu')
            self._plu_fixer(plu, zone, self.workspace/'data/')
        # Assemblage des zonages de protection
        z_path = self.globalData/'inpn'
        zonages_env = [str(z_path/z) for z in get_shapefiles(z_path)]
        self._env_restrict(zonages_env, zone, self.tmpdir/'inpn')
        zonages_env = []
        for z in get_shapefiles(self.tmpdir/'inpn'):
            zonages_env.append(str(self.tmpdir/('inpn/' + z)))

        params = {
            'LAYERS': zonages_env,
            'CRS': self.crs,
            'OUTPUT': str(self.workspace/'data/restrict/zonages_protection.shp')
        }
        processing.run('native:mergevectorlayers', params, None, feedback)

        # A partir d'ici, les traitements sont effectuee avec la zone reprojetee
        zone = reproj(zone, self.crs, self.workspace/'data/')
        zone_buffer = reproj(zone_buffer, self.crs, self.workspace/'data/')

        # Traitement de toutes les couches bati, routes, pai, surface en eau
        self._bdtopo_extractor(zone, zone_buffer)
        # Traitement des routes (differenciation, tampons...)
        self._roads_extractor(zone)
        # Preparation de la couche arrêts de transport en commun
        self._transp_extractor(zone_buffer)
        # Utilisation des parcelles DGFIP lors du calcul de densite
        self._dgfip_extractor(zone)
        # Correction de l'ocsol locale ou extraction de l'OSO CESBIO si besoin
        self._ocs_extractor(zone)
        # Gestion du risque innondation
        self._ppri_extractor(zone)

        # Liste pour tous les traitements qui peuvent être asynchrones
        processing_list = []
        # Zone tampon de 10m de part et d'autre des voies ferrees
        params = {
            'INPUT': str(self.workspace/'data/transp/troncon_voie_ferree.shp'),
            'EXPRESSION': """ "NATURE" != 'Transport urbain' """,
            'OUTPUT': 'memory:voies_ferrees',
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        voiesFerrees = res['OUTPUT']
        params = {
            'INPUT': voiesFerrees,
            'DISTANCE': 10,
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'JOIN_STYLE': 0,
            'MITER_LIMIT': 2,
            'DISSOLVE': True,
            'OUTPUT': str(self.workspace/'data/restrict/tampon_voies_ferrees.shp')
        }
        processing_list.append(('native:buffer', params, None, feedback))

        # Extraction et classification des points sirene
        sirene = clip(reproj(self.globalData/'sirene/geosirene.shp', self.crs), zone_buffer)
        sirene.addExpressionField(expr.SPLIT_SIRENE, QgsField('type', QVariant.String, len=20))
        params = {'INPUT': sirene, 'FIELD': 'type', 'OUTPUT': str(self.workspace/'data/sirene/')}
        processing_list.append(('qgis:splitvectorlayer', params, None, feedback))

        # Surface en eau (bdtopo)
        arg_list = []
        if self._mono_dpt:
            arg_list.append((reproj(self._bdt2016[0]/'D_HYDROGRAPHIE/SURFACE_EAU.SHP', self.crs),
                             zone, self.workspace/'data/restrict/'))
        else:
            merge(
                [clip(reproj(bd/'D_HYDROGRAPHIE/SURFACE_EAU.SHP', self.crs),
                      zone) for bd in self._bdt2016],
                self.workspace/'data/restrict/')

        # Traitement de la couche de restriction SAFER
        if (self.localData/'agro_restriction.shp').exists():
            path = self.localData/'agro_restriction.shp'
            agro_restriction = QgsVectorLayer(str(path), 'agro_restriction')
            arg_list.append((reproj(fix_geom(agro_restriction), self.crs),
                             zone, self.workspace/'data/restrict'))

        # >>>>>>>>>>> Traitement de la couche d'interêt ecologique
        # De preference un raster
        if (self.localData/'ecologie.tif').exists():
            gdal.Warp(
                str(self.workspace/'data/ecologie.tif'), str(self.localData/'ecologie.tif'),
                format='GTiff', outputType=gdal.GDT_Float32,
                xRes=self.pixRes, yRes=self.pixRes,
                resampleAlg='max',
                srcSRS='EPSG:2154', dstSRS=self.crs
            )
        #  Sinon une couche shape
        elif (self.localData/'ecologie.shp').exists():
            ecologie = QgsVectorLayer(str(self.localData/'ecologie.shp'), 'ecologie')
            fields = list(f.name() for f in ecologie.fields())
            if 'importance' not in fields:
                error = "Attribut requis 'importance' manquant dans la couche d'importance eco"
                self._write_logs('Erreur : ' + error)
                return False
            ecologie.addExpressionField('"importance"/100', QgsField('taux', QVariant.Double))
            arg_list.append((reproj(fix_geom(ecologie), self.crs), zone, self.workspace/'data/'))
        # Sinon couche d'importance ecologique de Biotope
        elif self._use_biotope_data:
            ecologie = QgsVectorLayer(str(self.globalData/'eco/biotope.shp'), 'ecologie')
            arg_list.append((reproj(fix_geom(ecologie), self.crs), zone, self.workspace/'data/'))

        # Traitement d'une couche facultative pour exclusion de zones baties et/ou d'interêt
        if (self.localData/'exclusion_manuelle.shp').exists():
            arg_list.append((reproj(fix_geom(self.localData/'exclusion_manuelle.shp'), self.crs),
                             zone, self.workspace/'data/restrict/'))

        self._get_jobs(clip, arg_list)

        # Traitement de la couche des mesures comensatoires
        path = self.globalData/'comp/l_mesure_compensatoire_s_000.shp'
        comp = reproj(QgsVectorLayer(str(path), 'compensation'), self.crs)
        comp.dataProvider().createSpatialIndex()
        try:
            clip(fix_geom(comp), zone, self.workspace/'data/restrict/')
        except QgsProcessingException:
            pass

        # Fusion des couches PAI
        mergePai = [
            str(self.workspace/'data/pai/administratif_militaire.shp'),
            str(self.workspace/'data/pai/culture_loisirs.shp'),
            str(self.workspace/'data/pai/industriel_commercial.shp'),
            str(self.workspace/'data/pai/religieux.shp'),
            str(self.workspace/'data/pai/sante.shp'),
            str(self.workspace/'data/pai/science_enseignement.shp'),
            str(self.workspace/'data/pai/sport.shp')
        ]
        params = {
            'LAYERS': mergePai,
            'CRS': self.crs,
            'OUTPUT': str(self.workspace/'data/pai/pai_merged.shp')
        }
        processing_list.append(('native:mergevectorlayers', params, None, feedback))

        path = self.workspace/'data/pai/surface_activite.shp'
        surf_activ = QgsVectorLayer(str(path), 'surf_activ')
        params = {
            'INPUT': fix_geom(surf_activ),
            'EXPRESSION': """ "CATEGORIE" != 'Industriel ou commercial' """,
            'OUTPUT': str(self.workspace/'data/restrict/surf_activ_non_com.shp'),
            'FAIL_OUTPUT': 'memory:'
        }
        processing_list.append(('native:extractbyexpression', params, None, feedback))

        self._get_jobs(processing.run, processing_list)

        return True

    def identify_non_residential(self):
        """Identification des batiments non habites pour estimer la densite de population"""

        # Nettoyage dans la couche de bati indifferencie
        path = self.workspace/'data/artif/2018/bati_indifferencie.shp'
        bati_indif = QgsVectorLayer(str(path), 'bati_indif_2016')
        bati_indif.dataProvider().createSpatialIndex()

        cleanPolygons = []
        cleanPoints = []

        # Couche supplementaire a exclure du calcul de pop a inclure dans les restrictions
        if (self.workspace/'data/restrict/exclusion_manuelle.shp').exists():
            cleanPolygons.append(str(self.workspace/'data/restrict/exclusion_manuelle.shp'))

        # Si possible, on utilise les parcelles non res DGFIP fusionnees avec un tampon de 2m
        if (self.workspace/'data/restrict/parcelles_non_res.shp').exists():
            path = self.workspace/'data/restrict/parcelles_non_res.shp'
            parcelles = QgsVectorLayer(str(path), 'parcelles_non_res')
            parcelles.dataProvider().createSpatialIndex()
            params = {
                'INPUT': parcelles,
                'DISTANCE': 2,
                'SEGMENTS': 5,
                'END_CAP_STYLE': 0,
                'JOIN_STYLE': 0,
                'MITER_LIMIT': 2,
                'DISSOLVE': True,
                'OUTPUT': 'memory:buffer_parcelles_non_res'
            }
            res = processing.run('native:buffer', params, None, feedback)
            parcelles = res['OUTPUT']
            to_shp(parcelles, self.workspace/'data/residential/buffer_parcelles_non_res.shp')
            cleanPolygons.append(parcelles)
        # Sinon, on utliser les surfaces d'activite et les PAI
        else:
            cleanPoints.append(str(self.workspace/'data/pai/pai_merged.shp'))
            # Fusion des polygones de zones d'activite pour eviter les oublis avec predicat WITHIN
            params = {
                'INPUT': str(self.workspace/'data/restrict/surf_activ_non_com.shp'),
                'FIELD': [],
                'OUTPUT': 'memory:'
            }
            res = processing.run('native:dissolve', params, None, feedback)
            # On inclut certaines surfaces d'activites dans la couche de restriction
            cleanPolygons.append(res['OUTPUT'])

        self._building_cleaner(bati_indif, cleanPolygons, cleanPoints,
                               self.workspace/'data/residential/builds.shp',
                               self.workspace/'data/residential/removed_builds.shp')
        # Intersection du bati residentiel avec les quartiers IRIS
        res_builds = QgsVectorLayer(str(self.workspace/'data/residential/builds.shp'))
        res_builds.dataProvider().createSpatialIndex()
        iris = QgsVectorLayer(str(self.workspace/'data/iris.shp'), 'iris')
        iris.dataProvider().createSpatialIndex()
        params = {
            'INPUT': res_builds,
            'OVERLAY': iris,
            'INPUT_FIELDS': ['ID', 'HAUTEUR', 'NB_NIV'],
            'OVERLAY_FIELDS': ['CODE_IRIS', 'ID_IRIS', 'NOM_IRIS', 'INSEE_COM',
                               'NOM_COM', 'TYP_IRIS', 'POP15', 'TXRP15'],
            'OUTPUT': str(self.workspace/'data/residential/builds_x_iris.shp')
        }
        processing.run('native:intersection', params, None, feedback)

        return True

    def fit_distributions(self):
        """Ajustement des distributions pour surface et hauteur du bati"""

        iris = QgsVectorLayer(str(self.workspace/'data/iris.shp'), 'iris')
        communes = QgsVectorLayer(str(self.workspace/'data/communes.shp'), 'communes')
        build_list = [self.workspace/'data/artif/2018/bati_indifferencie.shp',
                      self.workspace/'data/artif/2018/bati_remarquable.shp',
                      self.workspace/'data/artif/2018/bati_industriel.shp']

        build_list = [QgsVectorLayer(str(p)) for p in build_list]
        buildings = merge(build_list)
        buildings.dataProvider().createSpatialIndex()
        params = {
            'INPUT': buildings,
            'OVERLAY': iris,
            'INPUT_FIELDS': ['ID', 'HAUTEUR', 'NB_NIV'],
            'OVERLAY_FIELDS': ['CODE_IRIS', 'NOM_IRIS', 'ID_IRIS',
                               'TYP_IRIS', 'INSEE_COM', 'NOM_COM'],
            'OUTPUT': 'memory:all_builds_x_iris'}

        res = processing.run('native:intersection', params, None, feedback)
        buildings = res['OUTPUT']
        buildings.addExpressionField('$area', QgsField('area', QVariant.Double, len=12, prec=3))
        to_shp(buildings, str(self.workspace/'data/fitting/all_builds_x_iris.shp'))

        data_st = {'zone': {1: ([], [])},
                   'com': {},
                   'iris': {}}

        csv_com = self.workspace/'data/fitting/corresp_com.csv'
        csv_iris = self.workspace/'data/fitting/corresp_iris.csv'
        corresp_com = {}
        with csv_com.open('w') as w:
            w.write('code,id\n')
            for feat in communes.getFeatures():
                com_code = str(feat.attribute('INSEE_COM'))
                com_id = int(feat.attribute('ID_COM'))
                corresp_com[com_code] = com_id
                w.write('{},{}\n'.format(com_code, com_id))
                data_st['com'][com_id] = ([], [])

        with csv_iris.open('w') as w:
            w.write('code,id\n')
            for feat in iris.getFeatures():
                iris_code = str(feat.attribute('CODE_IRIS'))
                iris_id = int(feat.attribute('ID_IRIS'))
                w.write('{},{}\n'.format(iris_code, iris_id))
                data_st['iris'][iris_id] = ([], [])

        with open(str(self.workspace/'data/fitting/data_to_fit.csv'), 'w') as w:
            w.write('id_bat,id_iris,id_com,aire,hauteur\n')

            for feat in buildings.getFeatures():
                feat_id = str(feat.attribute('ID'))
                iris_code = str(feat.attribute('CODE_IRIS'))
                iris_id = int(feat.attribute('ID_IRIS'))
                com_code = str(feat.attribute('INSEE_COM'))
                com_id = corresp_com[com_code]
                a = float(feat.attribute('area'))
                h = int(feat.attribute('HAUTEUR'))

                if h > 0 and self.minSurf < a < 20000:
                    data_st['zone'][1][0].append(a)
                    data_st['zone'][1][1].append(h)
                    data_st['com'][com_id][0].append(a)
                    data_st['com'][com_id][1].append(h)
                    data_st['iris'][iris_id][0].append(a)
                    data_st['iris'][iris_id][1].append(h)
                    line = '{},{},{},{},{}\n'.format(feat_id, iris_id, com_id, a, h)
                    w.write(line)

        for method in self.fitLevels:
            directory = self.workspace/('data/fitting/' + method)
            if directory.exists():
                rmtree(str(directory))
            os.mkdir(str(directory))
            data = data_st[method]
            for id, ds in data.items():
                areas = ds[0]
                if areas:
                    area_fitter = Fitter(pd.Series(areas), binw=20,
                                         nature=str(id) + '_area', verbose=self._debug,
                                         nb_cores=self.nb_cores)
                    plot = area_fitter.plot_n_best(10, 'sse', max_x=2000, max_y=0.02)
                    if plot:
                        area_fitter.write_results(directory, plot)
                        plot.figure.clf()
                        plot = None

                heights = ds[1]
                if heights:
                    height_fitter = Fitter(pd.Series(heights), binw=self.levelHeight,
                                           nature=str(id) + '_height', verbose=self._debug,
                                           nb_cores=self.nb_cores)
                    plot = height_fitter.plot_n_best(10, 'sse', max_x=30, max_y=0.3)
                    if plot:
                        height_fitter.write_results(directory, plot)
                        plot.figure.clf()
                        plot = None

        return True

    def make_grid(self):
        """Creation d'une grille reguliere"""

        zone = QgsVectorLayer(str(self.workspace/'data/zone.shp'), 'zone')
        ex = zone.extent()
        extent = '{}, {}, {}, {} [{}]'
        grid_ext = extent.format(ex.xMinimum(), ex.xMaximum(),
                                 ex.yMinimum(), ex.yMaximum(), self.crs)
        params = {
            'TYPE': 2,
            'EXTENT': grid_ext,
            'HSPACING': self.pixRes,
            'VSPACING': self.pixRes,
            'HOVERLAY': 0,
            'VOVERLAY': 0,
            'CRS': self.crs,
            'OUTPUT': str(self.pixResDir/'grid.shp')
        }
        processing.run('qgis:creategrid', params, None, feedback)

        return True

    def process_restrictions(self):
        """Ecriture des rasters de restrictions"""

        # Objet pour transformation de coordonees
        lamb_93 = QgsCoordinateReferenceSystem()
        lamb_93.createFromString('EPSG:2154')
        laea = QgsCoordinateReferenceSystem()
        laea.createFromString('EPSG:3035')
        tr_cxt = QgsCoordinateTransformContext()
        coordTr = QgsCoordinateTransform(lamb_93, laea, tr_cxt)
        # BBOX pour extraction du MNT
        grid = QgsVectorLayer(str(self.pixResDir/'grid.shp'), 'grid')
        grid.dataProvider().createSpatialIndex()
        self.extent = grid.extent()
        xMin = self.extent.xMinimum()
        yMin = self.extent.yMinimum()
        xMax = self.extent.xMaximum()
        yMax = self.extent.yMaximum()
        if self.crs == 'EPSG:2154':
            extent_lamb93 = self.extent
        else:
            extent_lamb93 = coordTr.transform(self.extent, coordTr.ReverseTransform)
        # Extraction des tuiles MNT dans la zone d'etude
        demList = []
        for dpt in self.dpt:
            if dpt not in ['46']:
                path = self.globalData/'bdalti/5m'
                match = r'RGEALTI_2-[0-9]_5M_.*_D0' + dpt + '_.*'
                for d in os.listdir(str(path)):
                    res = re.match(match, d)
                    if res and (path/(d + '/RGEALTI')).exists():
                        path = path/(d + '/RGEALTI')
                        for n in os.listdir(str(path)):
                            if '_DONNEES_LIVRAISON_' in n and os.path.isdir(str(path/n)):
                                path = path/n
                                break
                        path = path/('RGEALTI_MNT_5M_ASC_LAMB93_IGN69_D0' + dpt)
                        demList += dem_extractor(path, extent_lamb93)
                        break
            else:
                path = self.globalData/'bdalti/25m'
                match = r'BDALTIV2_2-[0-9]_25M_.*_D0' + dpt + '_.*'
                for d in os.listdir(str(path)):
                    res = re.match(match, d)
                    if res and (path/(d + '/BDALTIV2')).exists():
                        path = path/(d + '/BDALTIV2')
                        for n in os.listdir(str(path)):
                            if '_DONNEES_LIVRAISON_' in n and os.path.isdir(str(path/n)):
                                path = path/n
                                break
                        path = path/('BDALTIV2_MNT_25M_ASC_LAMB93_IGN69_D0' + dpt)
                        demList += dem_extractor(path, extent_lamb93)
                        break

        # Fusion des tuiles et reprojection
        gdal.Warp(
            str(self.pixResDir/'tif/mnt.tif'), demList,
            format='GTiff', outputType=gdal.GDT_Float32,
            xRes=self.pixRes, yRes=self.pixRes,
            resampleAlg='cubicspline',
            srcSRS='EPSG:2154', dstSRS=self.crs,
            outputBounds=(xMin, yMin, xMax, yMax),
            srcNodata=-99999.00
        )
        # Calcul de pente en %
        gdal.DEMProcessing(
            str(self.pixResDir/'tif/restrictions/slope.tif'),
            str(self.pixResDir/'tif/mnt.tif'),
            'slope', format='GTiff'
        )
        # Rasterisations
        rasterize(self.workspace/'data/zone.shp', self.pixResDir/'tif/restrictions/mask.tif',
                  'Byte', self.crs, self.pixRes, self.extent, None, 1, True, False, self._debug)
        arg_list = [
            (self.workspace/'data/restrict/tampon_voies_ferrees.shp',
             self.pixResDir/'tif/restrictions/tampon_voies_ferrees.tif',
             'Byte', self.crs, self.pixRes, self.extent, None, 1, False, False, self._debug),

            (self.workspace/'data/restrict/tampon_routes_importantes.shp',
             self.pixResDir/'tif/restrictions/tampon_routes_importantes.tif',
             'Byte', self.crs, self.pixRes, self.extent, None, 1, False, False, self._debug),

            (self.workspace/'data/restrict/zonages_protection.shp',
             self.pixResDir/'tif/restrictions/zonages_protection.tif',
             'Byte', self.crs, self.pixRes, self.extent, None, 1, False, False, self._debug),

            (self.workspace/'data/pai/surf_activ_non_com.shp',
             self.pixResDir/'tif/restrictions/surf_activ_non_com.tif',
             'Byte', self.crs, self.pixRes, self.extent, None, 1, False, False, self._debug),

            (self.workspace/'data/restrict/cimetiere.shp',
             self.pixResDir/'tif/restrictions/cimetiere.tif',
             'Byte', self.crs, self.pixRes, self.extent, None, 1, False, False, self._debug),

            (self.workspace/'data/restrict/surface_eau.shp',
             self.pixResDir/'tif/restrictions/surface_eau.tif',
             'Byte', self.crs, self.pixRes, self.extent, None, 1, False, False, self._debug)
        ]
        arg_list += self._restriction_vectors_list()
        self._get_jobs(rasterize, arg_list)

        return True

    def process_interests(self):
        """Traitement et rasterisation des differentes couches d'interêt a urbaniser"""

        grid = QgsVectorLayer(str(self.pixResDir/'grid.shp'), 'grid')
        self.extent = grid.extent()
        # Etendue du projet
        xMin = self.extent.xMinimum()
        yMin = self.extent.yMinimum()
        xMax = self.extent.xMaximum()
        yMax = self.extent.yMaximum()
        arg_list = [
            (self.workspace/'data/artif/2016/routes_clean.shp',
             self.pixResDir/'tif/interets/routes_burn.tif',
             'Byte', self.crs, self.pixRes, self.extent, None, 1, False, False, self._debug),

            (self.workspace/'data/transp/arrets_transport.shp',
             self.pixResDir/'tif/interets/arrets_transport.tif',
             'Byte', self.crs, self.pixRes, self.extent, None, 1, False, False, self._debug)
            ]

        # Decoupe du tif d'interêt ecologique si il a ete traite lors de l'etape 1
        if (self.workspace/'data/ecologie.tif').exists():
            gdal.Warp(
                str(self.pixResDir/'tif/interets/ecologie.tif'),
                str(self.workspace/'data/ecologie.tif'),
                format='GTiff', outputType=gdal.GDT_Float32,
                xRes=self.pixRes, yRes=self.pixRes,
                outputBounds=(xMin, yMin, xMax, yMax)
                )
        elif (self.workspace/'data/ecologie.shp').exists():
            ecologie = QgsVectorLayer(str(self.workspace/'data/ecologie.shp'), 'ecologie')
            if self._use_biotope_data:
                ecologie.addExpressionField('"NOTE_NATU" / maximum("NOTE_NATU")',
                                            QgsField('taux', QVariant.Double, len=10, prec=6))
                to_shp(ecologie, self.workspace/'data/ecologie_biotope.shp')

                arg_list.append((self.workspace/'data/ecologie_biotope.shp',
                                 self.pixResDir/'tif/interets/ecologie.tif',
                                 'Float32', self.crs, self.pixRes, self.extent,
                                 'taux', None, False, False, self._debug))
            else:
                arg_list.append((self.workspace/'data/ecologie.shp',
                                 self.pixResDir/'tif/interets/ecologie.tif',
                                 'Float32', self.crs, self.pixRes, self.extent,
                                 'taux', None, False, False, self._debug))

        self._get_jobs(rasterize, arg_list)
        # Calcul des rasters de distance
        params = {
            'INPUT': str(self.pixResDir/'tif/interets/routes_burn.tif'),
            'BAND': 1,
            'VALUES': 1,
            'UNITS': 0,
            'NODATA': -1,
            'MAX_DISTANCE': self.roadDist,
            'DATA_TYPE': 5,
            'OUTPUT':  str(self.pixResDir/'tif/interets/distance_routes.tif')
        }
        processing.run('gdal:proximity', params, None, feedback)
        params = {
            'INPUT': str(self.pixResDir/'tif/interets/arrets_transport.tif'),
            'BAND': 1,
            'VALUES': 1,
            'UNITS': 0,
            'NODATA': -1,
            'MAX_DISTANCE': self.transDist,
            'DATA_TYPE': 5,
            'OUTPUT': str(self.pixResDir/'tif/interets/distance_arrets_transport.tif')
        }
        processing.run('gdal:proximity', params, None, feedback)
        # Calcul des rasters de densite
        projwin = str(xMin) + ',' + str(xMax) + ',' + str(yMin) + ',' + str(yMax)

        with (self.localData/'sirene_distances.csv').open('r') as csvFile:
            reader = csv.reader(csvFile)
            next(reader, None)
            distancesSirene = {rows[0]: int(rows[1]) for rows in reader}

        for k, v in distancesSirene.items():
            path = self.workspace/'data/sirene'
            if (path/('type_' + k + '.shp')).exists():
                layer = QgsVectorLayer(str(path/('type_' + k + '.shp')), k)
            elif (path/('type_' + k + '.gpkg')).exists():
                layer = QgsVectorLayer(str(path/('type_' + k + '.gpkg')), k)

            params = {
                'INPUT': layer,
                'RADIUS': v,
                'PIXEL_SIZE': self.pixRes,
                'KERNEL': 0,
                'OUTPUT_VALUE': 0,
                'OUTPUT': str(self.tmpdir/('densite_' + k + '.tif'))
            }
            processing.run('qgis:heatmapkerneldensityestimation', params, None, feedback=feedback)
            params = {
                'INPUT': str(self.tmpdir/('densite_' + k + '.tif')),
                'PROJWIN': projwin,
                'NODATA': -9999,
                'DATA_TYPE': 5,
                'OUTPUT': str(self.pixResDir/('tif/interets/densite_' + k + '.tif'))
            }
            processing.run('gdal:cliprasterbyextent', params, None, feedback=feedback)

        return True

    def estimate_demography(self):
        """Estimation de la population a partir de la surface plancher et de la donnee par IRIS"""

        grid = QgsVectorLayer(str(self.pixResDir/'grid.shp'), 'grid')
        self.extent = grid.extent()
        iris = QgsVectorLayer(str(self.workspace/'data/iris.shp'))

        # Calcul de la surface plancher
        buildings = QgsVectorLayer(str(self.workspace/'data/residential/builds_x_iris.shp'))
        buildings.dataProvider().createSpatialIndex()
        buildings.addExpressionField('$area', QgsField('area_i', QVariant.Double))
        expression = ' "area_i" * "NB_NIV" '
        if self.useTxrp:
            expression += ' * IF("TXRP15" IS NOT NULL, "TXRP15", 0) '
        buildings.addExpressionField(expression, QgsField('planch', QVariant.Double))
        buildings.addExpressionField('concat("CODE_IRIS", "ID")',
                                     QgsField('pkey_iris', QVariant.String, len=50))

        # Attribution de la population theorique en fonction du poids dans l'iris
        dicPop = {}
        dicSumBuilds = {}
        dicBuilds = {}
        dicWeightedPop = {}
        for f in iris.getFeatures():
            dicBuilds[f.attribute('CODE_IRIS')] = {}
            dicWeightedPop[f.attribute('CODE_IRIS')] = {}
            dicSumBuilds[f.attribute('CODE_IRIS')] = decimal.Decimal(0.0)
            dicPop[f.attribute('CODE_IRIS')] = decimal.Decimal(f['POP15'])
        for f in buildings.getFeatures():
            dicSumBuilds[f.attribute('CODE_IRIS')] += decimal.Decimal(f.attribute('planch'))
        for f in buildings.getFeatures():
            dicWeightedPop[f.attribute('CODE_IRIS')][f.attribute('ID')] = decimal.Decimal(0)
            try:
                v = decimal.Decimal(f.attribute('planch')) / dicSumBuilds[f.attribute('CODE_IRIS')]
                dicBuilds[f.attribute('CODE_IRIS')][f.attribute('ID')] = v
            except decimal.InvalidOperation:
                dicBuilds[f.attribute('CODE_IRIS')][f.attribute('ID')] = decimal.Decimal(0)

        # Ecriture de l'info de pop par batiment avec cle primaire par iris
        with (self.pixResDir/'demography/pop_bati_pkey.csv').open('w') as w:
            w.write('pkey_iris, pop\n')
            for quartier, builds in dicBuilds.items():
                irisPop = dicPop[quartier]
                reste = decimal.Decimal(0)
                for idBati, weight in builds.items():
                    popFloat = decimal.Decimal(weight * irisPop)
                    dicWeightedPop[quartier][idBati] += int(popFloat)
                    reste += popFloat - int(popFloat)
                keyList = list(builds.keys())
                valueList = list(builds.values())
                valueArray = np.array(valueList, np.float64)
                weight = valueArray/valueArray.sum()
                size = valueArray.size
                for _ in range(round(reste)):
                    i = np.random.choice(size, p=weight)
                    idBati = keyList[i]
                    dicWeightedPop[quartier][idBati] += 1
                for idBati, value in dicWeightedPop[quartier].items():
                    w.write(quartier + idBati + ',' + str(value) + '\n')

        # Jointure des donnees avec les batiments
        csvPop = QgsVectorLayer(str(self.pixResDir/'demography/pop_bati_pkey.csv'))
        csvPop.addExpressionField('to_int("pop")', QgsField('pop_bati', QVariant.Int))
        join_layers(buildings, 'pkey_iris', csvPop, 'pkey_iris', ['pop'])
        expression = ' "planch" / "pop_bati" '
        buildings.addExpressionField(expression, QgsField('m2_hab', QVariant.Double))

        # Declaration des structures de donnee
        dicPop = {}
        dicBuilds = {}
        for f in buildings.getFeatures():
            dicPop[f.attribute('pkey_iris')] = decimal.Decimal(f.attribute('pop_bati'))
            dicBuilds[f.attribute('pkey_iris')] = {}
        params = {
            'INPUT': buildings,
            'OVERLAY': grid,
            'INPUT_FIELDS': ['ID', 'HAUTEUR', 'NB_NIV', 'CODE_IRIS', 'ID_IRIS',
                             'NOM_IRIS', 'TYP_IRIS', 'POP15', 'TXRP15', 'area_i',
                             'planch', 'pkey_iris', 'pop_bati', 'm2_hab'],
            'OVERLAY_FIELDS': ['id'],
            'OUTPUT': 'memory:bati_inter_grid'
        }
        res = processing.run('native:intersection', params, None, feedback)
        buildings = res['OUTPUT']
        buildings.dataProvider().createSpatialIndex()
        buildings.addExpressionField('concat("CODE_IRIS", "ID", "id_2")',
                                     QgsField('pkey_grid', QVariant.String, len=50))
        buildings.addExpressionField('$area', QgsField('area_g', QVariant.Double))
        expression = ' "area_g" * "NB_NIV" '
        if self.useTxrp:
            expression += ' * "TXRP15"'
        buildings.addExpressionField(expression, QgsField('planch_g', QVariant.Double))
        for f in buildings.getFeatures():
            if f.attribute('area_i') > 0:
                v = decimal.Decimal(f.attribute('area_g')) / decimal.Decimal(f.attribute('area_i'))
                dicBuilds[f.attribute('pkey_iris')][f.attribute('id_2')] = v
            else:
                dicBuilds[f.attribute('pkey_iris')][f.attribute('id_2')] = 0

        # Attribution a chaque cellule d'une pop theorique
        dicWeightedPop = {}
        for build, parts in dicBuilds.items():
            dicWeightedPop[build] = {}
            reste = decimal.Decimal(0)
            for gid, weight in parts.items():
                pop = dicPop[build]
                popFloat = decimal.Decimal(weight * pop)
                dicWeightedPop[build][gid] = int(popFloat)
                reste += popFloat - int(popFloat)
            if reste > 0:
                keyList = list(parts.keys())
                valueList = list(parts.values())
                valueArray = np.array(valueList, np.float64)
                weight = valueArray/valueArray.sum()
                size = valueArray.size
                for _ in range(round(reste)):
                    i = np.random.choice(size, p=weight)
                    gid = keyList[i]
                    dicWeightedPop[build][gid] += 1

        # Ecriture des stats de pop par cellule
        with (self.pixResDir/'demography/pop_grid_pkey.csv').open('w') as w:
            w.write('pkey_grid, pop\n')
            for build, parts in dicWeightedPop.items():
                for gid, pop in parts.items():
                    w.write(build + str(gid) + ', ' + str(pop) + '\n')
        # Jointure avec la grille
        csvPop = QgsVectorLayer(str(self.pixResDir/'demography/pop_grid_pkey.csv'))
        csvPop.addExpressionField('to_int("pop")', QgsField('pop_g', QVariant.Int))
        join_layers(buildings, 'pkey_grid', csvPop, 'pkey_grid', ['pop'])

        # Calcul de statistiques par quartiers
        compute_stat_list = [
            ('planch', 'CODE_IRIS', str(self.pixResDir/'demography/iris_sbp.csv')),
            ('pop_g', 'id_2', str(self.pixResDir/'demography/grid_pop.csv')),
            ('m2_hab', 'CODE_IRIS', str(self.pixResDir/'demography/iris_m2_hab.csv')),
            ('area_g', 'id_2', str(self.pixResDir/'demography/grid_sbr.csv')),
            ('area_g', 'CODE_IRIS', str(self.pixResDir/'demography/iris_sbr.csv')),
            ('planch_g', 'id_2', str(self.pixResDir/'demography/grid_sbp.csv')),
            ('NB_NIV', 'CODE_IRIS', str(self.pixResDir/'demography/iris_nb_niv.csv'))
        ]
        processing_list = []
        for t in compute_stat_list:
            d = {
                'INPUT': buildings,
                'VALUES_FIELD_NAME': t[0],
                'CATEGORIES_FIELD_NAME': t[1],
                'OUTPUT': t[2]
            }
            processing_list.append(('qgis:statisticsbycategories', d, None, feedback))
        self._get_jobs(processing.run, processing_list)
        to_shp(buildings, self.pixResDir/'bati_inter_grid.shp')

        # Jointure des statistiques par IRIS
        csvIris = []
        csvIsbp = QgsVectorLayer(str(self.pixResDir/'demography/iris_sbp.csv'), 'iris_sbp')
        csvIsbp.addExpressionField('round(to_real("sum"))', QgsField('sbp_sum', QVariant.Int))
        csvIris.append(csvIsbp)
        csvIsbr = QgsVectorLayer(str(self.pixResDir/'demography/iris_sbr.csv'), 'iris_sbr')
        csvIsbr.addExpressionField('round(to_real("sum"))', QgsField('sbr_sum', QVariant.Int))
        csvIsbr.addExpressionField('round(to_real("median"))', QgsField('sbr_med', QVariant.Int))
        csvIris.append(csvIsbr)
        csvIm2 = QgsVectorLayer(str(self.pixResDir/'demography/iris_m2_hab.csv'), 'iris_m2_hab')
        csvIm2.addExpressionField('round(to_real("median"))', QgsField('m2_hab_med', QVariant.Int))
        csvIris.append(csvIm2)
        csvIniv = QgsVectorLayer(str(self.pixResDir/'demography/iris_nb_niv.csv'), 'iris_nb_niv')
        csvIniv.addExpressionField('to_int("max")', QgsField('nbniv_max', QVariant.Int))
        csvIris.append(csvIniv)
        for csvLayer in csvIris:
            join_layers(iris, 'CODE_IRIS', csvLayer, 'CODE_IRIS', expr.STATS_BLACKLIST)
        to_shp(iris, self.pixResDir/'demography/stat_iris.shp')

        # Jointure des statistiques par cellule
        csvGrid = []
        csvGplanch = QgsVectorLayer(str(self.pixResDir/'demography/grid_sbp.csv'))
        csvGplanch.addExpressionField('round(to_real("sum"))', QgsField('sbp', QVariant.Int))
        csvGrid.append(csvGplanch)
        csvGsbr = QgsVectorLayer(str(self.pixResDir/'demography/grid_sbr.csv'))
        csvGsbr.addExpressionField('round(to_real("sum"))', QgsField('sbr', QVariant.Int))
        csvGrid.append(csvGsbr)
        csvGpop = QgsVectorLayer(str(self.pixResDir/'demography/grid_pop.csv'))
        csvGpop.addExpressionField('to_int("sum")', QgsField('pop', QVariant.Int))
        csvGrid.append(csvGpop)
        for csvLayer in csvGrid:
            join_layers(grid, 'id', csvLayer, 'id_2', expr.STATS_BLACKLIST)
        to_shp(grid, self.pixResDir/'demography/stat_grid.shp')

        return True

    def artif_measurement(self):
        """Mesure de l'artif par une rasterisation des vecteurs a 1m puis resampling"""

        ds = gdal.Open(str(self.pixResDir/'tif/restrictions/mask.tif'))
        self.proj = ds.GetProjection()
        self.geot = ds.GetGeoTransform()
        self.rows = ds.RasterYSize
        self.cols = ds.RasterXSize
        ds = None
        grid = QgsVectorLayer(str(self.pixResDir/'grid.shp'), 'grid')
        self.extent = grid.extent()
        rasterize(self.workspace/'data/iris.shp', self.pixResDir/'tif/iris_id.tif',
                  'UInt16', self.crs, self.pixRes, self.extent, 'ID_IRIS', verbose=self._debug)
        iris_array = to_nparray(self.pixResDir/'tif/iris_id.tif', np.uint16)

        # mask = to_nparray(self.pixResDir/'tif/restrictions/mask.tif', np.byte)
        srf = self.pixRes * self.pixRes
        with open(str(self.pixResDir/'tif/artif/evo_artif.csv'), 'w') as w:
            w.write('annee_livraison,srf_bati,srf_routes,somme\n')
        for date in ['2005', '2009', '2012', '2016']:
            if date == '2005':
                b_shapes = [
                    self.workspace/('data/artif/' + date + '/batiment.shp'),
                    self.workspace/('data/artif/' + date + '/piste_aerodrome.shp'),
                    self.workspace/('data/artif/' + date + '/terrain_sport.shp')]

                r_shapes = [self.workspace/('data/artif/' + date + '/surface_route.shp'),
                            self.workspace/('data/artif/' + date + '/buffer_routes.shp')]

            elif date == '2016':
                # Couches pour mesurer l'artif (ancienne et nouvelle)
                b_shapes = [self.workspace/('data/artif/2018/bati_indifferencie.shp'),
                            self.workspace/('data/artif/2018/bati_industriel.shp'),
                            self.workspace/('data/artif/2018/bati_remarquable.shp'),
                            self.workspace/('data/artif/2018/terrain_sport.shp'),
                            self.workspace/('data/artif/2018/reservoir.shp')]

                r_shapes = [self.workspace/('data/artif/' + date + '/surface_route.shp'),
                            self.workspace/('data/artif/' + date + '/buffer_routes.shp')]

                path = self.workspace/('data/artif/2018/construction_legere.shp')
                if path.exists():
                    b_shapes.append(path)
                path = self.workspace/('data/artif/2018/piste_aerodrome.shp')
                if path.exists():
                    b_shapes.append(path)
                # path = self.workspace/'data/artif/construction_surfacique.shp'
                # if path.exists():
                #     b_shapes += path
            else:
                b_shapes = [self.workspace/('data/artif/' + date + '/bati_indifferencie.shp'),
                            self.workspace/('data/artif/' + date + '/bati_industriel.shp'),
                            self.workspace/('data/artif/' + date + '/bati_remarquable.shp'),
                            self.workspace/('data/artif/' + date + '/terrain_sport.shp'),
                            self.workspace/('data/artif/' + date + '/reservoir.shp')]

                r_shapes = [self.workspace/('data/artif/' + date + '/surface_route.shp'),
                            self.workspace/('data/artif/' + date + '/buffer_routes.shp')]

                path = self.workspace/('data/artif/' + date + '/construction_legere.shp')
                if path.exists():
                    b_shapes.append(path)
                path = self.workspace/('data/artif/' + date + '/piste_aerodrome.shp')
                if path.exists():
                    b_shapes.append(path)

            b_path = self.workspace/('data/artif/' + date + '/tout_bati.shp')
            r_path = self.workspace/('data/artif/' + date + '/toutes_routes.shp')
            to_shp(merge([QgsVectorLayer(str(shape)) for shape in b_shapes]), b_path)
            to_shp(merge([QgsVectorLayer(str(shape)) for shape in r_shapes]), r_path)
            del b_shapes, r_shapes

            builds = self._compute_artif_raster('tout_bati', str(b_path), date)
            roads = self._compute_artif_raster('toutes_routes', str(r_path), date)
            artif = builds + roads
            # Correction dans le cas ou un batiment recouvre un tampon de route
            # artif doit toujours avoir un max srfCell
            artif = np.where(artif > srf, srf, artif)
            roads = artif - builds
            with open(str(self.pixResDir/'tif/artif/evo_artif.csv'), 'a') as w:
                w.write('{},{},{},{}\n'.format(date, builds.sum(), roads.sum(), artif.sum()))

            if date == '2016':
                urb = np.where(artif > 0, 1, 0)
                built = np.where(builds > 0, 1, 0)
                tx_artif = artif / artif.max()

                # Surface batie totale par IRIS
                iris_sbt = np.ones([self.rows, self.cols], np.uint16)
                path = str(self.pixResDir/'tif/artif/iris_sbt.csv')
                with open(path, 'w') as w:
                    w.write('IRIS_ID, surface\n')
                    for n in np.unique(iris_array):
                        if n != 0:
                            district_sum = np.where(iris_array == n, builds, 0).sum()
                            w.write(str(int(n)) + ', ' + str(int(district_sum)) + '\n')
                            iris_sbt = iris_sbt * np.where(iris_array == n, district_sum, 1)
                # Artif totale par IRIS
                iris_artif = np.ones([self.rows, self.cols], np.uint16)
                path = str(self.pixResDir/'tif/artif/iris_artif.csv')
                with open(path, 'w') as w:
                    for n in np.unique(iris_array):
                        if n != 0:
                            district_sum = np.where(iris_array == n, artif, 0).sum()
                            w.write(str(int(n)) + ', ' + str(int(district_sum)) + '\n')
                            iris_artif = iris_artif * np.where(iris_array == n, district_sum, 1)

                rasterize(self.pixResDir/'demography/stat_iris.shp',
                          self.pixResDir/'tif/artif/iris_sbr.tif',
                          'UInt32', self.crs, self.pixRes, self.extent, 'sbr_sum',
                          verbose=self._debug)
                iris_sbr = to_nparray(self.pixResDir/'tif/artif/iris_sbr.tif', np.uint32)
                iris_tx_sbr = np.where(iris_sbt > 0, iris_sbr / iris_sbt, 0).astype(np.float32)
                to_tiff(iris_tx_sbr, 'float32', self.proj, self.geot,
                        self.pixResDir/'tif/artif/iris_tx_sbr.tif', verbose=self._debug)
                to_tiff(builds, 'uint16', self.proj, self.geot,
                        self.pixResDir/'tif/artif/builds.tif', verbose=self._debug)
                to_tiff(roads, 'uint16', self.proj, self.geot,
                        self.pixResDir/'tif/artif/srf_roads.tif', verbose=self._debug)
                to_tiff(artif, 'uint16', self.proj, self.geot,
                        self.pixResDir/'tif/artif/artif.tif', verbose=self._debug)
                to_tiff(tx_artif, 'float32', self.proj, self.geot,
                        self.pixResDir/'tif/artif/tx_artif.tif', verbose=self._debug)
                to_tiff(urb, 'byte', self.proj, self.geot,
                        self.pixResDir/'tif/artif/urban.tif', verbose=self._debug)
                to_tiff(built, 'byte', self.proj, self.geot,
                        self.pixResDir/'tif/artif/built_cells.tif', verbose=self._debug)

        return True

    def finalize(self):
        """Derniers calculs sur les rasters et ecriture de tous les resultats"""

        # Calcul de la population totale de la zone pour export en csv
        iris = QgsVectorLayer(str(self.pixResDir/'demography/stat_iris.shp'))
        dates = ['06', '07', '08', '09', '10', '12', '14', '15']
        pop = {d: 0 for d in dates}
        for feat in iris.getFeatures():
            for d in dates:
                try:  # Pour les cas our les IRIS ne correspondent pas dans certaines années
                    value = feat.attribute('POP' + d)
                    pop[d] += value
                except TypeError:
                    pass

        with (self.project/'population.csv').open('w') as w:
            w.write('annee, demographie\n')
            for d, n in pop.items():
                w.write('20' + d + ',' + str(pop[d]) + '\n')

        # Rasterisations
        grid = QgsVectorLayer(str(self.pixResDir/'demography/stat_grid.shp'), 'grid')
        self.extent = grid.extent()
        ds = gdal.Open(str(self.pixResDir/'tif/restrictions/mask.tif'))
        self.proj = ds.GetProjection()
        self.geot = ds.GetGeoTransform()
        self.rows = ds.RasterYSize
        self.cols = ds.RasterXSize
        ds = None
        os.mkdir(str(self.project/'interet'))
        os.mkdir(str(self.project/'fitting'))
        arg_list = [
            (self.workspace/'data/communes.shp',
             self.project/'com_id.tif',
             'UInt16', self.crs, self.pixRes, self.extent,
             'ID_COM', None, False, False, self._debug),

            (self.pixResDir/'demography/stat_iris.shp',
             self.project/'iris_m2_hab.tif',
             'UInt16', self.crs, self.pixRes, self.extent,
             'm2_hab_med', None, False, False, self._debug),

            (self.pixResDir/'demography/stat_grid.shp',
             self.project/'demographie.tif',
             'UInt16', self.crs, self.pixRes, self.extent,
             'pop', None, False, False, self._debug),

            (self.pixResDir/'demography/stat_grid.shp',
             self.project/'surface_bati_res_pla.tif',
             'UInt32', self.crs, self.pixRes, self.extent,
             'sbp', None, False, False, self._debug),

            (self.pixResDir/'demography/stat_grid.shp',
             self.project/'surface_bati_res_sol.tif',
             'UInt16', self.crs, self.pixRes, self.extent,
             'sbr', None, False, False, self._debug),

            (self.workspace/'data/ocsol.shp',
             self.project/'classes_ocsol.tif',
             'UInt16', self.crs, self.pixRes, self.extent,
             'code_orig', None, False, False, self._debug)]

        if (self.workspace/'data/plu.shp').exists():
            arg_list.append((self.workspace/'data/plu.shp',
                             self.project/'interet/plu_restriction.tif',
                             'Byte', self.crs, self.pixRes, self.extent,
                             'restrict', None, False, False, self._debug))
            arg_list.append((self.workspace/'data/plu.shp',
                             self.project/'interet/plu_priorite.tif',
                             'Byte', self.crs, self.pixRes, self.extent,
                             'priority', None, False, False, self._debug))
            self.pluPrio = True

        self._get_jobs(rasterize, arg_list)

        # Conversion des rasters de distance
        mask = to_nparray(self.pixResDir/'tif/restrictions/mask.tif', np.byte)
        distance_rou = to_nparray(self.pixResDir/'tif/interets/distance_routes.tif', np.float32)
        rou = np.where(distance_rou > -1, 1 - (distance_rou / np.amax(distance_rou)), 0)
        rou = np.where(mask == 1, 0, rou)
        dat = self.pixResDir/'tif/interets/distance_arrets_transport.tif'
        distance_tra = to_nparray(dat, np.float32)
        tra = np.where(distance_tra > -1, 1 - (distance_tra / np.amax(distance_tra)), 0)
        tra = np.where(mask == 1, 0, tra)
        # Conversion et aggregation des rasters de densite SIRENE
        with (self.localData/'sirene_poids.csv').open('r') as csvFile:
            reader = csv.reader(csvFile)
            next(reader, None)
            poidsSirene = {rows[0]: int(rows[1]) for rows in reader}
        admin = to_nparray(self.pixResDir/'tif/interets/densite_administratif.tif', np.float32)
        comm = to_nparray(self.pixResDir/'tif/interets/densite_commercial.tif', np.float32)
        enseign = to_nparray(self.pixResDir/'tif/interets/densite_enseignement.tif', np.float32)
        medical = to_nparray(self.pixResDir/'tif/interets/densite_medical.tif', np.float32)
        recreatif = to_nparray(self.pixResDir/'tif/interets/densite_recreatif.tif', np.float32)

        # Normalisation des valeurs entre 0 et 1
        admin = np.where(admin != -9999, admin / np.amax(admin), 0)
        comm = np.where(comm != -9999, comm / np.amax(comm), 0)
        enseign = np.where(enseign != -9999, enseign / np.amax(enseign), 0)
        medical = np.where(medical != -9999, medical / np.amax(medical), 0)
        recreatif = np.where(recreatif != -9999, recreatif / np.amax(recreatif), 0)
        # Ponderation
        sirene = ((admin * poidsSirene['administratif']) + (comm * poidsSirene['commercial'])
                  + (enseign * poidsSirene['enseignement']) + (medical * poidsSirene['medical'])
                  + (recreatif * poidsSirene['recreatif'])) / sum(poidsSirene.values())
        sirene = (sirene / np.amax(sirene)).astype(np.float32)
        sirene = np.where(mask == 1, 0, sirene)

        # Creation du raster de restriction (sans PLU)
        slope = to_nparray(str(self.pixResDir/'tif/restrictions/slope.tif'))
        slopeMask = np.where(slope > self.maxSlope, 1, 0).astype(np.byte)
        # Fusion
        restriction = np.where(slopeMask == 1, 1, 0)
        restriction = self._restriction_array_filter(restriction)
        restriction = np.where(mask == 1, 1, restriction)

        # Traitement de l'interêt ecologique
        if (self.pixResDir/'tif/interets/ecologie.tif').exists():
            ecologie = to_nparray(self.pixResDir/'tif/interets/ecologie.tif', np.float32)
            non_imp_eco = np.where(mask == 1, 0, 1 - ecologie)
            to_tiff(non_imp_eco, 'float32', self.proj, self.geot,
                    self.project/'interet/non-importance_ecologique.tif', verbose=self._debug)
        else:
            rasterize(self.workspace/'data/ocsol.shp',
                      self.project/'interet/non-importance_ecologique.tif',
                      'Float32', self.crs, self.pixRes, self.extent,
                      'interet', verbose=self._debug)

        # Situation de depart
        batiResSol = to_nparray(self.project/'surface_bati_res_sol.tif', np.uint16)
        batiResPla = to_nparray(self.project/'surface_bati_res_pla.tif', np.uint16)
        ratioPlaSol = np.where(batiResSol != 0, batiResPla / batiResSol, 0).astype(np.float32)
        to_tiff(ratioPlaSol, 'float32', self.proj, self.geot,
                self.project/'ratio_plancher_sol.tif', verbose=self._debug)
        # Export final
        to_tiff(restriction, 'byte', self.proj, self.geot,
                self.project/'interet/restriction_totale.tif', verbose=self._debug)
        to_tiff(sirene, 'float32', self.proj, self.geot,
                self.project/'interet/densite_sirene.tif', verbose=self._debug)
        to_tiff(rou, 'float32', self.proj, self.geot,
                self.project/'interet/proximite_routes.tif', verbose=self._debug)
        to_tiff(tra, 'float32', self.proj, self.geot,
                self.project/'interet/proximite_transport.tif', verbose=self._debug)

        if (self.localData/'interet_poids.csv').exists():
            copyfile(str(self.localData/'interet_poids.csv'),
                     str(self.project/'interet/poids.csv'))

        exports = [
            (str(self.pixResDir/'tif/iris_id.tif'), 'iris_id.tif'),
            (str(self.pixResDir/'tif/artif/evo_artif.csv'), 'evolution_artificialisation.csv'),
            (str(self.pixResDir/'tif/artif/urban.tif'), 'urbanisation.tif'),
            (str(self.pixResDir/'tif/artif/built_cells.tif'), 'cellules_baties.tif'),
            (str(self.pixResDir/'tif/artif/builds.tif'), 'surface_bati.tif'),
            (str(self.pixResDir/'tif/artif/artif.tif'), 'artificialisation.tif'),
            (str(self.pixResDir/'tif/restrictions/mask.tif'), 'masque.tif'),
            (str(self.pixResDir/'tif/artif/tx_artif.tif'), 'taux_artificialisation.tif'),
            (str(self.pixResDir/'tif/artif/srf_roads.tif'), 'surface_routes_parkings.tif'),
            (str(self.pixResDir/'tif/artif/iris_tx_sbr.tif'), 'iris_tx_bati_res.tif'),
        ]
        for e in exports:
            i, o = e[0], e[1]
            copyfile(i, str(self.project/o))
            if self._interface == 'cli' and self._debug:
                print('Copy ' + i + ' --> ' + str(self.project/o))

        for file in os.listdir(str(self.workspace/'data/fitting')):
            orig = str(self.workspace/('data/fitting/' + file))
            dest = str(self.project/('fitting/' + file))
            ignore = False
            if file in ['zone', 'com', 'iris']:
                copytree(orig, dest, ignore=ignore_patterns('*.png'))
            elif file in ['corresp_com.csv', 'corresp_iris.csv']:
                copyfile(orig, dest)
            else:
                ignore = True
            if not ignore and (self._interface == 'cli' and self._debug):
                print('Copy ' + orig + ' --> ' + dest)

        with (self.project/'settings.csv').open('w') as w:
            for k, v in self._settings.items():
                w.write(k + ', ' + str(v) + '\n')

        return True

    def _write_logs(self, logs, newline=True):
        """Journal des etapes, surtout utile pour ecrire le temps d'execution de chaque etape"""

        if isinstance(logs, str):
            with self._log_file.open('a') as logfile:
                logfile.write(logs)
                if newline:
                    logfile.write('\n')
        elif isinstance(logs, list):
            with self._log_file.open('a') as logfile:
                for l in logs:
                    logfile.write(l)
                    if newline:
                        logfile.write('\n')

    def _write_qgs_log(self, message: str, tag: str, level: str):
        """Pour enregistrer tous les logs de QGIS ; print() si mode debug"""

        line = '{}({}):   {}'.format(tag, level, message)
        with self._qgs_log_file.open('a') as logfile:
            logfile.write(line + '\n')
        if self._debug:
            print(line)

    def _check_settings(self):
        localData = self._settings['localData']
        self.studyAreaName = localData.parts[len(localData.parts)-1]
        if len(self._settings['dpt'].split(',')) == 1:
            self._mono_dpt = True
        else:
            self._mono_dpt = False

        pixRes = self._settings['pixRes']
        if not 100 >= pixRes >= 10:
            self._settings['pixRes'] = 10 if pixRes < 10 else 100

    def _get_step_vars(self, key):
        """Retourne le description, dossier et fonction d'une etape"""

        return self._steps[key][0], self._steps[key][1], self._steps[key][2]

    def _get_jobs(self, function, arg_list, multi=False, use_qgis_manager=False):
        """Fonction pour adapter la strategie en terme de multi-taches"""

        if self.speed and (multi or use_qgis_manager):
            if multi:
                pool = Pool(self.nb_cores)
                jobs = []
                for a in arg_list:
                    jobs.append(pool.apply_async(function, a))
                pool.close()
                pool.join()
                pool.terminate()
            elif use_qgis_manager:
                for a in arg_list:
                    self._task_manager.addTask(PreparationSubTask(function, a))
                for t in self._task_manager.tasks():
                    t.waitForFinished(600000)  # 1 minute

        else:
            for a in arg_list:
                function(*a)

        return True

    def _find_bdtopo(self):
        """Pour trouver les emplacements de la BDTOPO et les dates utiles dans les metadonnees"""

        self._bdt2005 = []
        path = self.globalData / 'bdtopo/2005'
        dirs = os.listdir(str(path/'DONNEES'))
        for dpt in self.dpt:
            datamatch = r'D0' + dpt + '(0[0-9])_AV-3D_LAMB93'
            # metapath = None
            data = None
            for d in dirs:
                res = re.match(datamatch, d)
                if res:
                    data = path/('DONNEES/' + d)
                    self._bdt2005.append(data)
                    # Pour regarder la date des PVA (on considere plutot
                    # que toutes les donnees sont bonnes pour 2005)
                    #
                    # pref = res.group(1)
                    # file = 'D0' + dpt + pref + '_AV-3D_LAMB93_DatePVA.txt'
                    # metapath = path/('METADONNEES/' + file)
                    break
            # with open(metapath, 'r') as r:
            #     line = r.readline()
            #     bdt2005pva = line[-5:-1]

        self._bdt2009 = []
        path = self.globalData / 'bdtopo/2009'
        for d in os.listdir(str(path)):
            if '_DONNEES_LIVRAISON_' in d and os.path.isdir(str(path/d)):
                bdt = path/d
                break
        for dpt in self.dpt:
            name = r'BDT_2-0_SHP_LAMB93_D0' + dpt + '-ED093'
            self._bdt2009.append(bdt/name)

        self._bdt2012 = []
        path = self.globalData / 'bdtopo/2012'
        for d in os.listdir(str(path)):
            if '_DONNEES_LIVRAISON_' in d and os.path.isdir(str(path/d)):
                bdt = path/d
                break
        for dpt in self.dpt:
            name = r'BDT_2-1_SHP_LAMB93_D0' + dpt + '-ED122'
            if dpt != '12':
                data = bdt/name
                self._bdt2012.append(data)

        self._bdt2016 = []
        path = self.globalData / 'bdtopo/2016'
        for dpt in self.dpt:
            bdt = path/('BDTOPO_2-1_TOUSTHEMES_SHP_LAMB93_D0' + dpt + '_2016-03-24/BDTOPO')
            name = 'BDT_2-1_SHP_LAMB93_D0' + dpt + '-ED161'
            dirs = os.listdir(str(bdt))
            for d in dirs:
                if '_DONNEES_LIVRAISON_' in d and os.path.isdir(str(bdt/d)):
                    data = bdt/(d + '/' + name)
                elif '_METADONNEES_LIVRAISON_' in d and os.path.isdir(str(bdt/d)):
                    file = 'IGNF_BDTOPOr_2-1_SHP_LAMB93_D0' + dpt + '.xml'
                    metadata = bdt/(d + '/' + name + '/' + file)

            dates_file = self.localData/('dates_bdtopo_2016_dpt' + dpt + '.txt')
            if not dates_file.exists():
                dates = get_bdt_metadata(metadata)
                with dates_file.open('w') as w:
                    w.write('dpt' + dpt + ' : ' + str(dates) + '\n')
                if self._debug and self._interface == 'cli':
                    print('\n    metadata 2016 : ' + str(dates))
            self._bdt2016.append(data)

        self._bdt2018 = []
        path = self.globalData / 'bdtopo/2018'
        for dpt in self.dpt:
            bdt = path/('BDTOPO_2-2_TOUSTHEMES_SHP_LAMB93_D0' + dpt + '_2018-09-25/BDTOPO')
            name = 'BDT_2-2_SHP_LAMB93_D0' + dpt + '-ED182'
            dirs = os.listdir(str(bdt))
            for d in dirs:
                if '_DONNEES_LIVRAISON_' in d and os.path.isdir(str(bdt/d)):
                    data = bdt/(d + '/' + name)
                elif '_METADONNEES_LIVRAISON_' in d and os.path.isdir(str(bdt/d)):
                    file = 'IGNF_BDTOPOr_2-2_SHP_LAMB93_D0' + dpt + '-ED182.xml'
                    metadata = bdt/(d + '/' + name + '/' + file)

            dates_file = self.localData/('dates_bdtopo_2018_dpt' + dpt + '.txt')
            if not dates_file.exists():
                dates = get_bdt_metadata(metadata)
                with dates_file.open('w') as w:
                    w.write('dpt' + dpt + ' : ' + str(dates) + '\n')
                if self._debug and self._interface == 'cli':
                    print('    metadata 2018 : ' + str(dates))
            self._bdt2018.append(data)

    def _iris_extractor(self, iris, overlay, csvdir):
        """Jointure avec donnees INSEE et extraction des IRIS dans la zone"""

        # Conversion des chaînes en nombre
        csvPop = {}
        csvLog = {}
        for d in ['06', '07', '08', '09', '10', '12', '14', '15']:
            csvPop[d] = QgsVectorLayer(str(csvdir/('pop' + d + '.csv')))
            csvPop[d].addExpressionField('to_int("P' + d + '_POP")',
                                         QgsField('POP' + d, QVariant.Int))
            join_layers(iris, 'CODE_IRIS', csvPop[d], 'IRIS', ['P' + d + '_POP'])
            if int(d) > 9:
                csvLog[d] = QgsVectorLayer(str(csvdir/('log' + d + '.csv')))
                csvLog[d].addExpressionField('to_real("P' + d + '_TXRP")',
                                             QgsField('TXRP' + d, QVariant.Double))
                join_layers(iris, 'CODE_IRIS', csvLog[d], 'IRIS', ['P' + d + '_TXRP'])

        expression = '("POP15"-"POP06") / "POP06" / 5 * 100'
        iris.addExpressionField(expression, QgsField('EVPOP0615', QVariant.Double))

        # Extraction des quartiers IRIS avec jointures
        params = {
            'INPUT': iris,
            'PREDICATE': 6,
            'INTERSECT': overlay,
            'OUTPUT': 'memory:iris'
        }
        res = processing.run('native:extractbylocation', params, None, feedback)
        iris = res['OUTPUT']
        iris.addExpressionField('$id', QgsField('ID_IRIS', QVariant.Int, len=4))
        to_shp(reproj(res['OUTPUT'], self.crs), self.workspace/'data/iris.shp')
        params = expr.AGGR_IRIS
        params['INPUT'] = str(self.workspace/'data/iris.shp')
        params['OUTPUT'] = 'memory:communes'
        res = processing.run('qgis:aggregate', params, None, feedback)
        communes = res['OUTPUT']
        communes.addExpressionField('$id', QgsField('ID_COM', QVariant.Int, len=4))
        to_shp(communes, self.workspace/'data/communes.shp')
        return iris

    def _bdtopo_extractor(self, zone: QgsVectorLayer, zone_buffer: QgsVectorLayer):
        arg_list = []
        if self.dpt in [['30'], ['30', '34'], ['34', '30'], ['34']]:
            expr.BDT_BUILDS.append('E_BATI/CONSTRUCTION_LEGERE.SHP')
        if self._mono_dpt:
            arg_list = [(reproj(self._bdt2005[0]/shape, self.crs),
                         zone, self.workspace/'data/artif/2005') for shape in expr.OLD_BDT_BUILDS]
            arg_list += [(reproj(self._bdt2005[0]/shape, self.crs),
                          zone, self.workspace/'data/artif/2005') for shape in expr.OLD_BDT_ROADS]
            self._get_jobs(clip, arg_list)

            arg_list = [(reproj(self._bdt2009[0]/shape, self.crs),
                         zone, self.workspace/'data/artif/2009') for shape in expr.BDT_BUILDS]
            arg_list += [(reproj(self._bdt2009[0]/shape, self.crs),
                          zone, self.workspace/'data/artif/2009') for shape in expr.BDT_ROADS]
            self._get_jobs(clip, arg_list)

            arg_list = [(reproj(self._bdt2012[0]/shape, self.crs),
                         zone, self.workspace/'data/artif/2012') for shape in expr.BDT_BUILDS]
            arg_list += [(reproj(self._bdt2012[0]/shape, self.crs),
                          zone, self.workspace/'data/artif/2012') for shape in expr.BDT_ROADS]
            self._get_jobs(clip, arg_list)

            arg_list = [(reproj(self._bdt2016[0]/shape, self.crs),
                         zone, self.workspace/'data/artif/2016') for shape in expr.BDT_BUILDS]
            arg_list += [(reproj(self._bdt2016[0]/shape, self.crs),
                          zone, self.workspace/'data/artif/2016') for shape in expr.BDT_ROADS]
            self._get_jobs(clip, arg_list)

            arg_list = [(reproj(self._bdt2018[0]/shape, self.crs),
                         zone, self.workspace/'data/artif/2018') for shape in expr.BDT_BUILDS]
            arg_list += [(reproj(self._bdt2018[0]/shape, self.crs),
                          zone, self.workspace/'data/artif/2018') for shape in expr.BDT_ROADS]
            self._get_jobs(clip, arg_list)

            arg_list = [(reproj(self._bdt2016[0]/shape, self.crs),
                         zone_buffer, self.workspace/'data/pai/') for shape in expr.BDT_SERV]
            shape = 'I_ZONE_ACTIVITE/SURFACE_ACTIVITE.SHP'
            arg_list.append((reproj(self._bdt2016[0]/shape, self.crs),
                             zone, self.workspace/'data/pai'))
            shape = 'B_VOIES_FERREES_ET_AUTRES/TRONCON_VOIE_FERREE.SHP'
            arg_list.append((reproj(self._bdt2016[0]/shape, self.crs),
                             zone, self.workspace/'data/transp'))
            shape = 'E_BATI/CIMETIERE.SHP'
            arg_list.append((reproj(self._bdt2016[0]/shape, self.crs),
                             zone, self.workspace/'data/restrict'))
            self._get_jobs(clip, arg_list)

        # Fusion un peu brutale des multiples departements. Ici, arg_list est une liste de tuple
        # qui contiennent le SHP decoupe et repojete retourne par clip()
        else:
            arg_list = [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2005],
                         self.workspace/'data/artif/2005') for shape in expr.OLD_BDT_BUILDS]
            arg_list += [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2005],
                          self.workspace/'data/artif/2005') for shape in expr.OLD_BDT_ROADS]
            self._get_jobs(merge, arg_list)

            arg_list = [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2009],
                         self.workspace/'data/artif/2009') for shape in expr.BDT_BUILDS]
            arg_list += [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2009],
                          self.workspace/'data/artif/2009') for shape in expr.BDT_ROADS]
            self._get_jobs(merge, arg_list)

            arg_list = [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2012],
                         self.workspace/'data/artif/2012') for shape in expr.BDT_BUILDS]
            arg_list += [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2012],
                          self.workspace/'data/artif/2012') for shape in expr.BDT_ROADS]
            self._get_jobs(merge, arg_list)

            arg_list = [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2016],
                         self.workspace/'data/artif/2016') for shape in expr.BDT_BUILDS]
            arg_list += [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2016],
                          self.workspace/'data/artif/2016') for shape in expr.BDT_ROADS]
            self._get_jobs(merge, arg_list)

            arg_list = [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2018],
                         self.workspace/'data/artif/2018') for shape in expr.BDT_BUILDS]
            arg_list += [([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2018],
                          self.workspace/'data/artif/2018') for shape in expr.BDT_ROADS]
            self._get_jobs(merge, arg_list)

            arg_list = [([clip(reproj(bdt/shape, self.crs), zone_buffer) for bdt in self._bdt2016],
                         self.workspace/'data/pai/') for shape in expr.BDT_SERV]
            shape = 'I_ZONE_ACTIVITE/SURFACE_ACTIVITE.SHP'
            arg_list.append(([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2016],
                             self.workspace/'data/pai'))
            shape = 'B_VOIES_FERREES_ET_AUTRES/TRONCON_VOIE_FERREE.SHP'
            arg_list.append(([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2016],
                             self.workspace/'data/transp'))
            shape = 'E_BATI/CIMETIERE.SHP'
            arg_list.append(([clip(reproj(bdt/shape, self.crs), zone) for bdt in self._bdt2016],
                             self.workspace/'data/restrict'))
            self._get_jobs(merge, arg_list)
        if 'E_BATI/CONSTRUCTION_LEGERE.SHP' in expr.BDT_BUILDS:
            expr.BDT_BUILDS.remove('E_BATI/CONSTRUCTION_LEGERE.SHP')

    def _roads_extractor(self, zone):
        # Traitement des autoroutes : bande de 100m de part et d'autre
        routes = QgsVectorLayer(str(self.workspace/'data/artif/2016/route.shp'), 'route')
        params = {
            'INPUT': routes,
            'EXPRESSION': """ "NATURE" = 'Autoroute' """,
            'METHOD': 0
        }
        res = processing.run('qgis:selectbyexpression', params, None, feedback)
        if routes.selectedFeatureCount() > 0:
            params = {
                'INPUT': routes,
                'OUTPUT': 'memory:autouroutes'
            }
            res = processing.run('native:saveselectedfeatures', params, None, feedback)
            params = {
                'INPUT': fix_geom(res['OUTPUT']),
                'DISTANCE': 100,
                'SEGMENTS': 5,
                'END_CAP_STYLE': 0,
                'JOIN_STYLE': 0,
                'MITER_LIMIT': 2,
                'DISSOLVE': True,
                'OUTPUT': str(self.workspace/'data/restrict/tampon_autoroutes.shp')
            }
            processing.run('native:buffer', params, None, feedback)
            routes.removeSelection()

        # Traitement des autres routes : bande de 75m ===> voir Loi Barnier
        params = {
            'INPUT': routes,
            'EXPRESSION': """ "NATURE" != 'Autoroute' AND to_int("IMPORTANCE") <= 2 """,
            'OUTPUT': 'memory:route_primaire',
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        params = {
            'INPUT': res['OUTPUT'],
            'DISTANCE': 75,
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'JOIN_STYLE': 0,
            'MITER_LIMIT': 2,
            'DISSOLVE': True,
            'OUTPUT': str(self.workspace/'data/restrict/tampon_routes_importantes.shp')
        }
        processing.run('native:buffer', params, None, feedback)

        # Tampons pour calculer l'artif
        routes = QgsVectorLayer(str(self.workspace/'data/artif/2005/troncon_route.shp'))
        params = {
            'INPUT': routes,
            'EXPRESSION': """ "NATURE" != 'En construction' AND "NATURE" IS NOT NULL """,
            'OUTPUT': 'memory:routes',
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        routes = res['OUTPUT']
        routes.addExpressionField(expr.ROADS_WIDTH, QgsField('buffer', QVariant.Double))
        to_shp(routes, str(self.workspace/'data/artif/2005/routes_clean.shp'))
        params = {
            'INPUT': routes,
            'FIELD': 'buffer',
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'DISSOLVE': False,
            'OUTPUT': 'memory:buffer_routes'
        }
        res = processing.run('qgis:variabledistancebuffer', params, None, feedback)
        clip(res['OUTPUT'], zone, self.workspace/'data/artif/2005/')

        routes = QgsVectorLayer(str(self.workspace/'data/artif/2009/route.shp'), 'route')
        params = {
            'INPUT': routes,
            'EXPRESSION': expr.ROADS,
            'OUTPUT': 'memory:routes',
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        routes = res['OUTPUT']
        routes.addExpressionField(expr.ROADS_WIDTH, QgsField('buffer', QVariant.Double))
        to_shp(routes, str(self.workspace/'data/artif/2009/routes_clean.shp'))
        params = {
            'INPUT': routes,
            'FIELD': 'buffer',
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'DISSOLVE': False,
            'OUTPUT': 'memory:buffer_routes'
        }
        res = processing.run('qgis:variabledistancebuffer', params, None, feedback)
        clip(res['OUTPUT'], zone, self.workspace/'data/artif/2009')

        routes = QgsVectorLayer(str(self.workspace/'data/artif/2012/route.shp'), 'route')
        params = {
            'INPUT': routes,
            'EXPRESSION': expr.ROADS,
            'OUTPUT': 'memory:routes',
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        routes = res['OUTPUT']
        routes.addExpressionField(expr.ROADS_WIDTH, QgsField('buffer', QVariant.Double))
        to_shp(routes, str(self.workspace/'data/artif/2012/routes_clean.shp'))
        params = {
            'INPUT': routes,
            'FIELD': 'buffer',
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'DISSOLVE': False,
            'OUTPUT': 'memory:buffer_routes'
        }
        res = processing.run('qgis:variabledistancebuffer', params, None, feedback)
        clip(res['OUTPUT'], zone, self.workspace/'data/artif/2012')

        routes = QgsVectorLayer(str(self.workspace/'data/artif/2016/route.shp'), 'route')
        params = {
            'INPUT': routes,
            'EXPRESSION': expr.ROADS,
            'OUTPUT': 'memory:routes',
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        routes = res['OUTPUT']

        routes.addExpressionField(expr.ROADS_WIDTH, QgsField('buffer', QVariant.Double))
        to_shp(routes, str(self.workspace/'data/artif/2016/routes_clean.shp'))
        params = {
            'INPUT': routes,
            'FIELD': 'buffer',
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'DISSOLVE': False,
            'OUTPUT': 'memory:buffer_routes'}
        res = processing.run('qgis:variabledistancebuffer', params, None, feedback)
        clip(res['OUTPUT'], zone, self.workspace/'data/artif/2016/')

        routes = QgsVectorLayer(str(self.workspace/'data/artif/2018/route.shp'), 'route')
        params = {
            'INPUT': routes,
            'EXPRESSION': expr.ROADS,
            'OUTPUT': 'memory:routes',
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        routes = res['OUTPUT']

        routes.addExpressionField(expr.ROADS_WIDTH, QgsField('buffer', QVariant.Double))
        to_shp(routes, str(self.workspace/'data/artif/2018/routes_clean.shp'))
        params = {
            'INPUT': routes,
            'FIELD': 'buffer',
            'SEGMENTS': 5,
            'END_CAP_STYLE': 0,
            'DISSOLVE': False,
            'OUTPUT': 'memory:buffer_routes'}
        res = processing.run('qgis:variabledistancebuffer', params, None, feedback)
        clip(res['OUTPUT'], zone, self.workspace/'data/artif/2018/')

    def _dgfip_extractor(self, zone):
        if self._mono_dpt:
            dgfip = self.globalData/('majic/' + self.dpt[0] + '/parcelles_non_res.shp')
            if dgfip.exists():
                parcelles = QgsVectorLayer(str(dgfip), 'parcelles_non_res')
                clip(reproj(parcelles, self.crs), zone, self.workspace/'data/restrict/')
        else:
            layer_list = []
            for dpt in self.dpt:
                dgfip = self.globalData/('majic/' + dpt + '/parcelles_non_res.shp')
                if dgfip.exists():
                    layer_list.append(clip(reproj(dgfip, self.crs), zone))
            to_shp(merge(layer_list), self.workspace/'data/restrict/parcelles_non_res.shp')

    def _ocs_extractor(self, zone):
        """Aggregation de classes et attribution d'une valeur d'interêt"""

        oso = False
        if not (self.localData/'ocsol.shp').exists():
            oso = True
            ocs_list = []
            for dpt in self.dpt:
                path = self.globalData/('oso/departement_' + dpt + '.shp')
                layer = QgsVectorLayer(str(path), 'oso')
                layer.dataProvider().createSpatialIndex()
                ocs_list.append(clip(reproj(fix_geom(layer), self.crs), zone))
            if len(ocs_list) > 1:
                ocsol = merge(ocs_list)
            else:
                ocsol = ocs_list[0]
        else:
            layer = QgsVectorLayer(str(self.localData/'ocsol.shp'), 'ocsol')
            layer.dataProvider().createSpatialIndex()
            try:
                ocsol = clip(reproj(layer, self.crs), zone)
            except QgsProcessingException:
                ocsol = clip(reproj(fix_geom(layer), self.crs), zone)

        if oso:
            params = {
                'INPUT': ocsol,
                'COLUMN': expr.OSO_CLASSES,
                'OUTPUT': 'memory:ocsol'
            }
            res = processing.run('qgis:deletecolumn', params, None, feedback)
            ocsol = res['OUTPUT']
            ocsol.addExpressionField(expr.SIMPLE_OSO,
                                     QgsField('classe_2', QVariant.String, len=30))
            ocsol.addExpressionField('"Classe"',
                                     QgsField('code_orig', QVariant.Int))
        else:
            # 1 = Urbain, 2 = Agricole, 3 = Espace naturel, 4 = Zone humide, 5 = Surface en eau
            if self.studyAreaName in ['mtp', 'montpellier']:
                ocsol.addExpressionField(expr.SIMPLE_OCS_3M,
                                         QgsField('classe_2', QVariant.String, len=30))
                ocsol.addExpressionField('"c2015_niv3"',
                                         QgsField('code_orig', QVariant.Int))
            elif self.studyAreaName in ['nim', 'nimes']:
                ocsol.addExpressionField(expr.SIMPLE_OCS_NIM,
                                         QgsField('classe_2', QVariant.String, len=30))
                ocsol.addExpressionField('"NIV1_12"',
                                         QgsField('code_orig', QVariant.Int))
            elif self.studyAreaName in ['gpsl', 'ccgpsl']:
                ocsol.addExpressionField(expr.SIMPLE_CLC,
                                         QgsField('classe_2', QVariant.String, len=30))
                ocsol.addExpressionField('"CODE_12"',
                                         QgsField('code_orig', QVariant.Int))

        ocsol.addExpressionField(expr.OCS_INTEREST,
                                 QgsField('interet', QVariant.Double))
        to_shp(ocsol, self.workspace/'data/ocsol.shp')

    def _env_restrict(self, layer_list, overlay, outdir):
        """Traitement des zonages reglementaires pour la couche de restrictions"""

        if not outdir.exists():
            os.mkdir(str(outdir))

        for file in layer_list:
            intersects = False
            name = os.path.basename(file).split('.')[0].lower()

            if 'PARCS_NATIONAUX_OCCITANIE_L93.shp' in file:
                name = name.replace('_occitanie_l93', '')
                layer = QgsVectorLayer(str(file), name)
                layer.setProviderEncoding('ISO-8859-14')
                params = {
                    'INPUT': layer,
                    'EXPRESSION': """ "CODE_R_ENP" = 'CPN' """,
                    'OUTPUT': 'memory:coeur_parcs_nationaux',
                    'FAIL_OUTPUT': 'memory:'
                }
                res = processing.run('native:extractbyexpression', params, None, feedback)
                layer = res['OUTPUT']
            elif '_OCCITANIE_L93' in file:
                name = name.replace('_occitanie_l93', '')
                layer = QgsVectorLayer(str(file), name)
                layer.setProviderEncoding('ISO-8859-14')
            elif '_OCC_L93' in file:
                name = name.replace('_occ_l93', '')
                layer = QgsVectorLayer(str(file), name)
                layer.setProviderEncoding('ISO-8859-14')
            elif '_s_r76' in file:
                name = name.replace('_s_r76', '')
                layer = QgsVectorLayer(str(file), name)
                layer.setProviderEncoding('UTF-8')
            elif '_r73' in file:
                name = name.replace('_r73', '')
                layer = QgsVectorLayer(str(file), name)
                layer.setProviderEncoding('ISO-8859-14')

            layer.dataProvider().createSpatialIndex()
            i = 1
            while i < layer.featureCount() and not intersects:
                if layer.getFeature(i).geometry().intersects(overlay.getFeature(1).geometry()):
                    intersects = True
                i += 1
            if intersects:
                reproj(clip(layer, overlay), self.crs, outdir)

    def _transp_extractor(self, zone_buffer):
        transList = []
        if (self.localData/'bus.shp').exists():
            clip(reproj(self.localData/'bus.shp', self.crs),
                 zone_buffer, self.workspace/'data/transp/')
            bus = QgsVectorLayer(str(self.workspace/'data/transp/bus.shp'), 'bus')
            transList.append(bus)

        params = {
            'INPUT': str(self.workspace/'data/pai/transport.shp'),
            'EXPRESSION': """ "NATURE" = 'Station de metro' """,
            'OUTPUT': str(self.workspace/'data/transp/transport_pai.shp'),
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        transList.append(res['OUTPUT'])
        params = {
            'INPUT': str(self.workspace/'data/pai/transport.shp'),
            'EXPRESSION': """ "NATURE" LIKE 'Gare voyageurs %' """,
            'OUTPUT': str(self.workspace/'data/transp/gare.shp'),
            'FAIL_OUTPUT': 'memory:'
        }
        res = processing.run('native:extractbyexpression', params, None, feedback)
        transList.append(res['OUTPUT'])
        params = {
            'LAYERS': transList,
            'CRS': self.crs,
            'OUTPUT': str(self.workspace/'data/transp/arrets_transport.shp')
        }
        processing.run('native:mergevectorlayers', params, None, feedback)

    def _ppri_extractor(self, zone):
        # Couche facultative du PPRI local : extraction des zones de restriction
        if (self.localData/'ppri.shp').exists():
            ppri = QgsVectorLayer(str(self.localData/'ppri.shp'))
            # Attention, le zonage R varie : ici on cherche Rouge mais peut signifier Residuel...
            expression = """ "CODEZONE" LIKE 'R%' """
            params = {
                'INPUT': fix_geom(ppri),
                'EXPRESSION': expression,
                'OUTPUT': 'memory:ppri',
                'FAIL_OUTPUT': 'memory:'
            }
            res = processing.run('native:extractbyexpression', params, None, feedback)
            ppri = res['OUTPUT']
            clip(reproj(ppri, self.crs), zone, self.workspace/'data/restrict/')

        # Sinon on cherche une couche PPRI du departement
        elif self._mono_dpt and self.dpt[0] in ['30', '34']:
            if self.dpt[0] == '30':
                e = """ "CODEZONE" LIKE 'TF%' OR "CODEZONE" LIKE '%-NU' OR "CODEZONE" = 'F-U'"""
                path = self.globalData/('ppri/L_ZONE_REG_PPRI_S_030.shp')
                ppri = QgsVectorLayer(str(path), 'ppri')
            elif self.dpt[0] == '34':
                e = """ "CODEZONE" LIKE 'R%' """
                path = self.globalData/('ppri/N_ZONE_REG_PPRI_S_034.shp')
                ppri = QgsVectorLayer(str(path), 'ppri')
            params = {
                'INPUT': fix_geom(ppri),
                'EXPRESSION': e,
                'OUTPUT': 'memory:ppri',
                'FAIL_OUTPUT': 'memory:'
            }
            res = processing.run('native:extractbyexpression', params, None, feedback)
            clip(reproj(res['OUTPUT'], self.crs), zone, self.workspace/'data/restrict/')

        elif not self._mono_dpt and self.dpt in [['30', '34'], ['34', '30']]:
            e = """ "CODEZONE" LIKE 'TF%' OR "CODEZONE" LIKE '%-NU' OR "CODEZONE" = 'F-U'"""
            path = self.globalData/('ppri/L_ZONE_REG_PPRI_S_030.shp')
            ppri30 = QgsVectorLayer(str(path), 'ppri')
            params = {
                'INPUT': fix_geom(ppri30),
                'EXPRESSION': e,
                'OUTPUT': 'memory:ppri',
                'FAIL_OUTPUT': 'memory:'
            }
            ppri30 = processing.run('native:extractbyexpression', params, None, feedback)
            e = """ "CODEZONE" LIKE 'R%' """
            path = self.globalData/('ppri/N_ZONE_REG_PPRI_S_034.shp')
            ppri34 = QgsVectorLayer(str(path), 'ppri')
            params = {
                'INPUT': fix_geom(ppri34),
                'EXPRESSION': e,
                'OUTPUT': 'memory:ppri',
                'FAIL_OUTPUT': 'memory:'
            }
            ppri34 = processing.run('native:extractbyexpression', params, None, feedback)
            merge([clip(reproj(ppri30, self.crs), zone), clip(reproj(ppri34, self.crs), zone)],
                  self.workspace/'data/restrict/')

        else:
            if self.reg == ['R91']:
                path = self.globalData/'azi/EX_LR_ATLAS_ZI.shp'
            if self.reg == ['R73']:
                path = self.globalData/'azi/EX_MP_ATLAS_ZI.shp'
            azi = reproj(QgsVectorLayer(str(path), 'azi'), self.crs)
            intersects = False
            i = 1
            while i < azi.featureCount() and not intersects:
                if azi.getFeature(i).geometry().intersects(zone.getFeature(0).geometry()):
                    intersects = True
                i += 1
            if intersects:
                clip(fix_geom(azi), zone, self.workspace/'data/restrict/')

    def _plu_fixer(self, plu, overlay, outdir):
        """Corrige les geometries et reclasse un PLU"""

        plu.setProviderEncoding('utf-8')
        plu.dataProvider().createSpatialIndex()
        fields = list(f.name() for f in plu.fields())
        if 'type' in fields:
            plu.addExpressionField(expr.SIMPLE_PLU, QgsField('classe', QVariant.String, len=3))

        if self.studyAreaName in ['mtp', 'montpellier']:
            plu.addExpressionField(expr.RESTRICT_PLU_3M, QgsField('restrict', QVariant.Int, len=1))
            plu.addExpressionField(expr.PLU_PRIO_3M, QgsField('priority', QVariant.Int, len=1))
        else:
            plu.addExpressionField(expr.RESTRICT_PLU, QgsField('restrict', QVariant.Int, len=1))
            plu.addExpressionField(expr.PLU_PRIO, QgsField('priority', QVariant.Int, len=1))

        reproj(clip(fix_geom(plu), overlay), self.crs, outdir)

    def _building_cleaner(self,
                          buildings: QgsVectorLayer,
                          polygons: list,
                          points: list,
                          cleaned_out: Path,
                          removed_out: Path):
        """Nettoye la couche de bati et calcule les champs utiles pour estimer la population"""

        # Estimation du nombre d'etages
        expression = """ CASE
            WHEN "HAUTEUR" = 0 THEN 1
            WHEN "HAUTEUR" < 5 THEN 1
            ELSE floor("HAUTEUR"/""" + str(self.levelHeight) + """) END
        """
        buildings.addExpressionField(expression, QgsField('NB_NIV', QVariant.Int, len=3))
        # Aire utilisee pour le fitting
        buildings.addExpressionField('$area', QgsField('area', QVariant.Double, len=10, prec=2))
        # Nettoyage des batiments supposes trop grand ou trop petit pour être habites
        params = {
            'INPUT': buildings,
            'EXPRESSION': ' "area" < ' + str(self.minSurf) + ' OR "area" > ' + str(self.maxSurf),
            'METHOD': 0
        }
        processing.run('qgis:selectbyexpression', params, None, feedback)
        # Selection des batiments situes dans les polygones (WITHIN)
        for layer in polygons:
            params = {
                'INPUT': buildings,
                'PREDICATE': 6,
                'INTERSECT': layer,
                'METHOD': 1
            }
            processing.run('native:selectbylocation', params, None, feedback)
        # Selection si le batiment intersecte des points
        for layer in points:
            params = {
                'INPUT': buildings,
                'PREDICATE': 0,
                'INTERSECT': layer,
                'METHOD': 1
            }
            processing.run('native:selectbylocation', params, None, feedback)

        params = {
            'INPUT': buildings,
            'OUTPUT': str(removed_out)
        }
        processing.run('native:saveselectedfeatures', params, None, feedback)
        # Inversion de la selection pour export final
        buildings.invertSelection()
        params = {
            'INPUT': buildings,
            'OUTPUT': str(cleaned_out)
        }
        processing.run('native:saveselectedfeatures', params, None, feedback)

    def _restriction_vectors_list(self):
        """Retourne une liste de restrictions a partir de ce que contient le dossier de donnees"""

        params = ('Byte', self.crs, self.pixRes, self.extent, None, 1)
        files = ['agro_restriction',
                 'compensation',
                 'exclusion_manuelle',
                 'parcelles_non_res',
                 'tampon_autoroutes',
                 'surf_activ_non_com']
        args = []

        for f in files:
            src = self.workspace/('data/restrict/' + f + '.shp')
            if src.exists():
                dest = self.pixResDir/('tif/restrictions/' + f + '.tif')
                args.append((src, dest) + params)

        path = self.workspace/'data/artif/2016/bati_remarquable.shp'
        if path.exists():
            args.append((path, self.pixResDir/'tif/restrictions/bati_remarquable.tif') + params)

        has_ppri = False
        if (self.workspace/'data/restrict/ppri.shp').exists():
            has_ppri = True
            args.append((self.workspace/'data/restrict/ppri.shp',
                         self.pixResDir/'tif/restrictions/ppri.tif') + params)

        elif (self.workspace/'data/plu.shp').exists():
            layer = QgsVectorLayer(str(self.workspace/'data/plu.shp'), 'plu')
            fields = list(f.name() for f in layer.fields())
            if 'ppri' in fields:
                has_ppri = True
                args.append((self.workspace/'data/plu.shp',
                             self.pixResDir/'tif/restrictions/ppri.tif') + params)

        if not has_ppri and (self.workspace/'data/restrict/azi.shp').exists():
            args.append((self.workspace/'data/restrict/azi.shp',
                         self.pixResDir/'tif/restrictions/azi.tif') + params)

        return args

    def _compute_artif_raster(self, name: str, path: str, year: str, outdir: Path = None):
        """Genere un raster d'artificialisation a partir d'une rasterisation 1m"""

        if not os.path.exists(path):
            raise FileNotFoundError(path)
        else:
            tmpfile = str(self.tmpdir/(year + '_' + name + '.tif'))
            rasterize(path, tmpfile, 'Byte', self.crs, 1, self.extent, burn=1, verbose=self._debug)
            one_meter_array = to_nparray(tmpfile)
            new_array = block_reduce(one_meter_array, (self.pixRes, self.pixRes))
            del one_meter_array
            if outdir:
                outfile = outdir/(name + '.tif')
                to_tiff(new_array, 'uint16', self.proj, self.geot, outfile, verbose=self._debug)
            os.remove(tmpfile)
            return new_array

    def _restriction_array_filter(self, restriction):
        """Retourne des restrictions en fonction des fichiers presents avec prise en compte PLU"""

        prio = None
        path = self.project/'interet/plu_priorite.tif'
        if path.exists():
            prio = to_nparray(path)
        # Les rasters qui n'ont pas la priorité sur les ZAU
        test_list = [
            'agro_restriction.tif',
            'azi.tif',
            'ppri.tif',
            'surf_activ_non_com.tif',
            'zonages_protection.tif'
        ]
        for rast in test_list:
            rast_file = self.pixResDir/('tif/restrictions/' + rast)
            if rast_file.exists():
                new_rest = to_nparray(rast_file, np.byte)
                restriction = np.where(new_rest == 1, 1, restriction)
        # ZAU priority
        if prio is not None:
            restriction = np.where((prio == 1) & (restriction == 1), 0, restriction)
        # Les restrictions "fortes"
        test_list = [
            'bati_remarquable.tif',
            'cimetiere.tif',
            'compensation.tif',
            'exclusion_manuelle.tif',
            'surface_eau.tif',
            'tampon_autoroutes.tif',
            'tampon_routes_importantes.tif',
            'tampon_voies_ferrees.tif',
        ]
        for rast in test_list:
            rast_file = self.pixResDir/('tif/restrictions/' + rast)
            if rast_file.exists():
                new_rest = to_nparray(rast_file, np.byte)
                restriction = np.where(new_rest == 1, 1, restriction)

        return restriction


# Autres classes et fonctions utiles
class PreparationSubTask(QgsTask):
    """Implementation de la classe abstraite QgsTask"""

    def __init__(self, function, args):
        super().__init__(str(function) + ' - ' + str(args))
        self._function = function
        self._args = args

    def run(self):
        try:
            self._function(*self._args)
        except (KeyboardInterrupt, QgsProcessingException):
            raise

        return True


def join_layers(layer: QgsVectorLayer,
                field: str,
                join_layer: QgsVectorLayer,
                join_field: str,
                blacklist: list = [],
                prefix: str = ''):
    """Realise une jointure entre deux QgsVectorLayer"""

    j = QgsVectorLayerJoinInfo()
    j.setTargetFieldName(field)
    j.setJoinLayerId(join_layer.id())
    j.setJoinFieldName(join_field)
    j.setJoinFieldNamesBlackList(blacklist)
    j.setUsingMemoryCache(True)
    j.setPrefix(prefix)
    j.setJoinLayer(join_layer)
    layer.addJoin(j)


def clean_layer_name(name):
    if 'PAI_' in name:
        name = name.replace('PAI_', '')
    elif 'PPRI' in name:
        name = 'ppri'
    res = re.match('.*(_[0-9]{2}).*', name)
    res = None
    if res:
        name = name.replace(res.group(1), '')
    return name.lower()


def clip(file, overlay, outdir='memory:'):
    """Decoupe une couche avec gestion de l'encodage pour la BDTOPO"""

    if isinstance(file, QgsVectorLayer):
        layer = file
        layer.dataProvider().createSpatialIndex()
        name = file.name()
        params = {
            'INPUT': layer,
            'OVERLAY': overlay
        }
    else:
        path = str(file)
        name = os.path.basename(path).split('.')[0].lower()
        layer = QgsVectorLayer(path, name)
        layer.dataProvider().createSpatialIndex()
        params = {
            'INPUT': layer,
            'OVERLAY': overlay
        }

    if outdir == 'memory:':
        params['OUTPUT'] = outdir + name
    else:
        params['OUTPUT'] = str(outdir/(name + '.shp'))
    res = processing.run('native:clip', params, None, feedback)
    del layer

    if isinstance(res['OUTPUT'], QgsVectorLayer):
        return res['OUTPUT']

    return QgsVectorLayer(res['OUTPUT'], name)


def merge(layer_list, out='memory:'):
    """Pour fusionner un ensemble de couches a partir d'une liste"""

    name = layer_list[0].name()
    params = {
        'LAYERS': layer_list
    }
    if out == 'memory:':
        params['OUTPUT'] = out + name
    elif '.shp' in str(out):
        params['OUTPUT'] = str(out)
    else:
        params['OUTPUT'] = str(out/(name + '.shp'))
    res = processing.run('native:mergevectorlayers', params, None, feedback)

    if isinstance(res['OUTPUT'], QgsVectorLayer):
        return res['OUTPUT']

    return QgsVectorLayer(res['OUTPUT'], name)


def reproj(file, crs, outdir='memory:'):
    """Reprojection d'une couche"""

    if isinstance(file, QgsVectorLayer):
        layer = file
        name = layer.name()
        params = {
            'INPUT': layer,
            'TARGET_CRS': crs
        }
    else:
        path = str(file)
        name = os.path.basename(path).split('.')[0]
        name = clean_layer_name(name)
        layer = QgsVectorLayer(path, name)
        params = {
            'INPUT': layer,
            'TARGET_CRS': crs
        }

    if outdir == 'memory:':
        params['OUTPUT'] = outdir + name
    else:
        params['OUTPUT'] = str(outdir/(name + '.shp'))
    res = processing.run('native:reprojectlayer', params, None, feedback)
    del layer

    if isinstance(res['OUTPUT'], QgsVectorLayer):
        return res['OUTPUT']

    return QgsVectorLayer(res['OUTPUT'], name)


def fix_geom(file):
    """Repare les geometries d'une couche"""

    if isinstance(file, QgsVectorLayer):
        layer = file
        name = layer.name()
        params = {
            'INPUT': layer,
            'OUTPUT': 'memory:' + name
        }
    else:
        path = str(file)
        name = os.path.basename(path).split('.')[0].lower()
        layer = QgsVectorLayer(path, name)
        params = {
            'INPUT': layer,
            'OUTPUT': 'memory:' + name
        }
    del layer
    res = processing.run('native:fixgeometries', params, None, feedback)
    return res['OUTPUT']


def to_shp(layer, path):
    """Enregistre un objet QgsVectorLayer sur le disque"""

    writer = QgsVectorFileWriter(
        str(path),
        'utf-8',
        layer.fields(),
        layer.wkbType(),
        layer.sourceCrs(),
        'ESRI Shapefile'
    )
    writer.addFeatures(layer.getFeatures())
    # return QgsVectorLayer(str(path), layer.name())


def dem_extractor(directory, bbox: QgsRectangle) -> list:
    """Retourne les chemins des tuiles MNT dans la zone d'etude"""

    tileList = []
    for tile in os.listdir(str(directory)):
        if os.path.splitext(tile)[1] == '.asc':
            path = str(directory/tile)
            with open(path) as file:
                for i in range(5):
                    line = file.readline()
                    res = re.search(r'[a-z]*\s*([0-9.]*)', line)
                    if i == 0:
                        xSize = int(res.group(1))
                    elif i == 1:
                        ySize = int(res.group(1))
                    elif i == 2:
                        xMin = float(res.group(1))
                    elif i == 3:
                        yMin = float(res.group(1))
                    elif i == 4:
                        cellSize = float(res.group(1))

            xMax = xMin + xSize * cellSize
            yMax = yMin + ySize * cellSize
            tileExtent = QgsRectangle(xMin, yMin, xMax, yMax)
            if bbox.intersects(tileExtent):
                tileList.append(path)

    return tileList
