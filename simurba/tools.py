# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Tools

    a.k.a. utils

        copyright            : (C) 2018 by Vincent DELBAR

"""
import os
import re
import sys
import gdal
import numpy as np
import pandas as pd
from time import time
from shutil import rmtree
from pathlib import Path
from ast import literal_eval
from collections import OrderedDict


def printer(string: str):
    """Pour affichage dynamique de la progression"""

    if sys.platform == 'win32':
        sys.stdout.write("\r" + string)
    else:
        sys.stdout.write("\r\x1b[K" + string)
    sys.stdout.flush()


def set_parameters(default_settings: OrderedDict,
                   interface: str = 'cli',
                   param_str: str = None):
    """Une fonction pour faciliter la manipulation des paramètres."""

    param_list = list(default_settings.keys())
    settings = OrderedDict([(k, v[1]) for k, v in default_settings.items()])

    if param_str is not None:
        # Pour le plugin ou openmole dans le cas ou tous les parametres sont presents
        if len(param_str.split()) == len(default_settings):
            i = 0
            while i < len(param_list):
                k = param_list[i]
                v = param_str.split()[i]
                target_type = default_settings[k][0]
                if target_type == bool:
                    try:
                        v = literal_eval(v)
                    except ValueError:
                        sys.exit('Error with ' + k + ' = ' + v)
                    if interface == 'openmole' and type(v) == float:
                        v = True if v > 0.5 else False
                    settings[k] = v
                elif target_type == int:
                    settings[k] = round(float(v))
                else:
                    settings[k] = target_type(v)
                i += 1
        # Sinon les parametres sont de type clef=valeur
        elif '=' in param_str:
            for t in param_str.split():
                k, v = t.split('=')[0], t.split('=')[1]
                target_type = default_settings[k][0]
                if target_type == bool:
                    settings[k] = literal_eval(v)
                elif target_type == int:
                    settings[k] = round(float(v))
                else:
                    settings[k] = target_type(v)
        else:
            sys.exit('Error while parsing parameters string : ' + param_str)

    return settings


def find_qgis():
    """Retourne l'emplacement de QGIS sur la machine"""

    qgs_root = None
    qgs_pyqgis = None

    if sys.platform == 'linux':
        for d in ['/usr', '/usr/local', '/opt/qgis', '/opt/qgis-dev']:
            if Path(d + '/lib/qgis').exists():
                qgs_root = Path(d)
                qgs_pyqgis = str(qgs_root/'share/qgis/python')
    elif sys.platform == 'win32':
        for d in Path('C:/Program Files').iterdir():
            if 'QGIS 3' in str(d) or 'OSGeo4W64' in str(d):
                qgs_root = d
        if not qgs_root:
            for d in Path('C:/').iterdir():
                if 'OSGeo4W64' in str(d):
                    qgs_root = d
        if qgs_root:
            qgs_pyqgis = str(qgs_root/'apps/qgis/python')
    elif sys.platform == 'darwin':
        if Path('/Applications/QGIS.app').exists():
            qgs_root = Path('/Applications/QGIS.app')
            qgs_pyqgis = str(qgs_root/'Contents/Resources/python')

    return str(qgs_root), str(qgs_pyqgis)


def memory_info():
    """Retourne le fichier meminfo dans un dictionnaire"""

    res = None
    if sys.platform == 'linux':
        res = {}
        with open('/proc/meminfo', 'r') as r:
            for row in r.readlines():
                row = row.replace('\n', '')
                k, v = row.split(':')
                k = k.strip()
                v = v.split()
                if len(v) == 1:
                    v = int(v[0])
                elif v[1] == 'kB':
                    v = int(v[0]) * 1024
                elif v[1] == 'mB':
                    v = int(v[0]) * 1024 * 1024
                res[k] = v
    return res


def get_env():
    "Pour initialiser des constantes d'environnement lors de la preparation des donnees"

    QGS_ROOT, PYQGIS_DIR = find_qgis()

    BIG_RAM = False
    TMPFS = False
    TRUE_TMP = False
    if sys.platform == 'linux':
        try:
            mem_info = memory_info()
            if mem_info:
                # avlbl_mem = mem_info['MemAvailable'] / (1024. ** 3)
                total_mem = mem_info['MemTotal'] / (1024. ** 3)
                if total_mem >= 8:
                    BIG_RAM = True
                with open('/etc/fstab', 'r') as r:
                    for l in r.readlines():
                        if re.match(r'tmpfs\s*/tmp\s*tmpfs.*\n', l):
                            TMPFS = True
            if not os.path.exists('/tmp/simurba'):
                os.mkdir('/tmp/simurba')
            TRUE_TMP = True
        except (PermissionError, FileNotFoundError, SystemError):
            raise

    return QGS_ROOT, PYQGIS_DIR, BIG_RAM, TMPFS, TRUE_TMP


def add_cpg(indir, encoding):
    """Scanne tout un dossier et ajoute un fichier d'encodage"""
    global c
    for root, dirs, files in os.walk(str(indir)):
        for name in files:
            base_name, ext = os.path.splitext(name)[0], os.path.splitext(name)[1]
            if ext == '.SHP':
                with Path(root + '/' + base_name + '.CPG').open('w') as w:
                    w.write(encoding + '\n')

            elif ext == '.shp':
                with Path(root + '/' + base_name + '.cpg').open('w') as w:
                    w.write(encoding + '\n')


def insee_to_csv(inseeDataPath):
    """Traitement des données démographiques XLS de l'INSEE"""

    if (inseeDataPath/'csv').exists():
        rmtree(str(inseeDataPath/'csv'))
    os.mkdir(str(inseeDataPath/'csv'))
    for date in ['06', '07', '08', '09']:
        pop = pd.read_excel(str(inseeDataPath/('BTX_IC_POP_20' + date + '.xls')),
                            skiprows=(0, 1, 2, 3, 4))
        pop['P' + date + '_POP'] = pop['P' + date + '_POP'].fillna(0).round().astype(int)
        pop.to_csv(str(inseeDataPath/('csv/pop' + date + '.csv')),
                   index=0, columns=['IRIS', 'P' + date + '_POP'])

    for date in ['10', '12', '14', '15']:
        pop = pd.read_excel(str(inseeDataPath/('base-ic-evol-struct-pop-20' + date + '.xls')),
                            skiprows=(0, 1, 2, 3, 4))
        pop['P' + date + '_POP'] = pop['P' + date + '_POP'].fillna(0).round().astype(int)
        pop.to_csv(str(inseeDataPath/('csv/pop' + date + '.csv')),
                   index=0, columns=['IRIS', 'P' + date + '_POP'])

        log = pd.read_excel(str(inseeDataPath/('base-ic-logement-20' + date + '.xls')),
                            skiprows=(0, 1, 2, 3, 4))
        log['P' + date + '_TXRP'] = log['P' + date + '_RP'] / log['P' + date + '_LOG']
        log.to_csv(str(inseeDataPath/('csv/log' + date + '.csv')),
                   index=0, columns=['IRIS', 'P' + date + '_TXRP'])

    return True


def get_bdt_metadata(xml_file):
    with open(xml_file, 'r') as r:
        lines = r.readlines()

    dates = {}
    exp_old = r'\s*<gml:beginPosition>([0-9\-]*)</gml:beginPosition>\n'
    exp_new = r'\s*<gml:endPosition>([0-9\-]*)</gml:endPosition>\n'
    next = None
    for l in lines:
        if 'Actualité du réseau routier classé' in l:
            next = 'route'
        if next:
            res = re.match(exp_old, l)
            if res:
                old = res.group(1)
                dates[next] = [old]
            else:
                res = re.match(exp_new, l)
                if res:
                    new = res.group(1)
                    dates[next] += [new]
                    next = None

        if 'Actualité du thème Bâti' in l:
            next = 'bati'
        if next:
            res = re.match(exp_old, l)
            if res:
                old = res.group(1)
                dates[next] = [old]
            else:
                res = re.match(exp_new, l)
                if res:
                    new = res.group(1)
                    dates[next] += [new]
                    next = None
                    break

    return dates


def get_exec_time(start: float) -> str:
    """Retourne le temps d'exécution d'une étape format minutes, secondes"""

    exec_time = time() - start
    exec_min = round(exec_time // 60)
    exec_sec = round(exec_time % 60)
    return '%im %is' % (exec_min, exec_sec)


def get_shapefiles(path: str):
    """Recherche les fichiers shp dans un dossier"""
    return [f for f in os.listdir(path) if os.path.splitext(f)[1] == '.shp']


def rasterize(vector: Path,
              output: Path,
              dtype: str,
              proj: str,
              resolution: int,
              extent: tuple = None,
              field: str = None,
              burn: int = None,
              inverse: bool = False,
              touch: bool = False,
              verbose: bool = False):
    """Facilitateur pour gdal_rasterize"""

    res = False
    if isinstance(extent, tuple):
        xMin = extent[0]
        yMin = extent[1]
        xMax = extent[2]
        yMax = extent[3]
    else:  # must be QgsExtent
        xMin = extent.xMinimum()
        yMin = extent.yMinimum()
        xMax = extent.xMaximum()
        yMax = extent.yMaximum()
    opt = gdal.RasterizeOptions(
        format='GTiff',
        outputSRS=proj,
        initValues=0,
        burnValues=burn,
        attribute=field,
        allTouched=touch,
        outputBounds=(xMin, yMin, xMax, yMax),
        inverse=inverse,
        options=['-ot', dtype, '-tr', str(resolution), str(resolution)]
        )
    res = gdal.Rasterize(str(output), str(vector), options=opt)
    if res and verbose:
        print('GDAL Rasterize():     Fichier ' + str(output)
              + ' de type ' + dtype + ' ecrit avec succes.')

    return res


def to_tiff(array: np.ndarray,
            dtype: str,
            proj: str,
            geot: tuple,
            path: Path,
            mask: np.ndarray = None,
            verbose: bool = False):
    """Enregistre un fichier .tif à partir d'un array et de variables GDAL"""

    v = np.nan
    dt = gdal.GDT_Unknown
    cols, rows = array.shape[1], array.shape[0]  # x, y
    driver = gdal.GetDriverByName('GTiff')
    if dtype == 'byte':
        dt = gdal.GDT_Byte
        v = 2 ** 8 - 1
    elif dtype == 'float32':
        dt = gdal.GDT_Float32
        v = (2 - 2 ** -23) * 2 ** 127
    elif dtype == 'uint16':
        dt = gdal.GDT_UInt16
        v = 2 ** 16 - 1
    elif dtype == 'uint32':
        dt = gdal.GDT_UInt32
        v = 2 ** 32 - 1

    if mask is not None:
        array = np.where(mask == 1, v, array)
    ds = driver.Create(str(path), cols, rows, 1, dt)
    ds.SetProjection(proj)
    ds.SetGeoTransform(geot)
    ds.GetRasterBand(1).SetNoDataValue(v)
    ds.GetRasterBand(1).WriteArray(array)
    if verbose:
        print('Fonction to_tiff():   Fichier ' + str(path)
              + ' de type ' + dtype + ' ecrit avec succes.')
    ds = None
    return str(path)


def to_nparray(tif: Path, dtype: type = None) -> np.ndarray:
    """Convertit un tif en numpy array"""

    arr = None
    ds = gdal.Open(str(tif))
    if dtype:
        arr = ds.ReadAsArray().astype(dtype)
    else:
        arr = ds.ReadAsArray()
    ds = None
    return arr


def write_iris_stat(iris_array: np.ndarray, array: np.ndarray,
                    csvpath: Path, jobs: list = ['sum']):
    """Écris des stats par iris à partir de nparray"""

    with open(str(csvpath), 'w') as w:
        w.write('IRIS_ID')
        for j in jobs:
            w.write(',' + j)
        w.write('\n')

        for n in np.unique(iris_array):
            line = str(int(n))
            if n > 0:
                if 'sum' in jobs:
                    value = np.where(iris_array == n, array, 0).sum()
                    line += ', ' + str(value)
                if 'max' in jobs:
                    value = np.where(iris_array == n, array, 0)
                    value = value.amax()
                    line += ', ' + str(value)
                if 'mean' in jobs:
                    value = np.where(iris_array == n, array, np.nan)
                    value = np.nanmean(value)
                    line += ', ' + str(value)
                w.write('\n')
    return True
