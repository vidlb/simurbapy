# -*- coding: utf-8 -*-
"""
/*****************************************************************************
Simulation

    Classe pour la simulation de l'urbanisation

        copyright            : (C) 2018 by Vincent DELBAR

"""
import os
import re
import sys
import csv
import gdal
import random
import numpy as np
import scipy.stats as st
from pathlib import Path
from shutil import rmtree
from collections import OrderedDict
from time import time, strftime, sleep

from .tools import to_tiff, to_nparray, printer, set_parameters
from .fit import DISTRIB_BLACKLIST

np.seterr(divide='ignore', invalid='ignore')
inf, nan = np.inf, np.nan

HOME = Path.home()
try:
    PSQL_USER = os.environ['USER']
except KeyError:
    PSQL_USER = 'postgres'

# {param: [type, default, tip]}
SETTINGS = OrderedDict([
    ("dataDir", [Path, HOME/'SimUrba/zooms/mtp/inputs/30m', "Dossier d'entree"]),
    ("outputDir", [Path, HOME/'SimUrba/outputs', "Dossier où enregistrer les sorties"]),
    ("growth", [float, -1.0, "Taux moyen annuel de croissance démographique"]),
    ("finalYear", [int, 2040, "Pour ajuster la durée de la simulation"]),
    ("scenario", [str, 'tendanciel', "Scenario de consommation d’espace "]),
    ("pluPriority", [bool, True, "Utilisation du PLU pour peupler les ZAU en priorite"]),
    ("buildNonRes", [bool, True, "Pour simuler la construction au sol du bati non residentiels"]),
    ("exclusionRatio", [float, 0.1, "Taux max de couverture initale des cellules à bâtir"]),
    ("maxBuiltRatio", [float, 80.0, "Taux maximal d'artificialisation d’une cellule (%)"]),
    ("maxUsedSrfPla", [float, 200.0, "Limite la surface plancher utilisée pour loger quelqu'un"]),
    ("densifyOld", [bool, True, "Pour loger tout le monde en densifiant l'existant chaque annee"]),
    ("winSize", [int, 5, "Taille en pixels de la fenêtre glissante pour calcul de la contiguite"]),
    ("minContig", [float, 0.1, "Taux minimal de cellules urbanisees contigues"]),
    ("maxContig", [float, 0.7, "Taux maximal de cellules urbanisees contigues"]),
    ("roadRatio", [float, -1, "Rapport routes divisees par bati"]),
    ("fitTo", [str, 'iris', "Niveau de fitting a utiliser (iris, com, zone)"]),
    ("seed", [int, None, "Pour initialiser le générateur de nombres aléatoires de Numpy"]),
    ("ecoWeight", [int, 4, "Poids diminuant l'intérêt a urba en fonction de l'importance eco"]),
    ("roadWeight", [int, 1, "Poids de la proximite aux routes"]),
    ("sirWeight", [int, 3, "Poids en lien avec la présence d'amenites"]),
    ("traWeight", [int, 2, "Poids de la proximité aux arrêts de transports en commun"]),
    ("tiffs", [bool, True, "Indique si les rasters sont sauvés en sortie"]),
    ("snaps", [bool, False, "Pour enregistrer des instantanés chaque annee (et generer des GIF)"]),
    ("verbose", [bool, False, "Pour des messages détaillés"])
])

FIRST_YEAR = 2016
IMPORTANCE_ZAU = 5 / 3
SCENARIOS = ('tendanciel', 'stable', 'reduction', 'inf', 'random')
FIT_LEVELS = ('zone', 'com', 'iris')
CONTIG_CELLS_INDEX = ((-1, 0), (0, -1), (1, 0), (0, 1), (-1, -1), (-1, 1), (1, -1), (1, 1))

RESTRICT_WITH_OCS = True
EXCLUSION_MTP = (120, 221, 222, 230, 240, 311, 312, 411, 412, 421,
                 422, 511, 740, 781, 782, 811, 812, 821, 822, 830)
EXCLUSION_NIM = (121, 331, 332, 411, 421, 511, 512, 521, 530)
EXCLUSION_CLC = (122, 123, 124, 131, 132, 141, 142, 335, 411,
                 412, 421, 422, 423, 511, 512, 521, 522, 523)
EXCLUSION_OSO = (46, 51, 53)


class Simulation:

    def __init__(self, interface='cli', param_str=None, debug=False):

        if interface == 'cli':
            print('\n\n********** SimUrba - Simulation **********\n\n__init__():\n')
        self._timestamp = strftime('%Y%m%d%H%M%S')
        self._interface = interface
        self._param_str = param_str
        self._debug = debug
        self._project_initialized = False
        self._workspace_initialized = False
        self._completed = False
        self._results_computed = False
        # Attribution des valeurs par défaut et vérification des paramètres utilisateur
        self._settings = set_parameters(SETTINGS, interface, param_str)
        self._check_settings()
        self.dataDir = Path(self._settings['dataDir'])
        self.outputDir = Path(self._settings['outputDir'])
        self.growth = self._settings['growth']
        self.finalYear = self._settings['finalYear']
        self.scenario = self._settings['scenario']
        self.pluPriority = self._settings['pluPriority']
        self.buildNonRes = self._settings['buildNonRes']
        self.exclusionRatio = self._settings['exclusionRatio']
        self.maxBuiltRatio = self._settings['maxBuiltRatio']
        self.maxUsedSrfPla = self._settings['maxUsedSrfPla']
        self.densifyOld = self._settings['densifyOld']
        self.winSize = self._settings['winSize']
        self.minContig = self._settings['minContig']
        self.maxContig = self._settings['maxContig']
        self.roadRatio = self._settings['roadRatio']
        self.fitTo = self._settings['fitTo']
        self.seed = self._settings['seed']
        self.ecoWeight = self._settings['ecoWeight']
        self.sirWeight = self._settings['sirWeight']
        self.roadWeight = self._settings['roadWeight']
        self.traWeight = self._settings['traWeight']
        self.tiffs = self._settings['tiffs']
        self.snaps = self._settings['snaps']
        self.verbose = self._settings['verbose']

        prep_set = []
        prep_file = self.dataDir/'settings.csv'
        if not prep_file.exists():
            prep_file = self.dataDir/'parametres_preparation.txt'
        with prep_file.open('r') as r:
            for l in r.readlines():
                prep_set.append(tuple(l[:-1].split(', ')))
        self._preparation_settings = dict(prep_set)
        self.startYear = FIRST_YEAR
        self.totalYears = 1 + self.finalYear - self.startYear
        name = self._preparation_settings['localData']
        self.studyAreaName = os.path.basename(name)
        self.levelHeight = int(self._preparation_settings['levelHeight'])

        # Creation des variables GDAL, indispensables pour la fonction to_tiff ()
        ds = gdal.Open(str(self.dataDir/'iris_id.tif'))
        self.irisId = ds.GetRasterBand(1).ReadAsArray().astype(np.uint16)
        self.rows, self.cols = self.irisId.shape[0], self.irisId.shape[1]  # y, x
        size = self.cols * self.rows
        if size >= 100000:
            self._n_choices = int(size / 10000)
        elif size >= 10000:
            self._n_choices = int(size / 1000)
        else:
            self._n_choices = int(size / 100)
        self.proj = ds.GetProjection()
        self.geot = ds.GetGeoTransform()
        self.pixRes = int(self.geot[1])
        self.srfCell = self.pixRes * self.pixRes
        self.nbIris = int(self.irisId.max())
        ds = None
        self.comId = to_nparray(self.dataDir/'com_id.tif', np.uint16)
        self.logs = []
        self.measures = []
        self.consOCS = {}

        # Les donnnees sur l'evolution demographique
        self._read_demography()
        # Statistiques sur l'evolution du bati
        self._compute_artif_stat()
        # Dictionnaire pour nombre de m2 ouverts a l'urbanisation par annee, selon le scenario
        self.dicSrf = self._compute_thresholds()
        # Les coefficients d'interet a urbaniser donnes par l'utilisateur
        self._compute_coeffs()
        # Les paramètres pour ajuster le RNG
        self._fitting_dict = {'zone': {'area': {}, 'height': {}},
                              'com': {'area': {}, 'height': {}},
                              'iris': {'area': {}, 'height': {}}}
        self._read_distributions()

        # Declaration des matrices
        self.heatMap = np.zeros([self.rows, self.cols], np.uint16)
        # self.srfRoutesInit = to_nparray(self.dataDir/'surface_routes_parkings.tif', np.uint16)
        self.artifInit = to_nparray(self.dataDir/'artificialisation.tif', np.uint16)
        self.demographieInit = to_nparray(self.dataDir/'demographie.tif', np.uint16)
        self.srfBatiSolInit = to_nparray(self.dataDir/'surface_bati.tif', np.uint16)
        self.srfBatiResSolInit = to_nparray(self.dataDir/'surface_bati_res_sol.tif', np.uint16)
        self.m2PlaHab = to_nparray(self.dataDir/'iris_m2_hab.tif', np.uint16)
        self.srfBatiPlaInit = to_nparray(self.dataDir/'surface_bati_res_pla.tif', np.uint16)
        self.ocs = to_nparray(self.dataDir/'classes_ocsol.tif', np.float32)
        self.mask = to_nparray(self.dataDir/'masque.tif', np.byte)
        if self.buildNonRes:
            self.txSsr = to_nparray(self.dataDir/'iris_tx_bati_res.tif', np.float32)

        # Preparation des restrictions et gestion du PLU
        self.skipZau = True
        self.skipZauYear = None
        self.skipPluRestYear = None
        self.pluRest = None
        self.restriction = to_nparray(self.dataDir/'interet/restriction_totale.tif')
        self.restriction = np.where(self.mask == 1, 1, self.restriction)
        if (self.dataDir/'interet/plu_restriction.tif').exists():
            self.pluRest = to_nparray(self.dataDir/'interet/plu_restriction.tif')
            self.restrictionNonPlu = self.restriction.copy()
            self.restriction = np.where(self.pluRest == 1, 1, self.restriction)
        if (self.dataDir / 'interet/plu_priorite.tif').exists() and self.pluPriority:
            self.skipZau = False
            self.pluPrio = to_nparray(self.dataDir/'interet/plu_priorite.tif')
        else:
            self.pluPriority = False

        # Exclusion supplementaire a partir de l'OCS
        if RESTRICT_WITH_OCS:
            exclusion_ocs = []
            if self.studyAreaName in ['montpellier', 'mtp']:
                exclusion_ocs = EXCLUSION_MTP
            elif self.studyAreaName in ['nimes', 'nim']:
                exclusion_ocs = EXCLUSION_NIM
            elif self.studyAreaName in ['gpsl', 'ccgpsl']:
                exclusion_ocs = EXCLUSION_CLC
            elif self.studyAreaName in ['tls', 'pyr', 'prp', 'fig']:
                exclusion_ocs = EXCLUSION_OSO
            for c in exclusion_ocs:
                self.restriction = np.where(self.ocs == c, 1, self.restriction)

        # Amenites entre 0 et 1
        non_eco = to_nparray(self.dataDir/'interet/non-importance_ecologique.tif', np.float32)
        rou = to_nparray(self.dataDir/'interet/proximite_routes.tif', np.float32)
        tra = to_nparray(self.dataDir/'interet/proximite_transport.tif', np.float32)
        sir = to_nparray(self.dataDir/'interet/densite_sirene.tif', np.float32)
        # Creation du raster final d'interêt avec ponderation (Experience : init à 1)
        coeffs = self.coeffInteret
        self.interetComplet = ((non_eco * coeffs['ecologie']) + (rou * coeffs['routes'])
                               + (tra * coeffs['transports']) + (sir * coeffs['sirene']))
        # Normalisé enre 0 et 1
        self.interetComplet = (self.interetComplet / self.interetComplet.max())
        self.interet = np.where(self.restriction != 1, self.interetComplet, 0)
        # Creation des rasters de capacite en surfaces sol et plancher
        self.capaSol = np.zeros([self.rows, self.cols], np.uint16) + \
                               (self.srfCell * self.maxBuiltRatio / 100)
        self.capaSol = np.where((self.restriction != 1) & (self.artifInit < self.capaSol),
                                self.capaSol - self.artifInit, 0).astype(np.uint16)
        self.totalCapacity = int(np.where(self.capaSol > 0, 1, 0).sum())
        # Cellules urbanisees (tout bati inclu)
        self.cellBatiesInit = np.where(self.srfBatiSolInit > 0, 1, 0).astype(np.byte)
        self.txArtifInit = (self.artifInit / self.srfCell).astype(np.float32)
        # Nombre moyen d'etages dans la cellule
        self.ratioPlaSolInit = np.where(self.srfBatiSolInit != 0,
                                        self.srfBatiPlaInit / self.srfBatiSolInit, 0)
        # On applique un seuil ainsi qu'un nombre de m² plancher moyen dans les IRIS sans donnee
        self.m2PlaHab = np.where(self.m2PlaHab > self.maxUsedSrfPla,
                                 self.maxUsedSrfPla, self.m2PlaHab)
        mean_m2_pla = np.nanmean(np.where(self.m2PlaHab == 0, np.nan, self.m2PlaHab))
        self.m2PlaHab = np.where((self.m2PlaHab == 0) & (self.irisId != 0),
                                 mean_m2_pla, self.m2PlaHab)

        # Copie des rasters de depart pour variables d'instances de la simulation
        self.non_eco = non_eco.copy()
        self.cellBaties = self.cellBatiesInit.copy()
        self.srfBatiSol = self.srfBatiSolInit.copy()
        self.srfBatiPla = self.srfBatiPlaInit.copy()
        self.srfBatiResSol = self.srfBatiResSolInit.copy()
        self.demographie = self.demographieInit.copy()
        # self.srfRoutes = self.srfRoutesInit.copy()
        self.artif = self.artifInit.copy()
        self.txArtif = self.txArtifInit.copy()

        # Chemins
        if interface == 'openmole':
            self.project = self.outputDir
        else:
            self.projectStr = '{}_{}_{}_{}'.format(self._timestamp, self.studyAreaName,
                                                   self.scenario, self.growth)
            self.project = self.outputDir/self.projectStr

        if interface == 'cli':
            for p in self._settings.keys():
                print('    ' + p + ' = ' + str(self.__getattribute__(p)))
            print('\n    area = ' + self.studyAreaName)
            print('    outdir = ' + self.projectStr)
            print('    nb_cells = ' + str(size))

        self._info('\n\nDictionnaire annualisé du scénario :\n' + str(self.dicSrf), 'verbose')

    def init_project(self):
        """Pour créer le dossier de sortie et écrire le premiers fichiers calculés pendant init"""

        if self._interface == 'openmole':
            if not self.project.exists():
                os.makedirs(str(self.project))
                with (self.project/'settings.csv').open('w') as w:
                    for k, v in self._settings.items():
                        w.write(k + ', ' + str(getattr(self, k)) + '\n')

        elif self._interface not in ['psql', 'void']:
            if self.project.exists():
                rmtree(str(self.project))
            os.makedirs(str(self.project))
            if self.tiffs and self.snaps:
                mkdirList = ['snapshots',
                             'snapshots/artificialisation',
                             'snapshots/surface_plancher']
                for d in mkdirList:
                    os.mkdir(str(self.project/d))
            # Ecriture de certaines des donnees de depart
            with (self.project/'settings.csv').open('w') as w:
                for k, v in self._settings.items():
                    w.write(k + ', ' + str(getattr(self, k)) + '\n')

            coeff_file = self.project/'coefficients_interet.csv'
            with coeff_file.open('w') as w:
                for key in self.coeffInteret.keys():
                    w.write(key + ', ' + str(self.coeffInteret[key]) + '\n')

        srf = str(int(round(self.m2SolHab15)))
        if 'inf' in self.scenario:
            seuil = 'inf'
        else:
            try:
                seuil = str(round(self.srfMax))
            except (ValueError, TypeError):
                seuil = 'None'

        self.logs += ['"Population a loger,"' + str(self.sumPopALoger),
                      '"Surface artif utilisee par habitant en 2015",' + srf,
                      '"Seuil en surface artificialisee",' + seuil]
        # Instantanés de la situation a t0
        if self.tiffs:
            to_tiff(self.capaSol, 'uint16', self.proj, self.geot,
                    self.project/'capacite_sol.tif', self.mask, self._debug)
            to_tiff(self.interet, 'float32', self.proj, self.geot,
                    self.project/'interet.tif', self.mask, self._debug)
            to_tiff(self.interetComplet, 'float32', self.proj, self.geot,
                    self.project/'interet_complet.tif', self.mask, self._debug)
        self._project_initialized = True

        return True

    def run(self, write_results=True, write_sql=False, sender=None):
        """Fonction principale pour lancer une simulation"""

        if not self._project_initialized:
            self.init_project()
        if self.seed is not None and self.seed >= 0:
            np.random.seed(self.seed)
        self.timings = {}
        self.beginTime = time()
        self._results_computed = False
        # Boucle principale pour iteration annuelle
        preLog = 0
        preBuilt = 0
        for year in range(self.startYear, self.finalYear + 1):
            begin = time()
            self.currentYear = year
            self._info('\n', 'verbose')
            progres = "Annee %i/%i" % (year, self.finalYear)
            if self._interface == 'cli':
                printer(progres)
            else:
                self._info(progres)
            self._info('\n', 'verbose')
            srfMax = self.dicSrf[year]
            popALoger = self.popDic[year]
            info = '\nObjectif demographique: ' + str(popALoger) \
                   + '\nSeuil surface artif: ' + str(srfMax)
            self._info(info, level='verbose')
            if self._debug:
                sleep(1)

            restePop, resteSrf = self._urbanisation(popALoger - preLog, srfMax - preBuilt,
                                                    use_zau=not self.skipZau)
            preBuilt = -resteSrf
            preLog = -restePop
            self._info('Population restante : ' + str(restePop), 'verbose')
            self._info('Surface a construire restante : ' + str(resteSrf), 'verbose')
            self.timings[year] = time() - begin
            # TODO : better snapshots
            if self.tiffs and self.snaps:
                to_tiff(self.srfBatiPla, 'uint16', self.proj, self.geot,
                        self.project/('snapshots/surface_plancher/srf_pla_' + str(year) + '.tif'),
                        self.mask, self._debug)
                to_tiff(self.txArtif, 'uint16', self.proj, self.geot,
                        self.project/('snapshots/artificialisation/artif_' + str(year) + '.tif'),
                        self.mask, self._debug)

        if resteSrf == inf:
            self.resteSrf = resteSrf
        elif resteSrf > 0:
            self.resteSrf = int(round(resteSrf))
        else:
            self.resteSrf = 0
        self.restePop = int(restePop) if restePop > 0 else 0
        self.endTime = time()
        self.execTime = round(self.endTime - self.beginTime, 2)
        self._info('\nDuration of the simulation: ' + str(self.execTime) + ' seconds')

        self._completed = self.compute_results()
        if write_sql:
            head, tup = self._to_sql()
            command = 'psql -d simurba -U {} -c "{}{}" > /dev/null'.format(PSQL_USER, head, tup)
            res = os.system(command)
            if self._interface == 'psql':
                if res == 0:
                    sys.stdout.write(str(tup) + '\n')
                else:
                    sys.stdout.write('psql error with tuple : ' + tup + '\n')
                sys.stdout.flush()
        if write_results and self._interface != 'void':
            self._completed = self.compute_results(self.project)
        if sender is not None:
            try:
                sender.send(self._completed)
            except (NameError, AttributeError):
                raise

        return self._completed

    def compute_results(self, dest=None):
        """Calcul et/ou export des resultats dans le repertoire du projet par defaut"""

        if not self._results_computed:
            self.urbanisationFinale = np.where(self.artif > 0, 1, 0)
            self.expansion = np.where((self.cellBatiesInit == 0) & (self.cellBaties == 1), 1, 0)
            self.artifNouv = self.artif - self.artifInit
            self.artifNouvSum = self.artifNouv.sum()
            self.srfBatiSolNouv = self.srfBatiSol - self.srfBatiSolInit
            self.srfBatiPlaNouv = self.srfBatiPla - self.srfBatiPlaInit
            self.popNouv = self.demographie - self.demographieInit
            self.ratioPlaSol = np.where(self.srfBatiSol != 0, self.srfBatiPla
                                        / self.srfBatiSol, 0).astype(np.float32)
            self.dsfBatiSol = np.where((self.srfBatiSolNouv > 0) & (self.artifInit > 0)
                                       & (self.txArtifInit <= self.exclusionRatio)
                                       & (self.expansion != 1), 1, 0)
            # self.dsfBatiSolSum = self.dsfBatiSol.sum()
            self.srfDsfPla = np.where((self.srfBatiPlaInit > 0) & (self.srfBatiPlaNouv > 0)
                                      & (self.dsfBatiSol != 1) & (self.expansion != 1),
                                      self.srfBatiPlaNouv, 0)
            self.dsfBatiPla = np.where(self.srfDsfPla > 0, 1, 0)
            self.srfBatiSolNouvSum = self.srfBatiSolNouv.sum()
            self.dsfBatiPlaSum = self.dsfBatiPla.sum()
            peupMean = np.nanmean(np.where(self.popNouv == 0, np.nan, self.popNouv))
            self.peuplementMoyen = round(peupMean, 4)
            self.txArtifNouv = np.where((self.expansion == 1) | (self.dsfBatiSol == 1),
                                        self.txArtif, np.nan)
            self.txArtifMoyen = round(np.nanmean(self.txArtifNouv) * 100, 4)
            self.choicesSum = self.heatMap.sum()
            self.popNouvSum = self.popNouv.sum()
            self.expansionSum = self.expansion.sum()
            self.srfBatiPlaNouvSum = self.srfBatiPlaNouv.sum()
            self.builtCellsRatio = self.expansionSum / self.totalCapacity
            eco = 1 - self.non_eco
            self.impactEnv = self.srfBatiSolNouv * eco
            self.cumImpact = (self.srfBatiSolNouv * eco).sum() * self.builtCellsRatio
            if self.roadRatio > 0:
                self.m2SolHabFinal = round(self.artif.sum() / self.demographie.sum(), 4)
            else:
                self.m2SolHabFinal = round(self.srfBatiSol.sum() / self.demographie.sum(), 4)

            # Calcul de la consommation de surface par classe d'ocsol
            for c in np.unique(self.ocs):
                if int(c) != 0:
                    self.consOCS[str(int(c))] = ((self.ocs == c) * self.artifNouv).sum()

            self.logs += [
                '"Population logee",' + str(self.popNouvSum),
                '"Demographie finale",' + str(self.demographie.sum()),
                '"Nombre de cellules choisies",' + str(self.choicesSum),
                '"Temps d\'execution",' + str(self.execTime),
                '"Annee de suppression des restrictions PLU",' + str(self.skipPluRestYear)
            ]
            self.measures += [
                '"Population non logee",' + str(self.restePop),
                '"Surface non construite",' + str(self.resteSrf),
                '"Peuplement moyen",' + str(self.peuplementMoyen),
                '"Surface artificialisee",' + str(self.artifNouvSum),
                '"Surface au sol batie",' + str(self.srfBatiSolNouvSum),
                '"Surface plancher batie",' + str(self.srfBatiPlaNouvSum),
                '"Cellules ouvertes a l\'urbanisation",' + str(self.expansionSum),
                '"Impact environnemental cumule",' + str(round(self.cumImpact, 4)),
                '"Taux moyen d\'artificialisation",' + str(self.txArtifMoyen),
                # '"Nombre de cellules densifees au sol",' + str(self.dsfBatiSolSum),
                '"Nombre de cellules densifees verticalement",' + str(self.dsfBatiPlaSum),
                '"Nombre final de m² par habitants",' + str(self.m2SolHabFinal),
                '"Annee de saturation des ZAU",' + str(self.skipZauYear)
            ]
            self._results_computed = True

        if dest is None:
            return self._results_computed

        if isinstance(dest, str):
            dest = Path(dest)
        if not dest.exists():
            os.makedirs(str(dest))

        if self._interface == 'cli':
            print('\nEcriture des resultats...')
        if self.tiffs:
            exports = [
                (self.heatMap, 'byte', 'choices_heatmap'),
                (self.artif, 'uint16', 'artificialisation_' + str(self.finalYear)),
                (self.artifNouv, 'uint16', 'artificialisation_nouvelle.tif'),
                (self.urbanisationFinale, 'uint16', 'urbanisation_' + str(self.finalYear)),
                (self.srfBatiSol, 'uint16', 'surface_bati_sol_' + str(self.finalYear)),
                (self.srfBatiPla, 'uint16', 'surface_plancher_' + str(self.finalYear)),
                (self.srfDsfPla, 'uint16', 'surface_densif_plancher'),
                (self.demographie, 'uint16', 'demographie_' + str(self.finalYear)),
                (self.ratioPlaSol, 'float32', 'ratio_plancher_sol_' + str(self.finalYear)),
                (self.txArtif, 'float32', 'taux_artif_' + str(self.finalYear)),
                (self.impactEnv, 'float32', 'impact_environnemental'),
                (self.expansion, 'byte', 'expansion'),
                (self.srfBatiPlaNouv, 'uint16', 'surface_plancher_construite'),
                (self.popNouv, 'uint16', 'population_nouvelle')]
            if self.dsfBatiPlaSum != 'nan':
                exports.append((self.dsfBatiPla, 'byte', 'densification_plancher'))
            # TODO : différents dtypes de densification
            # if self.dsfBatiSolSum != 'nan':
                # exports.append((self.dsfBatiSol, 'byte', 'densification_sol'))

            for e in exports:
                to_tiff(e[0], e[1], self.proj, self.geot,
                        dest/(e[2] + '.tif'), self.mask, self._debug)

        self._write(self.measures, dest/'measures.csv')
        self._write(self.logs, dest/'logs.csv')
        if self._interface not in ['void', 'psql']:
            timing_logs = [str(y) + ',' + str(t) for y, t in self.timings.items()]
            self._write(timing_logs, dest/'timing_logs.csv')
            if self._interface != 'openmole':
                with (dest/'conso_ocs.csv').open('w') as w:
                    w.write('classe, surface\n')
                    for key, value in self.consOCS.items():
                        w.write(key + ', ' + str(value) + '\n')

        return self._results_computed

    def _print(self, text):
        if self._interface in ['cli']:
            print(text)
        elif self._interface in ['qgis', 'gui']:
            self.logger.logMessage(text)

    def _info(self, text, level='normal'):
        if level == 'error':
            self._print('Error: ' + text)
        elif level == 'normal' or (level == 'verbose' and self.verbose) or self._debug:
            self._print(text)

    def _check_settings(self):

        if self._interface not in ['batch', 'cli', 'gui', 'openmole', 'psql', 'void']:
            sys.exit("SimUrba: Bad interface ({})".format(self._interface))
        if self._interface in ['psql', 'void']:
            self._settings['tiffs'] = False
            self._settings['snaps'] = False

        scenario = self._settings['scenario']
        if scenario not in SCENARIOS:
            try:
                i = float(scenario)
                # Index de scenario (openmole ou random() )
                if round(i) < len(SCENARIOS):
                    i = round(i)
                    scenario = SCENARIOS[i]
                    self._settings['scenario'] = scenario
            except (ValueError, TypeError):
                pass

        if scenario in ['aleatoire', 'rand', 'random']:
            new_settings = random_scenario(self._settings['dataDir'])
            for param, value in new_settings.items():
                self._settings[param] = value
            scenario = self._settings['scenario']

        if round(self._settings['winSize']) not in [3, 5, 7, 9]:
            # raise Exception('Bad contig win size')
            if self._settings['winSize'] < 4:
                winSize = 3
            elif 4 <= self._settings['winSize'] < 6:
                winSize = 5
            elif 6 <= self._settings['winSize'] < 8:
                winSize = 7
            elif self._settings['winSize'] >= 8:
                winSize = 9
            self._settings['winSize'] = winSize

        fitTo = self._settings['fitTo']
        if fitTo not in FIT_LEVELS:
            try:
                i = round(float(fitTo))
                self._settings['fitTo'] = FIT_LEVELS[i]
            except (ValueError, TypeError):
                raise Exception('Bad fitting level')

        fitting_dir = self._settings['dataDir']/('fitting/' + self._settings['fitTo'])
        if not fitting_dir.exists():
            raise FileNotFoundError(str(fitting_dir))

    def _read_demography(self):
        """Création des dictionnaires contenant la population par annee"""

        with (self.dataDir/'population.csv').open('r') as csvFile:
            reader = csv.reader(csvFile)
            next(reader, None)
            self.histPop = {rows[0]: rows[1] for rows in reader}
        self.pop09 = int(self.histPop['2009'])
        self.pop15 = int(self.histPop['2015'])
        self.evoPop = (self.pop15 - self.pop09) / self.pop09 / 6
        if self.growth < 0:
            growth = round(self.evoPop * 100, 4)
            self.growth = growth
        self.popDic = {}
        year = self.startYear
        pop = self.pop15
        while year <= self.finalYear:
            self.popDic[year] = round(pop * (self.growth / 100))
            pop += round(pop * (self.growth / 100))
            year += 1
        self.sumPopALoger = sum(self.popDic.values())

    def _compute_artif_stat(self):

        with (self.dataDir/'evolution_artificialisation.csv').open('r') as r:
            reader = csv.reader(r)
            next(reader, None)
            self.dicSsol = {rows[0]: [int(rows[1]), int(rows[2])] for rows in reader}

        builds09 = self.dicSsol['2009'][0]
        roads09 = self.dicSsol['2009'][1]
        builds15 = self.dicSsol['2016'][0]
        roads15 = self.dicSsol['2016'][1]

        if self.roadRatio < 0:
            self.roadRatio = round(roads15 / builds15, 4)

        if self.roadRatio == 0:
            self.m2SolHab09 = builds09 / self.pop09
            self.m2SolHab15 = builds15 / self.pop15
        else:
            self.m2SolHab09 = (builds09 + roads09) / self.pop09
            self.m2SolHab15 = (builds15 + roads15) / self.pop15
        self.m2SolHabEvo = (self.m2SolHab15 - self.m2SolHab09) / self.m2SolHab09 / 6

    def _compute_thresholds(self):
        """Calcul des valeurs de seuils a partir du scenario de simulation"""

        d = {}
        year = self.startYear
        if self.scenario == 'tendanciel':
            self.srfMax = self.m2SolHab15
            while year <= self.finalYear:
                self.srfMax += self.srfMax * self.m2SolHabEvo
                d[year] = int(round(self.srfMax) * self.popDic[year])
                year += 1
        elif self.scenario == 'stable':
            self.srfMax = self.m2SolHab15
            while year <= self.finalYear:
                d[year] = int(round(self.srfMax) * self.popDic[year])
                year += 1
        elif self.scenario == 'reduction':
            self.srfMax = self.m2SolHab15
            while year <= self.finalYear:
                d[year] = int(round(self.srfMax) * self.popDic[year])
                self.srfMax -= self.m2SolHab15 * (0.75 / self.totalYears)
                year += 1
        elif self.scenario[-3:] == 'inf':
            self.srfMax = np.inf
            mean_year = self.srfMax / (self.totalYears)
            while year <= self.finalYear:
                d[year] = mean_year
                year += 1
        elif self.scenario.isnumeric():
            self.srfMax = round(float(self.scenario))
            mean_year = self.srfMax / (self.totalYears)
            while year <= self.finalYear:
                d[year] = mean_year
                year += 1

        return d

    def _compute_coeffs(self):
        """Calcul des coefficients de pondération de chaque raster d'interêt"""

        if (self.dataDir/'interet/poids.csv').exists() and self._interface != 'openmole':
            with (self.dataDir/'interet/poids.csv').open('r') as r:
                reader = csv.reader(r)
                next(reader, None)
                self.poidsInteret = {rows[0]: int(rows[1]) for rows in reader}
            self.coeffInteret = {}
            for key in self.poidsInteret.keys():
                self.coeffInteret[key] = self.poidsInteret[key] / sum(self.poidsInteret.values())
        else:
            self.poidsInteret = {'ecologie': self.ecoWeight, 'sirene': self.sirWeight,
                                 'routes': self.roadWeight, 'transports': self.traWeight}
            sum_weights = sum(self.poidsInteret.values())
            self.coeffInteret = {k: v / sum_weights for k, v in self.poidsInteret.items()}

    def _read_distributions(self):
        """Ici on génère à l'avance les distributions pour le tirages des surfaces et hauteurs"""

        # Impossible d'eviter eval() ici a cause des inf et nan (erreur avec literal_eval)
        for level in ['zone', 'iris', 'com']:
            if level == self.fitTo or level == 'zone':
                exp_file = r'(.*)_(.*)_fittings.csv'
                directory = self.dataDir/('fitting/' + level)
                if directory.exists():
                    for file in os.listdir(str(directory)):
                        res = re.match(exp_file, file)
                        if res:
                            id = int(res.group(1))
                            nature = res.group(2)
                            with (self.dataDir/('fitting/' + level + '/' + file)).open('r') as r:
                                tup = eval(r.readline())
                                dist = tup[0]
                                # Si la première distribution est interdite on prend la prochaine
                                while (dist not in dir(st)) or (dist in DISTRIB_BLACKLIST):
                                    tup = eval(r.readline())
                                    dist = tup[0]
                                params = tup[1][0]
                                distrib = getattr(st, dist)
                                self._fitting_dict[level][nature][id] = distrib(*params)
                elif level == 'zone':
                    raise FileNotFoundError(str(self.dataDir/'fitting/zone'))

    def _write(self, lines, path):
        """Pour ecrire une ligne ou un tableau de lignes dans un fichier"""

        if os.path.basename(str(path)) == 'measures.csv' and self._interface == 'openmole':
            if isinstance(lines, list):
                with path.open('w') as w:
                    i = 1
                    for s in lines:
                        val = s.split(',')[1]
                        if val == 'inf':
                            val = 'Infinity'
                        if val == 'nan':
                            val = 'NaN'
                        w.write(val)
                        if i < len(lines):
                            w.write(',')
                        else:
                            w.write('\n')
                        i += 1
        else:
            with path.open('a') as w:
                if isinstance(lines, list):
                    for line in lines:
                        w.write(line + '\n')
                elif isinstance(lines, str):
                    w.write(lines + '\n')

    def _to_sql(self, table='sampling') -> str:
        """Pour ecrire une ligne de donnees a inserer dans une base psql"""

        if not self._completed:
            return ''
        if not self._results_computed:
            self.compute_results()
        params = (self.pixRes, self.growth, self.finalYear, self.scenario, self.pluPriority,
                  self.buildNonRes, self.exclusionRatio, self.maxBuiltRatio, self.maxUsedSrfPla,
                  self.densifyOld, self.winSize, self.minContig, self.maxContig, self.roadRatio,
                  self.fitTo, self.ecoWeight, self.roadWeight, self.sirWeight, self.traWeight)

        logs = (self.m2SolHab09, self.m2SolHab15, self.popNouvSum, self.skipZauYear,
                self.choicesSum, self.execTime)

        results = (self.restePop, self.resteSrf, self.peuplementMoyen, self.artifNouvSum,
                   self.srfBatiSolNouvSum, self.srfBatiPlaNouvSum, self.expansionSum,
                   self.cumImpact, self.txArtifMoyen, self.dsfBatiPlaSum, self.m2SolHabFinal)

        output = (self._timestamp,) + params + logs + results

        head = """
            INSERT INTO sampling (timestamp, pix_res, growth, final_year, scenario, plu_prio,
            build_non_res, exclusion_ratio, max_built_ratio, max_used_srf_pla, densify_old,
            win_size, min_contig, max_contig, road_ratio, fit_to,
            eco_weight, road_weight, sir_weight, tra_weight,
            m2sol_hab09, m2sol_hab15, pop_nouv, skip_zau_year, choices_sum, exec_time,
            reste_pop, reste_srf, peupl_moy, artif_nouv, bati_sol, bati_pla,
            cell_ouv, impact_env, tx_artif_moy, cell_dsf, m2sol_hab_fin) VALUES """

        i = 0
        tup = '('
        for val in output:
            if isinstance(val, str):
                tup += "'" + val + "'"
            elif isinstance(val, bool):
                tup += 'true' if val is True else 'false'
            elif val is None or val == 'None':
                tup += 'null'
            elif isinstance(val, float):
                tup += str(round(val, 4))
            else:
                tup += str(val)

            if i < len(output) - 1:
                tup += ','
            elif i == len(output) - 1:
                tup += ')'
            i += 1

        tup = tup.replace(',inf,', ',2147483647,')

        return head, tup

    @staticmethod
    def _choose_cells(weight: np.ndarray, size: int, replace: bool = False) -> list:
        """Tirage pondéré qui retourne une liste de cellules / tuples(row, col)"""

        cells = []
        flat_weight = weight.flatten()
        p = flat_weight / flat_weight.sum()
        count = np.where(p > 0, 1, 0).sum()
        if count <= 0:
            return None
        # Si la taille (size) exigée est < au nombre de valeurs non nulles
        # on tire le nombre maximal de cellules possibles (count)
        if count < size:
            size = count
        choices = np.random.choice(flat_weight.size, size, replace, p)
        # Inversion de la liste pour avoir les meilleurs en haut de la pile et utiliser pop()
        i = 1
        while i <= size:
            row = choices[-i] // weight.shape[1]
            col = choices[-i] % weight.shape[1]
            if row > 0 and col > 0:
                cells.append((row, col))
            i += 1

        return cells

    @staticmethod
    def _win_stat(calc: str, array: np.ndarray, idx: tuple, ndim: tuple, size: int = 5) -> float:
        """Fenêtre glissante pour statistique dans le voisinage d'un pixel"""

        (r, c) = idx
        (rows, cols) = ndim
        if (r >= size // 2 and r + size // 2 < rows) and (c >= size // 2 and c + size // 2 < cols):
            v = 0
            pos = [i + 1 for i in range(-size // 2, size // 2)]
            for a in pos:
                for b in pos:
                    v += array[r + a][c + b]
            if calc == 'mean':
                return v / (size * size)
            if calc == 'sum':
                return v

        return -1

    def _choose_building_dim(self, target: str, row: int, col: int) -> float:
        """Tirage aléatoire qui retourne une surface ou une hauteur de batiment (par com/iris)"""

        val = 0
        id = None
        if self.fitTo == 'zone':
            id = 1
        elif self.fitTo == 'com':
            id = self.comId[row, col]
        elif self.fitTo == 'iris':
            id = self.irisId[row, col]

        level = self.fitTo
        distrib = self._fitting_dict[level][target][id]
        val = distrib.rvs(1)[0]
        if target == 'height':
            val = round(val / self.levelHeight)

        return val

    def _cell_artif(self, row: int, col: int, srf: float, nb_niv: int) -> tuple:
        """Artificialisation d'une surface cellule vide ou deja urbanisee"""

        build_ratio = 1 / (1 + self.roadRatio)
        build = srf * build_ratio
        road = srf - build
        if self.buildNonRes:
            # On reduit la surface a une part de residentiel avant de calculer le pancher
            ssr = build * self.txSsr[row, col] if self.txSsr[row, col] > 0 else build
        else:
            ssr = build

        contig = self._win_stat('mean', self.cellBaties, (row, col),
                                (self.rows, self.cols), self.winSize)

        if not self.minContig <= contig < self.maxContig:
            self._info('! bad contig: ' + str(contig), level='debug')
            return ()

        sp = ssr * nb_niv
        conso = self.m2PlaHab[row, col]
        if not 0 < sp >= conso:
            info = '! built < area required for someone : ' + str(sp) + ' < ' + str(conso)
            self._info(info, level='debug')
            return ()

        housed = int(round(sp / conso))

        return (row, col, road, build, sp, housed)

    def _expansion(self, row: int, col: int) -> list:
        """Artificialisation et construction de batiments et routes (une cellule ou plus)"""

        # On vérifie que c'est bien constructible comme ce n'est plus assuré
        # avec les cellules tirées en cache (cells[]) dans self.tmpInteret qui a été modifié
        if self.tmpInteret[row, col] == 0:
            return []
        if self.cellBaties[row, col] == 1:
            self.tmpInteret[row, col] = 0
            return []
        cell_capa = self.capaSol[row, col]
        if cell_capa <= 0:
            self._info('! cell_capa = ' + str(cell_capa), level='debug')
            self.tmpInteret[row, col] = 0
            return []

        target_build = self._choose_building_dim('area', row, col)
        target_area = (target_build * self.roadRatio) + target_build
        win_capacity = self._win_stat('sum', self.capaSol, (row, col), (self.rows, self.cols), 3)
        self._info('\n? target_area = ' + str(target_area)
                   + ', win_capacity = ' + str(win_capacity), level='debug')
        if win_capacity < 0:
            self._info('! out of bounds: ' + str((row, col)), level='debug')
            self.tmpInteret[row, col] = 0
            return []

        if target_area > win_capacity:
            self._info('! not enough available space ', level='debug')
            return []

        nb_niv = self._choose_building_dim('height', row, col)
        nb_niv = 1 if nb_niv < 1 else nb_niv
        srf = cell_capa if cell_capa <= target_area else target_area
        building = []
        artif_area = 0
        a = self._cell_artif(row, col, srf, nb_niv)
        if a:
            self.cellBaties[row, col] = 1
            building.append(a)
            artif_area += srf
            i = 0
            # Ici on etale dans les cellules voisines pour le cas d'un grand batiment
            while artif_area < target_area and i < len(CONTIG_CELLS_INDEX):
                a = ()
                pos = CONTIG_CELLS_INDEX[i]
                r, c = row + pos[0], col + pos[1]
                capa = self.capaSol[r, c]
                if self.tmpInteret[r, c] > 0 and capa > 0:
                    rem_area = target_area - artif_area
                    srf = cell_capa if cell_capa <= rem_area else rem_area
                    a = self._cell_artif(r, c, srf, nb_niv)
                    if a:
                        building.append(a)
                        artif_area += srf
                        self.cellBaties[r, c] = 1
                else:
                    self.tmpInteret[r, c] = 0
                i += 1
            self._info('? artif_area = ' + str(artif_area) + 'm²', level='debug')
        else:
            self.tmpInteret[row, col] = 0

        return building

    def _reshape(self, row: int, col: int) -> tuple:
        """Pour densifier verticalement une surface au sol donnee"""

        sp = 0
        housed = 0
        ssol = self.srfBatiResSol[row, col]
        existing_sp = self.srfBatiPla[row, col]
        conso = self.m2PlaHab[row, col]
        niv_moy = float(existing_sp / ssol) if ssol != 0 else 0

        # On cherche a tirer un nombre d'etage sperieur a l'existant
        nb_niv = self._choose_building_dim('height', row, col)
        if nb_niv > niv_moy:
            sp = ssol * nb_niv
            # On enleve l'existant pour connaîte la surface nouvelle
            if sp > existing_sp:
                sp -= existing_sp
                # On verifie que la surface finale suffit a loger au moins une personne
                if sp < conso:
                    info = '! built < area required for someone : ' + str(sp) + ' < ' + str(conso)
                    self._info(info, 'debug')
                    return (0, 0)
            else:
                info = '! failed to densify here : ' + str(sp) + ' < ' + str(existing_sp)
                self._info(info, 'debug')
                return (0, 0)

            housed = int(round(sp/conso))

        return (sp, housed)

    def _continue(self, pop, srfMax=None):
        if srfMax:
            if self.yearArtif >= srfMax:
                return False
        if self.yearPopCount >= pop:
            return False
        if self.tmpInteret.sum() <= 0:
            return False
        return True

    def _requalify(self, pop: int) -> tuple:
        """Pour requalifier une cellule deja batie : densifier en hauteur"""

        # TODO: modifier la surface au sol + differents criteres
        ignored_cells = 0
        chosen_cells = 0
        if self.densifyOld:
            self.tmpInteret = np.where((self.srfBatiPlaInit > 0)
                                       & (self.txArtifInit >= self.exclusionRatio),
                                       self.interet, 0)
            # Ici on requalifie l'existant chaque annee
            self._info("densifyOld : on reconstruit sur des anciennes zones baties car "
                       + str(int(pop - self.yearPopCount)) + " personnes restantes.", 'verbose')
        # Densification partout en fin de simu si necessaire
        # elif self.currentYear == self.finalYear:
        #     info = "finalYear : on force la densification pour loger les " + \
        #            str(int(pop - self.yearPopCount)) + " personnes ne sont toujours pas logees."
        #     self._info(info, 'verbose')
        choosable_cells = (np.where(self.tmpInteret > 0, 1, 0)).sum()
        info = str(choosable_cells) + ' cellules disponibles pour le processus de densification...'
        self._info(info, 'verbose')

        # On tente de loger les personnes restantes
        while self._continue(pop):
            # Les cellules chosies en cache
            cells = self._choose_cells(self.tmpInteret, self._n_choices)
            while self._continue(pop) and cells:
                sp = 0
                row, col = cells.pop()
                self.heatMap[row, col] += 1
                sp, ho = self._reshape(row, col)
                if sp > 0:
                    chosen_cells += 1
                    self.tmpSrfBatiPla[row, col] += sp
                    self.yearPopCount += ho
                else:
                    ignored_cells += 1
                self.tmpInteret[row, col] = 0
                if self._interface in ['qgs', 'gui']:
                    if self.isCanceled():
                        return (0, 0)

        return (chosen_cells, ignored_cells)

    def _urbanisation(self, pop: int, srfMax: int,
                      use_zau: bool = False) -> tuple:
        """Fonction principale pour gerer artificialisation puis densification"""

        self.yearArtif = 0
        self.yearPopCount = 0
        self.tmpSrfBatiPla = np.zeros([self.rows, self.cols], np.uint16)
        self.tmpSrfBatiSol = np.zeros([self.rows, self.cols], np.uint16)
        self.tmpSrfRoutes = np.zeros([self.rows, self.cols], np.uint16)
        self.tmpInteret = np.where((self.txArtifInit < self.exclusionRatio)
                                   & (self.cellBaties == 0) & (self.capaSol > 0), self.interet, 0)
        if use_zau and self.studyAreaName in ['gpsl', 'pyr', 'prp', 'fig']:
            # Bidouille si on a pas toute les ZAU
            self.tmpInteret = self.tmpInteret * np.where(self.pluPrio == 1, IMPORTANCE_ZAU, 1)
        elif use_zau:
            self.tmpInteret = np.where(self.pluPrio == 1, self.tmpInteret, 0)
        self.tmpInteret = self.tmpInteret / self.tmpInteret.max()

        # Expansion par ouverture de nouvelles cellules ou densification au sol
        while self._continue(pop, srfMax):
            cells = self._choose_cells(self.tmpInteret, self._n_choices)
            while self._continue(pop, srfMax) and cells:
                row, col = cells.pop()
                self.heatMap[row, col] += 1
                building = self._expansion(row, col)
                if building:
                    self._info('? building = ', level='debug')
                    for e in building:
                        self._info('    ' + str(e), level='debug')
                        # y, x, route, bati sol, bati plancher, personnes logees
                        (row, col, sr, sb, sp, ho) = e
                        artif = sr + sb
                        self.capaSol[row, col] -= artif
                        self.tmpSrfBatiSol[row, col] += sb
                        self.tmpSrfBatiPla[row, col] += sp
                        self.tmpSrfRoutes[row, col] += sr
                        self.yearPopCount += ho
                        self.yearArtif += artif

                        self.tmpInteret[row, col] = 0

                if self._interface in ['qgs', 'gui']:
                    # Pour permettre l'annulation d'un objet QgsTask
                    if self.isCanceled():
                        return

        # Mise a jour des attributs de l'objet
        self.artif += self.tmpSrfBatiSol
        self.artif += self.tmpSrfRoutes
        self.txArtif = (self.artif / self.srfCell).astype(np.float32)
        self.srfBatiSol += self.tmpSrfBatiSol
        if self.buildNonRes:
            self.tmpSrfBatiSol = (self.tmpSrfBatiSol * self.txSsr).round().astype(np.uint16)
        self.srfBatiResSol += self.tmpSrfBatiSol
        self.srfBatiPla += self.tmpSrfBatiPla
        # self.srfRoutes += self.tmpSrfRoutes
        popCount = np.where(self.m2PlaHab != 0,
                            (self.tmpSrfBatiPla / self.m2PlaHab).round(), 0).astype(np.uint16)
        self.demographie += popCount

        sumInteret = self.tmpInteret.sum()
        # On arrête d'utiliser les ZAU si elles sont saturees
        if sumInteret <= 0 and use_zau:
            self.skipZau = True
            self.skipZauYear = self.currentYear
            info = "\n! pluPriority : tmpInteret.sum() == 0 -> on ignore les ZAU."
            self._info(info, 'verbose')
            return (pop - self.yearPopCount, srfMax - self.yearArtif)

        # On enleve les restrictions PLU si on a encore saturé la zone
        # (souvent déjà présentes via safer ou ppri...)
        if sumInteret <= 0 and (self.pluRest is not None and self.skipPluRestYear is None):
            self.interet = np.where(self.restrictionNonPlu != 1, self.interetComplet, 0)
            self.skipPluRestYear = self.currentYear
            info = "\n! pluRestriction : tmpInteret.sum() == 0 -> on ignore les restrictions PLU."
            self._info(info, 'verbose')
            return (pop - self.yearPopCount, srfMax - self.yearArtif)

        # Etape de densification si besoin
        if self.yearPopCount < pop:
            self.tmpSrfBatiPla = np.zeros([self.rows, self.cols], np.uint16)
            rebuiltCells, ignoredCells = self._requalify(pop)
            self._info(str(rebuiltCells) + " ont ete reconstruites avec succes.", 'verbose')
            self._info(str(ignoredCells) + " ont ete ignorees.", 'verbose')
            self.srfBatiPla += self.tmpSrfBatiPla
            popCount = np.where(self.m2PlaHab != 0, (self.tmpSrfBatiPla
                                                     / self.m2PlaHab).round(), 0)
            self.demographie += popCount.astype(np.uint16)

        # Retourne le trop ou le manque pour annee suivante
        return (pop - self.yearPopCount, srfMax - self.yearArtif)


# Autres classes ou fonctions utiles
def random_scenario(input_dir):

    exp = r'[0-9]*m'
    match = re.match(exp, input_dir.name)
    if match:
        data_dir = input_dir
    else:
        data_dir = data_dir = Path(input_dir/'fake')

    new_settings = {}
    rng_settings = {
        "pixRes": ['list', ['20m', '25m', '30m', '35m',
                            '40m', '45m', '50m', '55m',
                            '60m', '65m', '70m', '75m',
                            '80m', '85m', '90m', '95m', '100m']],
        "growth": ['interval', [0.01, 2.0]],
        "finalYear": ['list', [2040]],
        "scenario": ['list', ['tendanciel', 'stable', 'reduction']],
        "pluPriority": ['list', [True, False]],
        "buildNonRes": ['list', [True, False]],
        "exclusionRatio": ['interval', [0.0, 0.2]],
        "maxBuiltRatio": ['interval', [60.0, 100.0]],
        "maxUsedSrfPla": ['interval', [50, 500]],
        "densifyOld": ['list', [True, False]],
        "winSize": ['list', [3, 5, 7, 9]],
        "minContig": ['interval', [0.0, 0.4]],
        "maxContig": ['interval', [0.6, 1.0]],
        "roadRatio": ['list', [0]],
        "fitTo": ['list', ['iris']],
        "ecoWeight": ['interval', [0, 10]],
        "roadWeight": ['interval', [0, 10]],
        "sirWeight": ['interval', [0, 10]],
        "traWeight": ['interval', [0, 10]]}

    for param, values in rng_settings.items():
        mode = values[0]
        if param == 'pixRes':
            while not data_dir.exists():
                pos = random.randint(0, len(values[1]) - 1)
                v = values[1][pos]
                data_dir = input_dir/v
            new_settings['dataDir'] = data_dir
        else:
            if mode == 'interval':
                low, high = values[1][0], values[1][1]
                if isinstance(low, float):
                    v = round(random.uniform(low, high), 4)
                elif isinstance(low, int):
                    v = random.randint(low, high)
            else:
                pos = random.randint(0, len(values[1]) - 1)
                v = values[1][pos]
            new_settings[param] = v

    return new_settings
