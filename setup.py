# -*- coding: utf-8 -*-
from distutils.core import setup

setup(name='SimUrbaPy',
      packages=['simurba'],
      version='1.2',
      description='Outil de simulation',
      long_description="Outil de simulation et d'aide à la decision\
                        en contexte de planification urbaine",
      author='Vincent Delbar',
      author_email='vincent.delbar@gmail.com',
      url='https://gitlab.com/vidlb/simurbapy',
      license='None',
      platforms=['Linux', 'Mac', 'Windows'])
