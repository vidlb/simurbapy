# Dockerfile valable pour la simu uniquement (lignes pour qgis et module prep commentees)
FROM debian:buster

# Pour QGIS
#RUN apt-get update && apt-get -y install gnupg2 apt-transport-https
#RUN echo "deb     http://qgis.org/debian stretch main" >> /etc/apt/sources.list
#RUN gpg --keyserver keyserver.ubuntu.com  --recv CAEB3DC3BDF7FB45
#RUN gpg --keyserver keyserver.ubuntu.com --keyserver-options  http-proxy=http://proxy.ign.fr:3128 \
#  --keyserver-options  https-proxy=https://proxy.ign.fr:3128 --recv CAEB3DC3BDF7FB45
#RUN gpg --export --armor CAEB3DC3BDF7FB45 | apt-key add -
#RUN apt-get && apt-get install xvfb qgis python-qgis python3-skimage python3-xlrd

# Pour la simulation
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3-gdal python3-numpy python3-pandas python3-scipy python3-distutils python3-pycurl

RUN useradd docker && addgroup docker staff
RUN mkdir /home/docker \
&& mkdir /home/docker/simurba \
&& mkdir /home/docker/output

COPY simurba/__init__.py /home/docker/simurba
#COPY simurba/expr.py /home/docker/simurba
COPY simurba/fit.py /home/docker/simurba
#COPY simurba/prep.py /home/docker/simurba
COPY simurba/simu.py /home/docker/simurba
COPY simurba/tools.py /home/docker/simurba
COPY scripts/oms_run.py /home/docker
COPY setup.py /home/docker

COPY scripts/oms_run.py /usr/bin

WORKDIR /home/docker
RUN python3 /home/docker/setup.py install

RUN chown -R docker:docker /home/docker

USER docker
RUN python3 -c "from simurba import Simulation"

RUN pwd && ls -al
