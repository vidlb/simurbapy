# **SimUrbaPy**

### Installation
Afin d'utiliser uniquement la simulation, il n'est pas nécessaire d'installer QGIS.
Après avoir cloné le dépôt, installez les dépendances suivantes :

Pour Debian/Ubuntu
```shell
sudo apt-get install python3-numpy python3-scipy python3-pandas python3-gdal
```
Windows / OSX
```shell
python3 -m pip install -r requirements.txt
```
Vous pouvez ensuite installer le programme SimUrba dans votre répertoire utilisateur Python :
```shell
python3 setup.py install --user
```

### Docker
Après installation:
```shell
sudo usermod -aG docker $USER
sudo systemctl restart docker
```

Pour construire l'image Docker sur le repo d'images Gitlab (depuis un repo local)
```shell
cd simurbapy
docker login registry.gitlab.com
docker build -t registry.gitlab.com/vidlb/simurbapy .
docker push registry.gitlab.com/vidlb/simurbapy
```

### Care
Cela nécessite le binaire spécial de care (https://openmole.org/script/care).
```shell
cd simurbapy
care -o simurba.tgz.bin -p /home/vidlb/SimUrba/zooms/mtp/inputs/30m python3 -c \
"from simurba import Simulation; s=Simulation('cli', 'dataDir=mtp/inputs/30m')"
mv simurba.tgz.bin $HOME/.openmole/$HOST/webui/projects
```

### OpenMOLE
Simulation testée avec la derniere version (9.0) depuis https://next.openmole.org/Download.html
```shell
openmole --no-browser --mem 3G
```

## Simulation
Un objet Simulation s'initialise avec un premier argument "interface". Les valeurs possibles sont :
- batch : pour une utilisation avec le script batch.py, adapté au traitement pour le multiprocessing, pas de print( )
- cli : mode par défaut, avec bash ou l'interpréteur python/ipython s'associe avec le paramètre 'verbose' de la simulation
- gui : spécifique pour le plugin QGIS ou une interface Qt5 basique
- openmole : traitement spécial des paramètres et du fichier des measures.csv, pas de print( )
- psql : print des tuples et exécute une commande psql via os.system( ) - simule indéfiniment avec le script direct_sampling
- void : pas de print ni de sorties (pour faire des test / un benchmark cProfile)

Les paramètres sont ensuite donnés dans une chaine de type 'key=value' ou alors 'param_1 param_2...param_n'
 et ici n doit être le nombre exact de paramètres de possibles (voir objet SETTINGS en tête du module simu.py).

Le paramètre "scenario" doit être dans ['tendanciel', 'stable', 'reduction', 'inf', 'random'].
On voit ici deux nouveaux types de scénarios : inf permet de ne pas limiter la surface artificialisée (intéressant pour l'analyse de sensibilité).
Le bonus : scenario random = un tirage uniforme parmi toutes les combinaisons de paramètres
(voir les bornes spécifiées dans la fonction random_scenario en tête du fichier simu).
Ce scénario permet de faire un sampling direct en pur python dans l'espace des paramètres avec systemd et postgresql.

**Usage :**
Avec l’interpréteur Python
```python
from simurba import Simulation  # == from simurba.simu import Simulation
s = Simulation(interface='cli', param_str='dataDir=inputs/30m outputDir=some/path verbose=True')
s.run(write_results=True)
s.compute_results("some/other/path")
```

Depuis un shell
```shell
python3 -c "from simurba import Simulation; s = Simulation('cli', 'scenario=random'); s.run()"
```

Dans OpenMOLE
```javascript
val pythonTask=UDockerTask(DockerImage(image = "vidlb/simurbapy", tag = "latest", registry = "https://registry.gitlab.com"),
    //Avec tous les paramètres dans l'ordre:
    //"""python3 oms_run.py inputs/${pixRes} output ${growth} ${...}""") set (
    //Certains paramètres spécifiées dans n'importe quel ordre, les autres prendront des valeurs par défaut
    """python3 oms_run.py growth=${growth} outputDir=output dataDir=inputs/60m""") set (
    dataDir := workDirectory / "inputs/60m",
    (inputs, outputs) += growth,
    inputFiles += (dataDir, "inputs/60m"),
    outputFiles += ("output/measures.csv", measures)
)
```

*Paramètres disponibles :*

| Paramètre     | Minimum | Maximum | Type | Valeur par défaut | Description |
|-------------  |---------|---------|-------|-------------------|-------------|
| dataDir       | | | str | | Dossier contenant la donnée de simulation préparée |
| outputDir     | | | str | | Dossier contenant les sorties de la simulation |
| growth        | 0 | 3 | float | -1 | Taux d’évolution annuel de la population (%) ; 0 ou -1 => taux d'évolution récente |
| finalYear     | 2016 | 2100 | int | 2040 | Année de fin de la simulation |
| scenario      | 0 | inf | str | tendanciel | Scénario de le simulation (reduction, stable, tendanciel, inf OU random) |
| pluPriority   | 0.0 | 1.0 | bool | True | Utilisation du PLU pour peupler les ZAU en priorité |
| buildNonRes   | 0.0 | 1.0 | bool | True | Pour simuler la construction bâti non résidentiels (en utilisant un taux de résidentiel par IRIS) |
| exclusionRatio| 0 | 0.2 | float |0.1  | Taux pour exclure de la couche d'intérêt les cellules initialement artificialisées |
| maxBuiltRatio | 50.0 | 100.0 | float | 80 | Taux maximal de la surface bâtie au sol d’une cellule (%) |
| maxUsedSrfPla | 0 | 1000 | int | 200 | Nombre pour limiter le maximal de m² plancher utilisés par habitant |
| densifyOld    | 0.0  | 1.0 | bool | True | Pour autoriser à augmenter la surface plancher dans des cellules urbanisées avant le début de la simulation |
| winSize       | 5 | 9 | int | 5 | Taille en pixels du côté de la fenêtre glissante pour calcul de la somme ou de la moyenne des valeurs voisines |
| minContig     | 0 | 0.4 | float | 0.1 | Nombre minimal de cellules urbanisées contiguës pour urbanisation d’une cellule vide |
| maxContig     | 0.6  | 1 | float | 0.7 | Nombre maximal de cellules urbanisées contiguës pour urbanisation d’une cellule vide |
| roadRatio     | -1   | 3 | float | -1  | Rapport entre surface de routes et surfaces bâties (-1 pour utiliser les chiffres 2015-2016) |
| fitTo         | | | str | iris | Niveau(x) d'unité spatiale à utiliser pour le fitting (zone,com,iris) |
| seed          | 0 | inf | int | None | Graine du générateur de nombres aléatoires (pas de seed par défaut) |
| ecoWeight     | 0 | 10 | int | 5 | Poids diminuant l'intérêt d'une cellule en fonction de l'importance écologique |
| roadWeight    | 0 | 10 | int | 2 | Poids en lien avec la présence de routes |
| sirWeight     | 0 | 10 | int | 4 | Poids en lien avec la présence d'aménités |
| traWeight     | 0 | 10 | int | 3 | Poids en lien avec la présence de transports en commun |
| tiffs         | 0.0 | 1.0 | bool | True | Indique si les images sont écrites avec les sorties |
| snaps         | 0.0 | 1.0 | bool | False | Pour enregistrer des instantanés chaque année (et générer des GIF) |
| verbose       | 0.0 | 1.0 | bool | False | Pour des messages détaillés sur les erreurs + statistiques chaque année |

Les poids des rasters d'aménités peuvent également être renseignés dans le fichier "dataDir/interet/poids.csv"

## Preparation
**Installation**:
Ce module nécessite QGIS3 et n'a pas encore été testé sous OpenMole
```shell
sudo apt-get -y install gnupg2 apt-transport-https
sudo echo "deb     http://qgis.org/debian stretch main" >> /etc/apt/sources.list
sudo gpg --keyserver keyserver.ubuntu.com  --recv CAEB3DC3BDF7FB45
sudo gpg --export --armor CAEB3DC3BDF7FB45 | sudo apt-key add -
sudo apt-get update && sudo apt-get install qgis python3-qgis
sudo apt-get install python3-xlrd python3-skimage
```

**Usage :**
Avec l’interpréteur Python
```python
from simurba.prep import Preparation
p = Preparation(interface='cli', param_str='localData=local_data/figeac globalData=simu/global_data dpt=12,81', debug=True)
p.run()
```

Depuis un shell
```shell
python3 -c "from simurba.prep import Preparation; p = Preparation('cli', debug=True); p.run()"
```

*Paramètres disponibles :*

| Processus             | Paramètre   | Minimum | Maximum | Type | Valeur par défaut | Description  |
|-----------------------|-------------|---------|---------|------|-------------------|--------------|
| Extractions           | localData   | | | str | | Dossier contenant la donnée spécifique à la zone d'étude |
|                       | globalData  | | | str | | Dossier contenant la donnée nationale et régionale |
|                       | outputDir   | | | str | | Dossier de sortie  |
|                       | bufferDist  | 500 | 2000 | int | 1000 | Taille de tampon utilisée pour extraire les données (m) |
|                       | dpt         | 1 | 97 | str | 34 | Départements de la zone d'étude séparés par des virgules |
| Création de la grille | pixRes      | 20 | 200 | int | 50 | Résolution de la maille de la grille / des rasters (m) |
| Démographie           | levelHeight | 2 | 3 | int | 3 | Hauteur utilisée pour estimer le nombre d’étages (m) |
|                       | minSurf     | 50 | 100 | int | 40 | Surface minimale pour un bâtiment habitable (m²) |
|                       | maxSurf     | 5000 | 10000 | int | 8000 | Surface maximale pour un bâtiment habitable (m²) |
|                       | useTxrp     | 0.0 | 1.0 | bool | True | Utilisation du taux de résidences principales lors de l'estimation du plancher |
| Rasters de distance   | roadDist    | 50 | 300 | int | 200 | Distance maximale aux routes (m) |
|                       | transDist   | 100 | 500 | int | 300 | Distance maximale aux arrêts de transport (m) |
| Raster de restriction | maxSlope    | 0 | 30 | float | 30 | Seuil de pente pour interdiction à la construction (%) |
| Fitting               | fitLevels   | | | str | zone,com,iris | Niveaux géographiques pour le fitting séparés par des virgules |
| Tous                  | force       | 0.0 | 1.0 | bool | False | Attention: suppression brutale du répertoire de sortie avant de lancer la simulation |
|                       | speed       | 0.0 | 1.0 | bool | False | Utilisation de plusieurs threads - à éviter si moins de 8 Go de RAM disponibles |
|                       | wisdom      | 0.0 | 1.0 | bool | True | Conserver les données intermédiaires pour construire un projet complet par zones |

Les poids et distances pour la création des cartes de chaleurs à partir des points de la base SIRENE doivent être renseignés dans les fichiers localData/distances.csv et poids.csv
